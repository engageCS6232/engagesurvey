﻿
using System.Drawing;

namespace EvaluationSystem.View
{
    partial class TypeOneEvaluation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.submitButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.flowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.bottomPanel = new System.Windows.Forms.Panel();
            this.topPanel = new System.Windows.Forms.Panel();
            this.lblParticipant = new System.Windows.Forms.Label();
            this.txtStage = new System.Windows.Forms.Label();
            this.txtType = new System.Windows.Forms.Label();
            this.lblStage = new System.Windows.Forms.Label();
            this.lblType = new System.Windows.Forms.Label();
            this.bottomPanel.SuspendLayout();
            this.topPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // submitButton
            // 
            this.submitButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.submitButton.Location = new System.Drawing.Point(24, 2);
            this.submitButton.Margin = new System.Windows.Forms.Padding(2);
            this.submitButton.Name = "submitButton";
            this.submitButton.Size = new System.Drawing.Size(67, 25);
            this.submitButton.TabIndex = 35;
            this.submitButton.Text = "Submit";
            this.submitButton.UseVisualStyleBackColor = true;
            this.submitButton.Click += new System.EventHandler(this.submitButton_Click_1);
            // 
            // cancelButton
            // 
            this.cancelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelButton.Location = new System.Drawing.Point(113, 2);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(2);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(87, 25);
            this.cancelButton.TabIndex = 36;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // flowLayoutPanel
            // 
            this.flowLayoutPanel.BackColor = Color.White;
            this.flowLayoutPanel.AutoScroll = true;
            this.flowLayoutPanel.AutoSize = true;
            this.flowLayoutPanel.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.flowLayoutPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel.Location = new System.Drawing.Point(24, 59);
            this.flowLayoutPanel.Name = "flowLayoutPanel";
            this.flowLayoutPanel.Size = new System.Drawing.Size(598, 677);
            this.flowLayoutPanel.TabIndex = 37;
            this.flowLayoutPanel.WrapContents = false;
            // 
            // bottomPanel
            // 
            this.bottomPanel.Controls.Add(this.cancelButton);
            this.bottomPanel.Controls.Add(this.submitButton);
            this.bottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bottomPanel.Location = new System.Drawing.Point(0, 736);
            this.bottomPanel.Name = "bottomPanel";
            this.bottomPanel.Size = new System.Drawing.Size(663, 33);
            this.bottomPanel.TabIndex = 38;
            // 
            // topPanel
            // 
            this.topPanel.Controls.Add(this.lblParticipant);
            this.topPanel.Controls.Add(this.txtStage);
            this.topPanel.Controls.Add(this.txtType);
            this.topPanel.Controls.Add(this.lblStage);
            this.topPanel.Controls.Add(this.lblType);
            this.topPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.topPanel.Location = new System.Drawing.Point(0, 0);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(663, 59);
            this.topPanel.TabIndex = 39;
            // 
            // lblParticipant
            // 
            this.lblParticipant.AutoSize = true;
            this.lblParticipant.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.lblParticipant.Location = new System.Drawing.Point(493, 9);
            this.lblParticipant.Name = "lblParticipant";
            this.lblParticipant.Size = new System.Drawing.Size(129, 22);
            this.lblParticipant.TabIndex = 4;
            this.lblParticipant.Text = "Evaluation for:";
            this.lblParticipant.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtStage
            // 
            this.txtStage.AutoSize = true;
            this.txtStage.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.txtStage.Location = new System.Drawing.Point(100, 34);
            this.txtStage.Name = "txtStage";
            this.txtStage.Size = new System.Drawing.Size(0, 22);
            this.txtStage.TabIndex = 3;
            // 
            // txtType
            // 
            this.txtType.AutoSize = true;
            this.txtType.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.txtType.Location = new System.Drawing.Point(100, 4);
            this.txtType.Name = "txtType";
            this.txtType.Size = new System.Drawing.Size(0, 22);
            this.txtType.TabIndex = 2;
            // 
            // lblStage
            // 
            this.lblStage.AutoSize = true;
            this.lblStage.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.lblStage.Location = new System.Drawing.Point(27, 34);
            this.lblStage.Name = "lblStage";
            this.lblStage.Size = new System.Drawing.Size(59, 22);
            this.lblStage.TabIndex = 1;
            this.lblStage.Text = "Stage:";
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.lblType.Location = new System.Drawing.Point(27, 4);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(55, 22);
            this.lblType.TabIndex = 0;
            this.lblType.Text = "Type:";
            // 
            // TypeOneEvaluation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(680, 390);
            this.Controls.Add(this.topPanel);
            this.Controls.Add(this.bottomPanel);
            this.Controls.Add(this.flowLayoutPanel);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "TypeOneEvaluation";
            this.Text = "Type One Evaluation";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.bottomPanel.ResumeLayout(false);
            this.topPanel.ResumeLayout(false);
            this.topPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button submitButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel;
        private System.Windows.Forms.Panel bottomPanel;
        private System.Windows.Forms.Panel topPanel;
        private System.Windows.Forms.Label txtStage;
        private System.Windows.Forms.Label txtType;
        private System.Windows.Forms.Label lblStage;
        private System.Windows.Forms.Label lblType;
        private System.Windows.Forms.Label lblParticipant;
    }
}