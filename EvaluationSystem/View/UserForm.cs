﻿using EvaluationSystem.Model;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace EvaluationSystem.View
{
    public partial class UserForm : Form
    {
        private Employees newEmployee;
        private EvaluatorForm evaluatorForm;
        private EmployeeEvaluations employeeEvaluationsForm;
        private EmployeeSelfEvaluationForm emplyeeEvaluationsFormSelf;
        private DisplayEvaluationsByCohort displayEvaluationsbyCohortForm;

        public UserForm()
        {
            InitializeComponent();
            // This renderer displays the underline of each accelerator key in the menu
            menuStrip1.Renderer = new CustomMenuStripRenderer();
           
        }

        // Used to render the menustrip to display the underline under the letter
        class CustomMenuStripRenderer : ToolStripProfessionalRenderer
        {
            public CustomMenuStripRenderer() : base() { }
            public CustomMenuStripRenderer(ProfessionalColorTable table) : base(table) { }


            protected override void OnRenderItemText(ToolStripItemTextRenderEventArgs e)
            {
                e.TextFormat &= ~TextFormatFlags.HidePrefix;
                base.OnRenderItemText(e);
            }
        }

        /// <summary>
        /// The functions that occur when the UserForm window is opened.  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserForm_Load(object sender, EventArgs e)
        {
            this.showAdminMenu();
        }

        /// <summary>
        /// Loads the employee's information into the system.  
        /// </summary>
        /// <param name="passedEmployee"></param>
        public void LoadEmployee(Employees passedEmployee)
        {
            this.newEmployee = passedEmployee;
            this.toolStripUserName.Text = newEmployee.FName + " " + newEmployee.LName + "   ";
            this.toolStripUserName.BackColor = ColorTranslator.FromHtml("#FFFFFF");
         }

        /// <summary>
        /// Closes the application when the X at the top of the window is clicked.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void viewOpenEvaluationsToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void selectToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void evaluationsToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void administratorToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void viewUserProfileToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void selectCohortToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Displays the form to create a cohort as a Mdi Child of the UserForm. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void createCohortToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                CohortsCreateForm frmCreateCohort = new CohortsCreateForm();
                frmCreateCohort.MdiParent = this;
                frmCreateCohort.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }

        }

        /// <summary>
        /// Enables the form when you click the menu item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void selectEvaluatorsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.evaluatorForm == null)
            {
                this.evaluatorForm = new EvaluatorForm();
                this.evaluatorForm.MdiParent = this;
                this.evaluatorForm.LoadEmployee(this.newEmployee);
                this.evaluatorForm.FormClosed += new FormClosedEventHandler(Evaluator_FormClosed);
                this.evaluatorForm.Show();
            }
            else
            {
                this.evaluatorForm.Activate();
            }
        }

        /// <summary>
        /// Closes the form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Evaluator_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.evaluatorForm = null;
        }

        /// <summary>
        /// Enables the form when you click the menu item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void openEvaluationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.emplyeeEvaluationsFormSelf == null)
            {
                this.emplyeeEvaluationsFormSelf = new EmployeeSelfEvaluationForm(this.newEmployee, this);
                this.emplyeeEvaluationsFormSelf.MdiParent = this;
                this.emplyeeEvaluationsFormSelf.FormClosed += new FormClosedEventHandler(openEvaluations_FormClosed);
                this.emplyeeEvaluationsFormSelf.Show();
            }
            else
            {
                this.emplyeeEvaluationsFormSelf.Activate();
            }
        }

        /// <summary>
        /// Closes the form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void openEvaluations_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.emplyeeEvaluationsFormSelf = null;
        }

        /// <summary>
        /// Enables the form when you click the menu item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void othersEvaluationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.employeeEvaluationsForm == null)
            {
                this.employeeEvaluationsForm = new EmployeeEvaluations(this.newEmployee, this);
                this.employeeEvaluationsForm.MdiParent = this;
                this.employeeEvaluationsForm.FormClosed += new FormClosedEventHandler(othersEvaluations_FormClosed);
                this.employeeEvaluationsForm.Show();
            }
            else
            {
                this.employeeEvaluationsForm.Activate();
            }
        }

        /// <summary>
        /// Closes the form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void othersEvaluations_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.employeeEvaluationsForm = null;
        }

        /// <summary>
        /// Removes the administrator menu if the employee is not identified as having administrative privileges in the system.
        /// </summary>
        private void showAdminMenu()
        {
            if (!this.newEmployee.Administrator)
            {
                menuStrip1.Items.Remove(administratorToolStripMenuItem);
            }
        }

        /// <summary>
        /// Closes the application when exit menu item is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// Creates and shows a new evaluation create form when the menu item is clicked. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void createEvaluationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                EvaluationCreateForm frmEvalCreate = new EvaluationCreateForm();
                frmEvalCreate.MdiParent = this;
                frmEvalCreate.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        /// <summary>
        /// Enables the form when you click the menu item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cohortReportToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            try
            {
                GraphReportSettings report = new GraphReportSettings();
                report.MdiParent = this;
                report.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        /// <summary>
        /// Enables the form when you click the menu item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void userReportsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                EvalReportSettings report = new EvalReportSettings();
                report.MdiParent = this;
                report.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        private void updateDeleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                CohortsDeleteForm frmDeleteCohort = new CohortsDeleteForm();
                frmDeleteCohort.MdiParent = this;
                frmDeleteCohort.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

        private void raterEvaluationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                EmployeeEvaluations frmEmployeeEvaluations = new EmployeeEvaluations(newEmployee, this);
                frmEmployeeEvaluations.MdiParent = this;
                frmEmployeeEvaluations.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        private void displayEvaluationsByCohortToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.employeeEvaluationsForm == null)
            {
                this.displayEvaluationsbyCohortForm = new DisplayEvaluationsByCohort();
                this.displayEvaluationsbyCohortForm.MdiParent = this;
                this.displayEvaluationsbyCohortForm.FormClosed += new FormClosedEventHandler(displayEvaluationsbyCohortForm_FormClosed);
                this.displayEvaluationsbyCohortForm.Show();
            }
            else
            {
                this.employeeEvaluationsForm.Activate();
            }
        }

        private void displayEvaluationsbyCohortForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.displayEvaluationsbyCohortForm = null;
        }
    }
}
