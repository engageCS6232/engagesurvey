﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluationSystem.Model
{
    /// <summary>
    /// Evaluations table class
    /// </summary>
    public class Evaluations
    {

        public Evaluations()
        {
        }

        //*********** Getters and Setters **********//
        public int EvaluationID { get; set; }
        public int StageID { get; set; }
        public int? RoleID { get; set; }
        public DateTime? CompletionDate { get; set; }
        public int TypeID { get; set; }
        public int ParticipantID { get; set; }
        public int? EvaluatorID { get; set; }
        public String RoleName { get; set; }
        public String EvaluatorName { get; set; } 
        public String ParticipantName { get; set; }

        /// <summary>
        /// Returns the concatenated string of evaluation ID, (evaluation type),  (evaluation stage).
        /// </summary>
        public string EvaluationInfo
        {
            get
            {
                return this.EvaluationID + "(Type: " + this.TypeID + ")(Stage:" + this.StageID + ")";
            }
        }

        /// <summary>
        /// Returns the concatenated string of evaluation type, (evaluation stage).
        /// </summary>
        public string EvaluationInfoNoID
        {
            get
            {
                return "Type: " + this.TypeID + "(Stage:" + this.StageID + ")";
            }
        }
    }
}
