﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EvaluationSystem.Model;
using EvaluationSystem.DBAccess;

namespace EvaluationSystemTest.DBAccessTest
{
    [TestClass]
    public class CohortEvalTrackerDALTest
    {
        //Create Eval Traker [A]
        private int cohortIDA = 25;
        private int stageIDA = 5;
        private int typeIDA = 1;
        private DateTime startA = Convert.ToDateTime("01/01/2017");
        private DateTime endA = Convert.ToDateTime("01/15/2017");



        //Create Eval Tracker (and Self) [B]
        private int cohortIDB = 26;
        private int stageIDB = 1;
        private int typeIDB = 2;
        private DateTime startB = Convert.ToDateTime("01/16/2017");
        private DateTime endB = Convert.ToDateTime("01/28/2017");
        
        /// <summary>
        /// Test for creation of CohortEvalTraker
        /// </summary>
        [TestMethod]
        public void TestCreateCohortEval()
        {
            Boolean check = CohortEvalTrackerDAL.CreateCohortEval(cohortIDA, stageIDA, typeIDA, startA, endA);
            Assert.IsTrue(check, "Cohort Evaluation not created.");
        }

        /// <summary>
        /// Test for creation of CohortEvalTraker (and Self
        /// </summary>
        [TestMethod]
        public void TestCreateCohortEvalAndSelfEval()
        {
            Boolean check = CohortEvalTrackerDAL.CreateCohortEvalAndSelfEval(cohortIDB, typeIDB, stageIDB, startB, endB);
            Assert.IsTrue(check, "Cohort Evaluation not created.");
        }
    }
}
