﻿namespace EvaluationSystem.View
{
    partial class CohortsCreateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.employeeListBox = new System.Windows.Forms.ListBox();
            this.employeesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cohortBox = new System.Windows.Forms.ListBox();
            this.cohortButton = new System.Windows.Forms.Button();
            this.employeeButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cohortNameBox = new System.Windows.Forms.TextBox();
            this.submitButton = new System.Windows.Forms.Button();
            this.cohortNameLabel = new System.Windows.Forms.Label();
            this.cancelButton = new System.Windows.Forms.Button();
            this._CS6232_g3DataSet = new EvaluationSystem._CS6232_g3DataSet();
            this.cS6232g3DataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.selectedBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.selectedTableAdapter = new EvaluationSystem._CS6232_g3DataSetTableAdapters.selectedTableAdapter();
            this.selectedBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.employeesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._CS6232_g3DataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cS6232g3DataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.selectedBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.selectedBindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // employeeListBox
            // 
            this.employeeListBox.DataSource = this.employeesBindingSource;
            this.employeeListBox.DisplayMember = "employeeInfo";
            this.employeeListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employeeListBox.FormattingEnabled = true;
            this.employeeListBox.ItemHeight = 20;
            this.employeeListBox.Location = new System.Drawing.Point(9, 92);
            this.employeeListBox.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.employeeListBox.Name = "employeeListBox";
            this.employeeListBox.Size = new System.Drawing.Size(226, 124);
            this.employeeListBox.TabIndex = 0;
            this.employeeListBox.ValueMember = "EmployeeID";
            // 
            // employeesBindingSource
            // 
            this.employeesBindingSource.DataSource = typeof(EvaluationSystem.Model.Employees);
            // 
            // cohortBox
            // 
            this.cohortBox.DataSource = this.employeesBindingSource;
            this.cohortBox.DisplayMember = "employeeInfo";
            this.cohortBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cohortBox.FormattingEnabled = true;
            this.cohortBox.ItemHeight = 20;
            this.cohortBox.Location = new System.Drawing.Point(317, 92);
            this.cohortBox.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cohortBox.Name = "cohortBox";
            this.cohortBox.Size = new System.Drawing.Size(234, 124);
            this.cohortBox.TabIndex = 1;
            this.cohortBox.ValueMember = "EmployeeID";
            // 
            // cohortButton
            // 
            this.cohortButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cohortButton.Location = new System.Drawing.Point(248, 135);
            this.cohortButton.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cohortButton.Name = "cohortButton";
            this.cohortButton.Size = new System.Drawing.Size(56, 29);
            this.cohortButton.TabIndex = 2;
            this.cohortButton.Text = ">";
            this.cohortButton.UseVisualStyleBackColor = true;
            this.cohortButton.Click += new System.EventHandler(this.cohortButton_Click);
            // 
            // employeeButton
            // 
            this.employeeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employeeButton.Location = new System.Drawing.Point(248, 169);
            this.employeeButton.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.employeeButton.Name = "employeeButton";
            this.employeeButton.Size = new System.Drawing.Size(56, 29);
            this.employeeButton.TabIndex = 3;
            this.employeeButton.Text = "< ";
            this.employeeButton.UseVisualStyleBackColor = true;
            this.employeeButton.Click += new System.EventHandler(this.employeeButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(313, 69);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(127, 20);
            this.label1.TabIndex = 4;
            this.label1.Text = "Cohort Members";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(11, 69);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 20);
            this.label2.TabIndex = 5;
            this.label2.Text = "Employees";
            // 
            // cohortNameBox
            // 
            this.cohortNameBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cohortNameBox.Location = new System.Drawing.Point(211, 26);
            this.cohortNameBox.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cohortNameBox.Name = "cohortNameBox";
            this.cohortNameBox.Size = new System.Drawing.Size(234, 26);
            this.cohortNameBox.TabIndex = 1;
            this.cohortNameBox.Tag = "Cohort Name";
            // 
            // submitButton
            // 
            this.submitButton.Enabled = false;
            this.submitButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.submitButton.Location = new System.Drawing.Point(385, 279);
            this.submitButton.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.submitButton.Name = "submitButton";
            this.submitButton.Size = new System.Drawing.Size(76, 29);
            this.submitButton.TabIndex = 8;
            this.submitButton.Text = "Submit";
            this.submitButton.UseVisualStyleBackColor = true;
            this.submitButton.Click += new System.EventHandler(this.submitButton_Click);
            // 
            // cohortNameLabel
            // 
            this.cohortNameLabel.AutoSize = true;
            this.cohortNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cohortNameLabel.Location = new System.Drawing.Point(106, 26);
            this.cohortNameLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.cohortNameLabel.Name = "cohortNameLabel";
            this.cohortNameLabel.Size = new System.Drawing.Size(103, 20);
            this.cohortNameLabel.TabIndex = 9;
            this.cohortNameLabel.Text = "Cohort Name";
            // 
            // cancelButton
            // 
            this.cancelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelButton.Location = new System.Drawing.Point(475, 279);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(76, 29);
            this.cancelButton.TabIndex = 10;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // _CS6232_g3DataSet
            // 
            this._CS6232_g3DataSet.DataSetName = "_CS6232_g3DataSet";
            this._CS6232_g3DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cS6232g3DataSetBindingSource
            // 
            this.cS6232g3DataSetBindingSource.DataSource = this._CS6232_g3DataSet;
            this.cS6232g3DataSetBindingSource.Position = 0;
            // 
            // selectedBindingSource
            // 
            this.selectedBindingSource.DataMember = "selected";
            this.selectedBindingSource.DataSource = this.cS6232g3DataSetBindingSource;
            // 
            // selectedTableAdapter
            // 
            this.selectedTableAdapter.ClearBeforeFill = true;
            // 
            // selectedBindingSource1
            // 
            this.selectedBindingSource1.DataMember = "selected";
            this.selectedBindingSource1.DataSource = this.cS6232g3DataSetBindingSource;
            // 
            // CohortsCreateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(560, 318);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.cohortNameLabel);
            this.Controls.Add(this.submitButton);
            this.Controls.Add(this.cohortNameBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.employeeButton);
            this.Controls.Add(this.cohortButton);
            this.Controls.Add(this.cohortBox);
            this.Controls.Add(this.employeeListBox);
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Name = "CohortsCreateForm";
            this.Text = "Create Cohort";
            this.Load += new System.EventHandler(this.CohortsCreateForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.employeesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._CS6232_g3DataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cS6232g3DataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.selectedBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.selectedBindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox employeeListBox;
        private System.Windows.Forms.ListBox cohortBox;
        private System.Windows.Forms.Button cohortButton;
        private System.Windows.Forms.Button employeeButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox cohortNameBox;
        private System.Windows.Forms.Button submitButton;
        private System.Windows.Forms.Label cohortNameLabel;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.BindingSource employeesBindingSource;
        private System.Windows.Forms.BindingSource cS6232g3DataSetBindingSource;
        private _CS6232_g3DataSet _CS6232_g3DataSet;
        private System.Windows.Forms.BindingSource selectedBindingSource;
        private _CS6232_g3DataSetTableAdapters.selectedTableAdapter selectedTableAdapter;
        private System.Windows.Forms.BindingSource selectedBindingSource1;
    }
}