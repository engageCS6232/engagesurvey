﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluationSystem.Model
{
    /// <summary>
    /// Employees table class
    /// </summary>
    public class Employees
    {

        public Employees()
        {
        }

        //*********** Getters and Setters **********//
        public int EmployeeID { get; set; }
        public String FName { get; set; }
        public String LName { get; set; }
        public String Email { get; set; }
        public String Username { get; set; }
        public Boolean Administrator { get; set; }
        public int? CohortID { get; set; }

        /// <summary>
        /// Returns the concatenated string of first name, last name (employeeID).
        /// </summary>
        public string EmployeeInfo
        {
            get
            {
                return this.LName + ", " + this.FName + "(" + this.EmployeeID + ")";
            }
        }

    }
}