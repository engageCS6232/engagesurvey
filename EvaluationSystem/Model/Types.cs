﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluationSystem.Model
{
    public class Types
    {
        public int TypeID { get; set; }
        public string TypeName { get; set; }

    }
}
