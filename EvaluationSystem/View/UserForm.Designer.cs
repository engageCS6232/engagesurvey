﻿namespace EvaluationSystem.View
{
    partial class UserForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectEvaluatorsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.evaluationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewOpenEvaluationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openEvaluationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.othersEvaluationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.administratorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createCohortsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.createCohortToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateDeleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.evaluationsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.createEvaluationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.displayEvaluationsByCohortToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createReportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userReportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cohortReportToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripUserName = new System.Windows.Forms.ToolStripTextBox();
            this.dELETEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.userToolStripMenuItem,
            this.evaluationsToolStripMenuItem,
            this.administratorToolStripMenuItem,
            this.toolStripUserName});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(3, 1, 0, 1);
            this.menuStrip1.Size = new System.Drawing.Size(1075, 26);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.logoutToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(44, 24);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // logoutToolStripMenuItem
            // 
            this.logoutToolStripMenuItem.Name = "logoutToolStripMenuItem";
            this.logoutToolStripMenuItem.Size = new System.Drawing.Size(132, 26);
            this.logoutToolStripMenuItem.Text = "&Log off";
            this.logoutToolStripMenuItem.Click += new System.EventHandler(this.logoutToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(132, 26);
            this.exitToolStripMenuItem.Text = "&Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // userToolStripMenuItem
            // 
            this.userToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectEvaluatorsToolStripMenuItem});
            this.userToolStripMenuItem.Name = "userToolStripMenuItem";
            this.userToolStripMenuItem.Size = new System.Drawing.Size(123, 24);
            this.userToolStripMenuItem.Text = "My &Information";
            // 
            // selectEvaluatorsToolStripMenuItem
            // 
            this.selectEvaluatorsToolStripMenuItem.Name = "selectEvaluatorsToolStripMenuItem";
            this.selectEvaluatorsToolStripMenuItem.Size = new System.Drawing.Size(196, 26);
            this.selectEvaluatorsToolStripMenuItem.Text = "&Select Evaluators";
            this.selectEvaluatorsToolStripMenuItem.Click += new System.EventHandler(this.selectEvaluatorsToolStripMenuItem_Click);
            // 
            // evaluationsToolStripMenuItem
            // 
            this.evaluationsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewOpenEvaluationsToolStripMenuItem});
            this.evaluationsToolStripMenuItem.Name = "evaluationsToolStripMenuItem";
            this.evaluationsToolStripMenuItem.Size = new System.Drawing.Size(120, 24);
            this.evaluationsToolStripMenuItem.Text = "My &Evaluations";
            this.evaluationsToolStripMenuItem.Click += new System.EventHandler(this.evaluationsToolStripMenuItem_Click);
            // 
            // viewOpenEvaluationsToolStripMenuItem
            // 
            this.viewOpenEvaluationsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openEvaluationsToolStripMenuItem,
            this.othersEvaluationsToolStripMenuItem});
            this.viewOpenEvaluationsToolStripMenuItem.Name = "viewOpenEvaluationsToolStripMenuItem";
            this.viewOpenEvaluationsToolStripMenuItem.Size = new System.Drawing.Size(223, 26);
            this.viewOpenEvaluationsToolStripMenuItem.Text = "&My Open Evaluations";
            this.viewOpenEvaluationsToolStripMenuItem.Click += new System.EventHandler(this.viewOpenEvaluationsToolStripMenuItem_Click);
            // 
            // openEvaluationsToolStripMenuItem
            // 
            this.openEvaluationsToolStripMenuItem.Name = "openEvaluationsToolStripMenuItem";
            this.openEvaluationsToolStripMenuItem.Size = new System.Drawing.Size(229, 26);
            this.openEvaluationsToolStripMenuItem.Text = "&Self-Evaluations";
            this.openEvaluationsToolStripMenuItem.Click += new System.EventHandler(this.openEvaluationsToolStripMenuItem_Click);
            // 
            // othersEvaluationsToolStripMenuItem
            // 
            this.othersEvaluationsToolStripMenuItem.Name = "othersEvaluationsToolStripMenuItem";
            this.othersEvaluationsToolStripMenuItem.Size = new System.Drawing.Size(229, 26);
            this.othersEvaluationsToolStripMenuItem.Text = "&Employee Evaluations";
            this.othersEvaluationsToolStripMenuItem.Click += new System.EventHandler(this.othersEvaluationsToolStripMenuItem_Click);
            // 
            // administratorToolStripMenuItem
            // 
            this.administratorToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createCohortsToolStripMenuItem1,
            this.evaluationsToolStripMenuItem1,
            this.createReportsToolStripMenuItem});
            this.administratorToolStripMenuItem.Name = "administratorToolStripMenuItem";
            this.administratorToolStripMenuItem.Size = new System.Drawing.Size(112, 24);
            this.administratorToolStripMenuItem.Text = "&Administrator";
            this.administratorToolStripMenuItem.Click += new System.EventHandler(this.administratorToolStripMenuItem_Click);
            // 
            // createCohortsToolStripMenuItem1
            // 
            this.createCohortsToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createCohortToolStripMenuItem,
            this.updateDeleteToolStripMenuItem});
            this.createCohortsToolStripMenuItem1.Name = "createCohortsToolStripMenuItem1";
            this.createCohortsToolStripMenuItem1.Size = new System.Drawing.Size(159, 26);
            this.createCohortsToolStripMenuItem1.Text = "&Cohorts";
            // 
            // createCohortToolStripMenuItem
            // 
            this.createCohortToolStripMenuItem.Name = "createCohortToolStripMenuItem";
            this.createCohortToolStripMenuItem.Size = new System.Drawing.Size(221, 26);
            this.createCohortToolStripMenuItem.Text = "&Create Cohort";
            this.createCohortToolStripMenuItem.Click += new System.EventHandler(this.createCohortToolStripMenuItem_Click);
            // 
            // updateDeleteToolStripMenuItem
            // 
            this.updateDeleteToolStripMenuItem.Name = "updateDeleteToolStripMenuItem";
            this.updateDeleteToolStripMenuItem.Size = new System.Drawing.Size(221, 26);
            this.updateDeleteToolStripMenuItem.Text = "&Update/Delete/View";
            this.updateDeleteToolStripMenuItem.Click += new System.EventHandler(this.updateDeleteToolStripMenuItem_Click);
            // 
            // evaluationsToolStripMenuItem1
            // 
            this.evaluationsToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createEvaluationToolStripMenuItem,
            this.displayEvaluationsByCohortToolStripMenuItem});
            this.evaluationsToolStripMenuItem1.Name = "evaluationsToolStripMenuItem1";
            this.evaluationsToolStripMenuItem1.Size = new System.Drawing.Size(159, 26);
            this.evaluationsToolStripMenuItem1.Text = "&Evaluations";
            // 
            // createEvaluationToolStripMenuItem
            // 
            this.createEvaluationToolStripMenuItem.Name = "createEvaluationToolStripMenuItem";
            this.createEvaluationToolStripMenuItem.Size = new System.Drawing.Size(281, 26);
            this.createEvaluationToolStripMenuItem.Text = "&Create Evaluation";
            this.createEvaluationToolStripMenuItem.Click += new System.EventHandler(this.createEvaluationToolStripMenuItem_Click);
            // 
            // displayEvaluationsByCohortToolStripMenuItem
            // 
            this.displayEvaluationsByCohortToolStripMenuItem.Name = "displayEvaluationsByCohortToolStripMenuItem";
            this.displayEvaluationsByCohortToolStripMenuItem.Size = new System.Drawing.Size(281, 26);
            this.displayEvaluationsByCohortToolStripMenuItem.Text = "Display Evaluations By Cohort";
            this.displayEvaluationsByCohortToolStripMenuItem.Click += new System.EventHandler(this.displayEvaluationsByCohortToolStripMenuItem_Click);
            // 
            // createReportsToolStripMenuItem
            // 
            this.createReportsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.userReportsToolStripMenuItem,
            this.cohortReportToolStripMenuItem1});
            this.createReportsToolStripMenuItem.Name = "createReportsToolStripMenuItem";
            this.createReportsToolStripMenuItem.Size = new System.Drawing.Size(159, 26);
            this.createReportsToolStripMenuItem.Text = "&Reports";
            // 
            // userReportsToolStripMenuItem
            // 
            this.userReportsToolStripMenuItem.Name = "userReportsToolStripMenuItem";
            this.userReportsToolStripMenuItem.Size = new System.Drawing.Size(178, 26);
            this.userReportsToolStripMenuItem.Text = "&User Report";
            this.userReportsToolStripMenuItem.Click += new System.EventHandler(this.userReportsToolStripMenuItem_Click);
            // 
            // cohortReportToolStripMenuItem1
            // 
            this.cohortReportToolStripMenuItem1.Name = "cohortReportToolStripMenuItem1";
            this.cohortReportToolStripMenuItem1.Size = new System.Drawing.Size(178, 26);
            this.cohortReportToolStripMenuItem1.Text = "&Cohort Report";
            this.cohortReportToolStripMenuItem1.Click += new System.EventHandler(this.cohortReportToolStripMenuItem1_Click);
            // 
            // toolStripUserName
            // 
            this.toolStripUserName.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripUserName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(251)))), ((int)(((byte)(251)))));
            this.toolStripUserName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.toolStripUserName.Enabled = false;
            this.toolStripUserName.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripUserName.Name = "toolStripUserName";
            this.toolStripUserName.Size = new System.Drawing.Size(200, 24);
            this.toolStripUserName.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // dELETEToolStripMenuItem
            // 
            this.dELETEToolStripMenuItem.Name = "dELETEToolStripMenuItem";
            this.dELETEToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // UserForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1075, 619);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.Name = "UserForm";
            this.Text = "Engage Evaluations";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.UserForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem userToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem evaluationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewOpenEvaluationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem administratorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createCohortsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem createReportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem userReportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createCohortToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openEvaluationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cohortReportToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem othersEvaluationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectEvaluatorsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem evaluationsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem createEvaluationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateDeleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dELETEToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem displayEvaluationsByCohortToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripUserName;
    }
}