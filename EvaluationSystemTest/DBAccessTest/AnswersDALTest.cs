﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EvaluationSystem.Model;
using EvaluationSystem.DBAccess;
using System.Collections.Generic;

namespace EvaluationSystemTest.DBAccessTest
{
    [TestClass]
    public class AnswersDALTest
    {
        /// <summary>
        /// Test Creates Answers
        /// </summary>
        [TestMethod]
        public void TestCreateAnswers()
        {
            //Create Inputs
            Answers answers = new Answers();
            Boolean results = false;
            List<Answers> answersList = new List<Answers>();
            int answerA = 5;
            int questionIDA = 6;
            int evaluationIDA = 18;

            //Create Answer List
            answers.Answer = answerA;
            answers.QuestionId = questionIDA;
            answers.EvaluationId = evaluationIDA;
            answersList.Add(answers);
            results = AnswersDAL.CreateAnswers(answersList);

            //Check Answer Insert Success
            Assert.IsTrue(results, "Answers list not created.");
        }
    }
}