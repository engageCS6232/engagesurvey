﻿using System;

namespace EvaluationSystem.Model
{
    public class Questions
    {
        public Questions()
        {

        }

        public int QuestionId { get; set; }
        public String Question { get; set; }
        public String Description { get; set; }
        public int TypeId { get; set; }
        public int CategoryId { get; set; }
    }
}
