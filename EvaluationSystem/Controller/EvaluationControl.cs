﻿using EvaluationSystem.DBAccess;
using EvaluationSystem.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluationSystem.Controller
{
    class EvaluationControl
    {
        public Employees getEmployeeData(String username)
        {
            return EmployeesDAL.getEmployeeData(username);
        }

        public List<Employees> getEmployeesWithoutCohorts()
        {
            return EmployeesDAL.GetEmployeesWithoutCohorts();
        }

        public Boolean ComparePassword(String username, String password)
        {
            return EmployeesDAL.ComparePassword(username, password);
        }

        public List<Employees> getEmployeeList(int cohortID, int employeeID, int supervisorID)
        {
            return EmployeesDAL.getEmployeeList(cohortID, employeeID, supervisorID);
        }

        public bool insertEmployeesSupervisor(int employeeID, int supervisorID, int stageID, int typeID)
        {
            return EvaluationsDAL.insertEmployeesSupervisor(employeeID, supervisorID, stageID, typeID);
        }

        public bool insertEmployeesCoworker(int employeeID, int coworkerID, int stageID, int typeID)
        {
            return EvaluationsDAL.insertEmployeesCoworker(employeeID, coworkerID, stageID, typeID);
        }

        public Evaluations getEvaluation(int evaluationID)
        {
            return EvaluationsDAL.getEvaluation(evaluationID);
        }

        public List<Evaluations> getEvaluationsOpenForEmployeeToAssign(int employeeID)
        {
            return EvaluationsDAL.getEvaluationsOpenForEmployeeToAssign(employeeID);
        }

        public List<Evaluations> getEvaluationsOpenWithEmployee(int employeeID, bool isSelf)
        {
            return EvaluationsDAL.getEvaluationsOpenWithEmployee(employeeID, isSelf);
        }

        public Boolean updateEvaluation(Evaluations oldEvaluation, Evaluations newEvaluation)
        {
            return EvaluationsDAL.updateEvaluation(oldEvaluation, newEvaluation);
        }

        public int CreateCohort(String cohortName, BindingList<Employees> cohortMemberList)
        {
            return CohortDAL.CreateCohort(cohortName, cohortMemberList);
        }


        public Boolean CreateEvaluationsForCohortAndMembers(CohortEvalTracker tracker)
        {
            return EvaluationsDAL.CreateEvaluationsForCohortAndMembers(tracker);
        }

        public List<Categories> getCategoriesByType(int typeID)
        {
            return CategoriesDAL.getCategoriesByType(typeID);
        }

        internal List<Questions> getQuestionsByType(int typeID)
        {
            return QuestionsDAL.getQuestionsByType(typeID);
        }

        public List<Evaluation> getEvaluationsByEmployee(int employeeID)
        {
            return EvaluationsDAL.GetEvaluationsForEmployee(employeeID);
        }

        public List<Evaluation> getSelfEvaluationsByEmployee(int employeeID)
        {
            return EvaluationsDAL.GetSelfEvaluationsForEmployee(employeeID);
        }

        public Evaluation GetSingleEvaluationByID(int evaluationIdInEvaluation)
        {
            return EvaluationsDAL.GetSingleEvaluationByID(evaluationIdInEvaluation);
        }

        public Boolean SaveAnswersAndUpdateEvaluationStatus(List<Answers> answers, Evaluation evaluation)
        {
            return EvaluationsDAL.SaveAnswersAndUpdateEvaluationStatus(answers, evaluation);
        }

        public BindingList<Evaluation> GetEvaluationsByCohortID(int cohortID)
        {
            return EvaluationsDAL.GetEvaluationByCohortID(cohortID);
        }

        public Employees GetEmployeeByID(int employeeID)
        {
            return EmployeesDAL.GetEmployeesByID(employeeID);
        }

        public List<Employees> GetMembersOfCohort(int cohortID)
        {
            return CohortDAL.GetMembersOfCohort(cohortID);
        }

        public List<Cohorts> GetCohortList()
        {
            return CohortDAL.GetCohortsList();
        }

        public List<Types> GetTypesList()
        {
            return TypesDAL.GetTypesList();
        }

        public List<Stage> GetStageList()
        {
            return StageDAL.GetStageList();
        }

        public String getEmployeeUserName(int employeeID)
        {
            return EmployeesDAL.getEmployeeUserName(employeeID);
        }

        public bool DeleteCohort(int cohortID)
        {
            return CohortDAL.DeleteCohort(cohortID);
        }

        public bool UpdateCohort(int cohortID, BindingList<Employees> employeesList)
        {
            return CohortDAL.UpdateCohort(cohortID, employeesList);
        }

        public void DidCohortAnswer(int cohortID) {
            AnswersDAL.DidCohortAnswer(cohortID);
        }

        public bool HasAllEvauatorsSelected(int evaluationID)
        {
            return EvaluationsDAL.HasAllEvauatorsSelected(evaluationID);
        }
    }
}
