﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EvaluationSystem.View
{
    public partial class GraphReport : Form
    {
        private int cohortID;
        private int typeID;

        public GraphReport()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Loads the data.
        /// </summary>
        /// <param name="cohortID"></param>
        /// <param name="typeID"></param>
        public void load(int cohortID, int typeID)
        {
            this.cohortID = cohortID;
            this.typeID = typeID;
        }

        /// <summary>
        /// Loads the graph.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GraphReport_Load(object sender, EventArgs e)
        {
            try
            {
                //Defines the report parameters.
                ReportParameter[] parameters = new ReportParameter[2];
                parameters[0] = new ReportParameter("cohortID", this.cohortID.ToString());
                parameters[1] = new ReportParameter("typeID", this.typeID.ToString());

                //Sets the report parameters.
                this.reportViewer1.LocalReport.SetParameters(parameters);

                //Gets the data from the dataset.
                this.graphReportTableAdapter.Fill(this.reports.graphReport, this.cohortID, this.typeID);

                this.reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }
    }
}
