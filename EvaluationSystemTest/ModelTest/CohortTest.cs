﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EvaluationSystem.Model;

namespace EvaluationSystemTest.ModelTest
{
    [TestClass]
    public class CohortTest
    {
        private Cohorts cohorts = new Cohorts();
        private int cohortID = 1;
        private string cohortName = "Test Name";
        private string cohortInfo;

        /// <summary>
        /// Test for newly created Cohort, all gets show return null
        /// </summary>
        [TestMethod]
        public void TestNullCohort()
        {
            //Create Cohort
            this.cohortInfo = "(0)";
            
            //Return Cohort Data
            Assert.AreEqual(0, this.cohorts.CohortID, 0, "Cohorts's CohortID not 0.");
            Assert.IsNull(this.cohorts.CohortName, "Cohorts's CohortName not null.");
            Assert.AreEqual(this.cohortInfo, cohorts.CohortInfo, "Cohorts's CohortInfo not correct.");
        }

        /// <summary>
        /// Test for set and get of an cohorts [Cohorts class]
        /// </summary>
        [TestMethod]
        public void TestActiveCohort()
        {
            //Create Cohort
            this.cohorts.CohortID = this.cohortID;
            this.cohorts.CohortName = this.cohortName;
            this.cohortInfo = this.cohortName + "(" + this.cohortID + ")";

            //Return Cohort Data
            Assert.AreEqual(this.cohortID, this.cohorts.CohortID, 0, "Cohorts's CohortID not " + this.cohortID + ".");
            Assert.AreEqual(this.cohortName, this.cohorts.CohortName, "Cohorts's CohortName not " + this.cohortName + ".");
            Assert.AreEqual(this.cohortInfo, this.cohorts.CohortInfo, "Cohorts's CohortInfo not correct.");
        }
    }
}