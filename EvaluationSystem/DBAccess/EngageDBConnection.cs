﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluationSystem.DBAccess
{
    /// <summary>
    /// Connect to CS6232-g3 Database
    /// </summary>
    public static class EngageDBConnection
    {
        /// <summary>
        /// Tries connecting to the Database
        /// </summary>
        public static SqlConnection GetConnection()
        {
            try
            {
                String connectionString =
                    "Data Source=localhost;Initial Catalog=CS6232-g3;" +
                    "Integrated Security=True";
                SqlConnection connection = new SqlConnection(connectionString);
                return connection;
            }
            catch (SqlException)
            {
                throw;
            }
        }
    }
}