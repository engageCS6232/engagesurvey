﻿using EvaluationSystem.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Data.SqlClient;

namespace EvaluationSystem.DBAccess
{
    public class AnswersDAL
    {
        public static bool CreateAnswers(List<Answers> answers)
        {
            bool insertSuccessful = true;
            String insertStatement =
                "INSERT INTO answers (answer, questionID, evaluationID) " +
                "VALUES(@answer, @questionID, @evaluationID)";

            try
            {
                using (SqlConnection connection = EngageDBConnection.GetConnection())
                {
                    using (SqlCommand insertCommand = new SqlCommand(insertStatement, connection))
                    {
                        connection.Open();

                        insertCommand.Parameters.Add("@answer", SqlDbType.Int);
                        insertCommand.Parameters.Add("@questionID", SqlDbType.Int);
                        insertCommand.Parameters.Add("@evaluationID", SqlDbType.Int);

                        foreach (Answers current in answers)
                        {
                            insertCommand.Connection = connection;
                            insertCommand.CommandText = insertStatement;
                            insertCommand.Parameters["@answer"].Value = current.Answer;
                            insertCommand.Parameters["@questionID"].Value = current.QuestionId;
                            insertCommand.Parameters["@evaluationID"].Value = current.EvaluationId;

                            int affectedRows = insertCommand.ExecuteNonQuery();
                            if (affectedRows < 1)
                            {
                                insertSuccessful = false;
                            }
                        }
                    }
                }
            }
            catch (DuplicateKeyException)
            {
                throw;
            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            return insertSuccessful;
        }

        public static void DidCohortAnswer(int cohortID)
        {
            String selectStatement = "SELECT * from answers a" +
                                     " JOIN evaluations e" +
                                     " ON  a.evaluationID = e.evaluationID" +
                                     " JOIN selected s " +
                                     " ON e.evaluatorID = s.employeeID" +
                                     " WHERE s.cohortID = @cohortID";
            try
            {
                using (SqlConnection connection = EngageDBConnection.GetConnection())
                {
                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        selectCommand.Parameters.AddWithValue("@cohortID", cohortID);
                        connection.Open();
                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                throw new Exception("You cannot update/delete a cohort that already submitted answers!");
                            }
                        }
                    }
                }
            }
            catch (SqlException)
            {

                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}