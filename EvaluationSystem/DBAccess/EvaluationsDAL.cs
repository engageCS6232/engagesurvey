﻿using EvaluationSystem.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Linq;
using System.Data.SqlClient;
using System.Transactions;

namespace EvaluationSystem.DBAccess
{
    /// <summary>
    /// This the DAL class that deals with evaluations. 
    /// </summary>
    public class EvaluationsDAL
    {

        /// <summary>
        /// Gets the evaluation.  
        /// </summary>
        /// <returns>A list of Open Evalutations, where employee is a participant.</returns>
        public static Evaluations getEvaluation(int evaluationID)
        {
            Evaluations evaluation = new Evaluations();
            string selectStatement =
                "SELECT evaluationID, stageID, roleId, completionDate, typeId, participantID, evaluatorID " +
                "FROM evaluations " +
                "WHERE Evaluations.evaluationID= @evaluationID";
            try
            {
                using (SqlConnection connection = EngageDBConnection.GetConnection())
                {
                    connection.Open();

                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        selectCommand.Parameters.AddWithValue("@evaluationID", evaluationID);
                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            int evalID = reader.GetOrdinal("evaluationID");
                            int sID = reader.GetOrdinal("stageID");
                            int rID = reader.GetOrdinal("roleID");
                            int cDate = reader.GetOrdinal("completionDate");
                            int tID = reader.GetOrdinal("typeID");
                            int pID = reader.GetOrdinal("participantID");
                            int eID = reader.GetOrdinal("evaluatorID");

                            while (reader.Read())
                            {
                                evaluation.EvaluationID = reader.GetInt32(evalID);
                                evaluation.StageID = reader.GetInt32(sID);
                                if (!reader.IsDBNull(sID))
                                {
                                    evaluation.RoleID = reader.GetInt32(rID);
                                }
                                else
                                {
                                    evaluation.RoleID = null;
                                }
                                if (!reader.IsDBNull(cDate))
                                {
                                    evaluation.CompletionDate = reader.GetDateTime(cDate);
                                }
                                else
                                {
                                    evaluation.CompletionDate = null;
                                }
                                evaluation.TypeID = reader.GetInt32(tID);
                                evaluation.ParticipantID = reader.GetInt32(pID);
                                if (!reader.IsDBNull(eID))
                                {
                                    evaluation.EvaluatorID = reader.GetInt32(eID);
                                }
                                else
                                {
                                    evaluation.EvaluatorID = null;
                                }
                            }
                        }
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            return evaluation;
        }

        /// <summary>
        /// 
        /// finally block.
        /// </summary>
        /// <returns>a list of Open Evalutation, where employee is participant</returns>
        public static List<Evaluations> getEvaluationsOpenForEmployeeToAssign(int employeeID)
        {
            List<Evaluations> evaluationList = new List<Evaluations>();

            string selectStatement =
                "SELECT * FROM ( " +
                "SELECT evaluations.evaluationID, " +
                "evaluations.stageID,  " +
                "evaluations.typeID, " +
                "Count ( Concat (evaluations.stageID, evaluations.typeID)) OVER(PARTITION BY evaluations.stageID, " +
                "evaluations.typeID) AS Number " +
                "FROM evaluations " +
                "WHERE evaluations.completionDate IS NULL " +
                "AND evaluations.participantID = @employeeID  " +
                ") as A " +
                "WHERE A.Number = 1 " +
                "ORDER BY A.evaluationID;";
            try
            {
                using (SqlConnection connection = EngageDBConnection.GetConnection())
                {
                    connection.Open();

                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        selectCommand.Parameters.AddWithValue("@employeeID", employeeID);
                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {

                            int evalID = reader.GetOrdinal("evaluationID");
                            int sID = reader.GetOrdinal("stageID");
                            int tID = reader.GetOrdinal("typeID");

                            while (reader.Read())
                            {
                                Evaluations evaluations = new Evaluations();

                                evaluations.EvaluationID = reader.GetInt32(evalID);
                                evaluations.StageID = reader.GetInt32(sID);
                                evaluations.TypeID = reader.GetInt32(tID);
                                evaluationList.Add(evaluations);
                            }
                        }
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            return evaluationList;
        }

        /// <summary>
        ///  Inserts the Supervisor evaluation into the evaluations table for provided stage, type, and employee
        /// </summary>
        /// <param name="employeeID"></param>
        /// <param name="supervisorID"></param>
        /// <param name="coworker"></param>
        /// <param name="evaluationID"></param>
        /// <returns></returns>
        public static bool insertEmployeesSupervisor(int employeeID, int supervisorID, int stageID, int typeID)
        {
            Boolean result;

            String insertStatement =
              "INSERT INTO evaluations " +
              "(stageID, roleID, typeId, participantID, evaluatorID) VALUES " +
              "(@stageID, 2, @typeId, @employeeID, @supervisorID);";

            try
            {
                using (SqlConnection connection = EngageDBConnection.GetConnection())
                {
                    connection.Open();

                    using (SqlCommand insertCommand = new SqlCommand(insertStatement, connection))
                    {
                        insertCommand.Parameters.AddWithValue("@employeeID", employeeID);
                        insertCommand.Parameters.AddWithValue("@supervisorID", supervisorID);
                        insertCommand.Parameters.AddWithValue("@stageID", stageID);
                        insertCommand.Parameters.AddWithValue("@typeID", typeID);
                        int count = insertCommand.ExecuteNonQuery();
                        if (count > 0)
                        {
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        ///  Inserts the Co-Worker evaluation into the evaluations table for provided stage, type, and employee
        /// </summary>
        /// <param name="employeeID"></param>
        /// <param name="supervisorID"></param>
        /// <param name="coworker"></param>
        /// <param name="evaluationID"></param>
        /// <returns></returns>
        public static bool insertEmployeesCoworker(int employeeID, int coworkerID, int stageID, int typeID)
        {
            Boolean result;

            String insertStatement =
              "INSERT INTO evaluations " +
              "(stageID, roleID, typeId, participantID, evaluatorID) VALUES " +
              "(@stageID, 3, @typeId, @employeeID, @coworkerID);";

            try
            {
                using (SqlConnection connection = EngageDBConnection.GetConnection())
                {
                    connection.Open();

                    using (SqlCommand insertCommand = new SqlCommand(insertStatement, connection))
                    {
                        insertCommand.Parameters.AddWithValue("@employeeID", employeeID);
                        insertCommand.Parameters.AddWithValue("@coworkerID", coworkerID);
                        insertCommand.Parameters.AddWithValue("@stageID", stageID);
                        insertCommand.Parameters.AddWithValue("@typeID", typeID);
                        int count = insertCommand.ExecuteNonQuery();
                        if (count > 0)
                        {
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        ///  Open Evaluations that a user was seclected for as Self, Co-Worker, or Supervisor.
        /// </summary>
        /// <param name="employeeID"></param>
        /// <returns></returns>
        public static List<Evaluations> getEvaluationsOpenWithEmployee(int employeeID, bool isSelf)
        {
            List<Evaluations> evaluationList = new List<Evaluations>();
            string limit = "";

            if (isSelf)
            {
                limit = "AND  employees.employeeID  =  evaluations.evaluatorID ";
            }
            else
            {
                limit = "AND  employees.employeeID  !=  evaluations.evaluatorID ";
            }
            string selectStatement =
                "SELECT evaluations.evaluationID, " +
                "evaluations.stageID, " +
                "evaluations.roleID, " +
                "roles.roleName, " +
                "evaluations.typeID, " +
                "evaluations.participantID, " +
                "evaluations.evaluatorID, " +
                "Concat(evaluator.lName,', ',evaluator.fName,' (',evaluations.evaluatorID,')') as evaluatorName, " +
                "Concat(employees.lName,', ',employees.fName,' (',evaluations.participantID,')') as participantName " +
                "FROM evaluations , roles , employees , employees as evaluator " +
                "WHERE evaluations.completionDate  IS NULL " +
                "AND evaluations.evaluatorID = @employeeID " +
                "AND evaluations.roleID = roles.roleId " +
                "AND employees.employeeID = evaluations.participantID " +
                "AND evaluator.employeeID = evaluations.evaluatorID " +
                limit +
                "ORDER BY evaluations.evaluationID";
            try
            {
                using (SqlConnection connection = EngageDBConnection.GetConnection())
                {
                    connection.Open();

                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        selectCommand.Parameters.AddWithValue("@employeeID", employeeID);
                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {

                            int evalID = reader.GetOrdinal("evaluationID");
                            int sID = reader.GetOrdinal("stageID");
                            int rID = reader.GetOrdinal("roleID");
                            int rName = reader.GetOrdinal("roleName");
                            int tID = reader.GetOrdinal("typeID");
                            int pID = reader.GetOrdinal("participantID");
                            int pName = reader.GetOrdinal("participantName");
                            int eID = reader.GetOrdinal("evaluatorID");
                            int eName = reader.GetOrdinal("evaluatorName");

                            while (reader.Read())
                            {
                                Evaluations evaluations = new Evaluations();

                                if (!reader.IsDBNull(evalID))
                                {
                                    evaluations.EvaluationID = reader.GetInt32(evalID);
                                }
                                else
                                {
                                    evaluations.EvaluationID = 0;
                                }
                                if (!reader.IsDBNull(sID))
                                {
                                    evaluations.StageID = reader.GetInt32(sID);
                                }
                                else
                                {
                                    evaluations.StageID = 0;
                                }

                                if (!reader.IsDBNull(sID))
                                {
                                    evaluations.RoleID = reader.GetInt32(rID);
                                }
                                else
                                {
                                    evaluations.RoleID = null;
                                }
                                evaluations.RoleName = reader.GetString(rName);
                                if (!reader.IsDBNull(tID))
                                {
                                    evaluations.TypeID = reader.GetInt32(tID);
                                }
                                else
                                {
                                    evaluations.TypeID = 0;
                                }
                                if (!reader.IsDBNull(pID))
                                {
                                    evaluations.ParticipantID = reader.GetInt32(pID);
                                }
                                else
                                {
                                    evaluations.ParticipantID = 0;
                                }
                                if (!reader.IsDBNull(eID))
                                {
                                    evaluations.EvaluatorID = reader.GetInt32(eID);
                                }
                                else
                                {
                                    evaluations.EvaluatorID = null;
                                }
                                evaluations.EvaluatorName = reader.GetString(eName);
                                evaluations.ParticipantName = reader.GetString(pName);
                                evaluationList.Add(evaluations);
                            }
                        }
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            return evaluationList;
        }

        /// <summary>
        /// This method uses try/catch/finally and placed closing of the resources (connection, reader) in the 
        /// finally block.
        /// </summary>
        /// <returns>true if completed</returns>
        public static bool updateEvaluation(Evaluations oldEvaluation, Evaluations newEvaluation)
        {
            string updateStatement =
             "UPDATE evaluations " +
             "SET " +
             "evaluations.stageID = @newStageID, " +
             "evaluations.roleID = @newRoleID, " +
             "evaluations.typeID = @newTypeID, " +
             "evaluations.participantID = @newParticipantID, " +
             "evaluations.evaluatorID = @newEvaluatorID " +
             "WHERE " +
             "evaluations.evaluationID = @oldEvaluationID " +
             "AND evaluations.stageID = @oldStageID " +
             "AND evaluations.roleID = @oldRoleID " +
             "AND evaluations.typeID = @oldTypeID " +
             "AND evaluations.participantID = @oldParticipantID " +
             "AND evaluations.evaluatorID = @oldEvaluatorID";
            try
            {
                using (SqlConnection connection = EngageDBConnection.GetConnection())
                {
                    connection.Open();

                    using (SqlCommand updateCommand = new SqlCommand(updateStatement, connection))
                    {
                        updateCommand.Parameters.AddWithValue("@newEvaluationID", newEvaluation.EvaluationID);
                        updateCommand.Parameters.AddWithValue("@newStageID", newEvaluation.StageID);
                        if (newEvaluation.RoleID != null)
                        {
                            updateCommand.Parameters.AddWithValue("@newRoleID", newEvaluation.RoleID);
                        }
                        else
                        {
                            updateCommand.Parameters.AddWithValue("@newRoleID", System.DBNull.Value);
                        }
                        updateCommand.Parameters.AddWithValue("@newTypeID", newEvaluation.TypeID);
                        updateCommand.Parameters.AddWithValue("@newParticipantID", newEvaluation.ParticipantID);
                        if (newEvaluation.EvaluatorID != null)
                        {
                            updateCommand.Parameters.AddWithValue("@newEvaluatorID", newEvaluation.EvaluatorID);
                        }
                        else
                        {
                            updateCommand.Parameters.AddWithValue("@newEvaluatorID", System.DBNull.Value);
                        }

                        updateCommand.Parameters.AddWithValue("@oldEvaluationID", oldEvaluation.EvaluationID);
                        updateCommand.Parameters.AddWithValue("@oldStageID", oldEvaluation.StageID);
                        if (oldEvaluation.RoleID != null)
                        {
                            updateCommand.Parameters.AddWithValue("@oldRoleID", oldEvaluation.RoleID);
                        }
                        else
                        {
                            updateCommand.Parameters.AddWithValue("@oldRoleID", System.DBNull.Value);
                        }
                        updateCommand.Parameters.AddWithValue("@oldTypeID", oldEvaluation.TypeID);
                        updateCommand.Parameters.AddWithValue("@oldParticipantID", oldEvaluation.ParticipantID);
                        if (oldEvaluation.EvaluatorID != null)
                        {
                            updateCommand.Parameters.AddWithValue("@oldEvaluatorID", oldEvaluation.EvaluatorID);
                        }
                        else
                        {
                            updateCommand.Parameters.AddWithValue("@oldEvaluatorID", System.DBNull.Value);
                        }
                        int count = updateCommand.ExecuteNonQuery();
                        if (count > 0)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        ///     Creates self evaluations for each member of a given cohort
        /// </summary>
        /// <param name="cohortID"></param>
        /// <param name="typeID"></param>
        /// <param name="stageID"></param>
        /// <returns></returns>
        public static bool CreateSelfEvaluations(int cohortID, int typeID, int stageID)
        {
            bool insertSuccessful = true;
            String insertStatement =
                "INSERT INTO evaluations (stageID, roleID, completionDate, typeID, participantID, evaluatorID)" +
                " VALUES(@stageID, @roleID, @completionDate, @typeID, @participantID, @evaluatorID)";
            List<Employees> cohortMembers = CohortDAL.GetMembersOfCohort(cohortID);

            try
            {
                using (SqlConnection connection = EngageDBConnection.GetConnection())
                {
                    using (SqlCommand insertCommand = new SqlCommand(insertStatement, connection))
                    {
                        connection.Open();

                        insertCommand.Parameters.Add("@stageID", SqlDbType.Int);
                        insertCommand.Parameters.Add("@roleID", SqlDbType.Int);
                        insertCommand.Parameters.Add("@completionDate", SqlDbType.DateTime);
                        insertCommand.Parameters.Add("@typeID", SqlDbType.Int);
                        insertCommand.Parameters.Add("@participantID", SqlDbType.Int);
                        insertCommand.Parameters.Add("@evaluatorID", SqlDbType.Int);

                        foreach (Employees current in cohortMembers)
                        {
                            insertCommand.Connection = connection;
                            insertCommand.CommandText = insertStatement;
                            insertCommand.Parameters["@stageID"].Value = stageID;
                            insertCommand.Parameters["@roleID"].Value = 1;
                            insertCommand.Parameters["@completionDate"].Value = DBNull.Value;
                            insertCommand.Parameters["@typeID"].Value = typeID;
                            insertCommand.Parameters["@participantID"].Value = current.EmployeeID;
                            insertCommand.Parameters["@evaluatorID"].Value = current.EmployeeID;

                            int affectedRows = insertCommand.ExecuteNonQuery();
                            if (affectedRows < 1)
                            {
                                insertSuccessful = false;
                            }
                        }
                    }
                }
            }
            catch (DuplicateKeyException)
            {
                throw;
            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            return insertSuccessful;
        }

        /// <summary>
        /// Uses a single TransactionScope class enclosed in a using statement to make sure that if any exception occur that the changes will rollback
        /// </summary>
        /// <param name="cohort_eval_entry"></param>
        /// <returns></returns>
        public static bool CreateEvaluationsForCohortAndMembers(CohortEvalTracker cohort_eval_entry)
        {
            bool successCohortAndEmployeeInsert = true;
            try
            {
                using (TransactionScope transaction = new TransactionScope())
                {
                    bool insertCohortEvalSuccess = CohortEvalTrackerDAL.CreateEvalForCohort(cohort_eval_entry.cohortID,
                        cohort_eval_entry.typeID,
                        cohort_eval_entry.stageID,
                        cohort_eval_entry.startDate,
                        cohort_eval_entry.endDate);

                    bool insertEmployeesEvalSuccess = EvaluationsDAL.CreateSelfEvaluations(cohort_eval_entry.cohortID,
                        cohort_eval_entry.typeID,
                        cohort_eval_entry.stageID);

                    if (!insertEmployeesEvalSuccess || !insertCohortEvalSuccess)
                    {
                        successCohortAndEmployeeInsert = false;
                        throw new Exception("Assigment of Cohort Evaluation was not successful");
                    }

                    // If there are any exceptions the resources are disposed 
                    // and the changes will rollback after the Complete method is called. If no exception occur then all the changes will be Commit
                    transaction.Complete();
                }
            }
            catch (DuplicateKeyException)
            {
                throw;
            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {

                throw;
            }
            return successCohortAndEmployeeInsert;
        }

        public static List<Evaluation> GetEvaluationsForEmployee(int employeeID)
        {
            List<Evaluation> evalList = new List<Evaluation>();
            string selectionString = "SELECT evaluationID, ev.stageID, ev.typeID, roleName, fName, lName, participantID, evaluatorID " +
                "FROM evaluations ev  " +
                "JOIN roles r ON r.roleID = ev.roleID " +
                "JOIN employees em ON em.employeeID = ev.participantID " +
                "JOIN selected se ON se.employeeID = em.employeeID " +
                "JOIN cohort c ON se.cohortID = c.cohortID " +
                "JOIN cohort_eval_tracker cet ON cet.cohortEvalID = c.cohortID " +
                "WHERE evaluatorID = @employeeID AND completionDate IS NULL AND participantID != @employeeID";
            try
            {
                using (SqlConnection connection = EngageDBConnection.GetConnection())
                {
                    using (SqlCommand selectCommand = new SqlCommand(selectionString, connection))
                    {
                        connection.Open();
                        selectCommand.Parameters.AddWithValue("@employeeID", employeeID);

                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Evaluation eval = new Evaluation();
                                eval.StageID = Convert.ToInt32(reader["stageID"]);
                                eval.TypeID = Convert.ToInt32(reader["typeID"]);
                                eval.EvaluationID = Convert.ToInt32(reader["evaluationID"]);

                                eval.RoleName = reader["roleName"].ToString();
                                eval.ParticipantID = Convert.ToInt32(reader["participantID"]);
                                eval.ParticipantLastName = reader["lName"].ToString();
                                eval.ParticipantFirstName = reader["fName"].ToString();


                                evalList.Add(eval);
                            }
                        }
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }

            return evalList;
        }

        public static List<Evaluation> GetSelfEvaluationsForEmployee(int employeeID)
        {
            List<Evaluation> evalList = new List<Evaluation>();
            string selectionString = "SELECT evaluationID, ev.stageID, ev.typeID, roleName, fName, lName, participantID, evaluatorID " +
                "FROM evaluations ev  " +
                "JOIN roles r ON r.roleID = ev.roleID " +
                "JOIN employees em ON em.employeeID = ev.participantID " +
                "JOIN selected se ON se.employeeID = em.employeeID " +
                "JOIN cohort c ON se.cohortID = c.cohortID " +
                "JOIN cohort_eval_tracker cet ON cet.cohortEvalID = c.cohortID " +
                "WHERE evaluatorID = @employeeID AND completionDate IS NULL AND participantID = @employeeID";
            try
            {
                using (SqlConnection connection = EngageDBConnection.GetConnection())
                {
                    using (SqlCommand selectCommand = new SqlCommand(selectionString, connection))
                    {
                        connection.Open();
                        selectCommand.Parameters.AddWithValue("@employeeID", employeeID);

                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Evaluation eval = new Evaluation();
                                eval.StageID = Convert.ToInt32(reader["stageID"]);
                                eval.TypeID = Convert.ToInt32(reader["typeID"]);
                                eval.EvaluationID = Convert.ToInt32(reader["evaluationID"]);

                                eval.RoleName = reader["roleName"].ToString();
                                eval.ParticipantID = Convert.ToInt32(reader["participantID"]);
                                eval.ParticipantLastName = reader["lName"].ToString();
                                eval.ParticipantFirstName = reader["fName"].ToString();


                                evalList.Add(eval);
                            }
                        }
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }

            return evalList;
        }

        public static Evaluation GetSingleEvaluationByID(int evaluationID)
        {
            String selectString = "SELECT stageID, roleID, completionDate, typeID, participantID, evaluatorID" +
                                  " FROM evaluations" +
                                  " WHERE evaluationID = @evaluationID" +
                                  " AND completionDate IS NULL";
            Evaluation evaluation = new Evaluation();
            try
            {
                using (SqlConnection connection = EngageDBConnection.GetConnection())
                {
                    using (SqlCommand selectCommand = new SqlCommand(selectString, connection))
                    {
                        connection.Open();
                        selectCommand.Parameters.AddWithValue("@evaluationID", evaluationID);

                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {

                            int stageIdOrd = reader.GetOrdinal("stageID");
                            int roleIDORd = reader.GetOrdinal("roleID");
                            int completionDateOrd = reader.GetOrdinal("completionDate");
                            int typeIDOrd = reader.GetOrdinal("typeID");
                            int participantIDOrd = reader.GetOrdinal("participantID");
                            int evaluatorID = reader.GetOrdinal("evaluatorID");

                            reader.Read();

                            evaluation.StageID = reader.GetInt32(stageIdOrd);
                            evaluation.RoleID = reader.GetInt32(roleIDORd);
                            if (reader[completionDateOrd] == DBNull.Value)
                            {
                                evaluation.CompletionDate = null;
                            }
                            else
                            {
                                evaluation.CompletionDate = Convert.ToDateTime(reader["completionDate"]);
                            }

                            evaluation.TypeID = reader.GetInt32(typeIDOrd);
                            evaluation.ParticipantID = reader.GetInt32(participantIDOrd);
                            evaluation.EvaluatorID = reader.GetInt32(evaluatorID);
                        }
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            return evaluation;
        }

        /// <summary>
        /// Saves answers to a given evaluationID
        /// </summary>
        /// <param name="answersList"></param>
        /// <returns></returns>
        public static Boolean SavedAnswers(List<Answers> answersList)
        {
            Boolean insertSuccess = true;
            string completionDateInsertStatement = "INSERT INTO answers" +
                                                     " VALUES (@answer," +
                                                     " @questionID," +
                                                     " @evaluationID)";
            try
            {
                using (SqlConnection connection = EngageDBConnection.GetConnection())
                {
                    using (SqlCommand insertCmd = new SqlCommand())
                    {
                        insertCmd.Connection = connection;
                        connection.Open();

                        // Check if completionDate still null
                        insertCmd.Parameters.Add("@answer", SqlDbType.Int);
                        insertCmd.Parameters.Add("@questionID", SqlDbType.Int);
                        insertCmd.Parameters.Add("@evaluationID", SqlDbType.Int);
                        try
                        {
                            foreach (Answers current in answersList)
                            {

                                insertCmd.CommandText = completionDateInsertStatement;
                                insertCmd.Parameters["@answer"].Value = current.Answer;
                                insertCmd.Parameters["@questionID"].Value = current.QuestionId;
                                insertCmd.Parameters["@evaluationID"].Value = current.EvaluationId;

                                int completionDateRow = (Int32)insertCmd.ExecuteNonQuery();

                                if (completionDateRow < 1)
                                {
                                    insertSuccess = false;
                                }
                            }
                        }
                        catch (SqlException)
                        {
                            throw;
                        }
                        catch (Exception)
                        {
                            throw;
                        }
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            return insertSuccess;
        }

        /// <summary>
        /// Updates the completion date of an evaluation
        /// </summary>
        /// <param name="updateEvaluation"></param>
        /// <returns></returns>
        public static Boolean UpdateEvaluationCompletionDate(Evaluation updateEvaluation)
        {
            String updateStatement = "UPDATE evaluations" +
                                     " SET completionDate = GETDATE()" +
                                     " WHERE evaluationID = @evaluationID AND" +
                                     " stageID = @stageID AND" +
                                     " roleID = @roleID AND" +
                                     " completionDate IS NULL AND" +
                                     " typeID = @typeID AND" +
                                     " participantID = @participantID AND" +
                                     " evaluatorID = @evaluatorID";

            try
            {
                using (SqlConnection connection = EngageDBConnection.GetConnection())
                {

                    using (SqlCommand cmd = new SqlCommand(updateStatement, connection))
                    {

                        cmd.Parameters.AddWithValue("@evaluationID", updateEvaluation.EvaluationID);
                        cmd.Parameters.AddWithValue("@stageID", updateEvaluation.StageID);
                        cmd.Parameters.AddWithValue("@roleID", updateEvaluation.RoleID);
                        cmd.Parameters.AddWithValue("@typeID", updateEvaluation.TypeID);
                        cmd.Parameters.AddWithValue("@participantID", updateEvaluation.ParticipantID);
                        cmd.Parameters.AddWithValue("@evaluatorID", updateEvaluation.EvaluatorID);

                        connection.Open();
                        int count = cmd.ExecuteNonQuery();
                        if (count < 1)
                        {
                            throw new Exception("This evaluation has been deleted or changed since it was last retrieved");
                        }
                    }
                }
            }
            catch (SqlException)
            {

                throw;
            }
            catch (Exception)
            {
                throw;
            }

            return false;
        }

        /// <summary>
        /// Updates the completion date of an evaluation and saves the answers to a given evaluation.
        /// </summary>
        /// <param name="answers"></param>
        /// <param name="updateEvaluation"></param>
        /// <returns></returns>
        public static Boolean SaveAnswersAndUpdateEvaluationStatus(List<Answers> answers, Evaluation updateEvaluation)
        {
            try
            {
                using (TransactionScope transaction = new TransactionScope())
                {
                    EvaluationsDAL.UpdateEvaluationCompletionDate(updateEvaluation);

                    EvaluationsDAL.SavedAnswers(answers);

                    // If any of the above fails then the changes will be reverted back because they are enclosed in a TransactionScope using statement
                    transaction.Complete();
                }
            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            return false;
        }

        public static BindingList<Evaluation> GetEvaluationByCohortID(int cohortID)
        {
            BindingList<Evaluation> evalList = new BindingList<Evaluation>();

            string selectStatement = "SELECT evaluationID, e.stageID, e.typeID, roleName, evaluatorID, participantID, completionDate, endDate " +
                "FROM cohort_eval_tracker cet JOIN selected s ON s.employeeID = cet.cohortEvalID " +
                "JOIN evaluations e ON e.participantID = s.employeeID " +
                "JOIN roles r ON r.roleID = e.roleID " +
                "JOIN cohort c ON s.cohortID = c.cohortID " +
                "WHERE c.cohortID = @cohortID " +
                "ORDER BY endDate";
            try
            {
                using (SqlConnection connection = EngageDBConnection.GetConnection())
                {
                    connection.Open();
                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        selectCommand.Parameters.AddWithValue("@cohortID", cohortID);

                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Evaluation eval = new Evaluation();
                                eval.EvaluationID = Convert.ToInt32(reader["evaluationID"]);
                                eval.StageID = Convert.ToInt32(reader["stageID"]);
                                eval.TypeID = Convert.ToInt32(reader["typeID"]);
                                eval.RoleName = reader["roleName"].ToString();
                                eval.DueDate = (DateTime)reader["endDate"];
                                if (reader["completionDate"] == DBNull.Value)
                                {
                                    eval.CompletionDate = null;
                                }
                                else
                                {
                                    eval.CompletionDate = (DateTime)reader["completionDate"];
                                }
                                eval.EvaluatorID = Convert.ToInt32(reader["evaluatorID"]);
                                eval.ParticipantID = Convert.ToInt32(reader["participantID"]);
                                evalList.Add(eval);
                            }
                        }
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }

            return evalList;
        }

        public static Boolean HasAllEvauatorsSelected(int evaluationID)
        {
            String selectString = "SELECT DISTINCT COUNT(evaluations.roleID) AS counts, evaluations.roleID "+
                                    "FROM evaluations, "+ 
	                                "(SELECT evaluations.typeID, evaluations.stageID, evaluations.participantID "+ 
	                                 "FROM evaluations "+
                                     "WHERE evaluationID=@evaluationID) AS a " +
                                "WHERE evaluations.typeID =a.typeID "+
                                "AND evaluations.stageID=a.stageID "+
                                "AND evaluations.participantID =a.participantID "+
                                "AND evaluations.roleID IN (SELECT roles.roleID FROM roles) "+
                                "GROUP BY evaluations.roleID "+
                                "ORDER BY evaluations.roleID";
            Boolean evaluation =true;
            int count = 0;
            String conact = "";
            try
            {
                using (SqlConnection connection = EngageDBConnection.GetConnection())
                {
                    using (SqlCommand selectCommand = new SqlCommand(selectString, connection))
                    {
                        connection.Open();
                        selectCommand.Parameters.AddWithValue("@evaluationID", evaluationID);

                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {

                            int countsOrd = reader.GetOrdinal("counts");
                            int roleIDORd = reader.GetOrdinal("roleID");

                            while (reader.Read())
                            {
                                conact = conact + reader.GetInt32(roleIDORd).ToString();
                                if (reader.GetInt32(countsOrd) > 1 || reader.GetInt32(countsOrd) <= 0)
                                {
                                    evaluation = false;
                                }

                                count++;
                            }

                            if (count < 3 && conact !="123")
                            {
                                evaluation = false;
                            }
                        }
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            return evaluation;
        }
    }
}
