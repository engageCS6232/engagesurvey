﻿namespace EvaluationSystem.View
{
    partial class EvalReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.evaluationReportBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.reports = new EvaluationSystem.Reports();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.evaluationReportTableAdapter = new EvaluationSystem.ReportsTableAdapters.evaluationReportTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.evaluationReportBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reports)).BeginInit();
            this.SuspendLayout();
            // 
            // evaluationReportBindingSource
            // 
            this.evaluationReportBindingSource.DataMember = "evaluationReport";
            this.evaluationReportBindingSource.DataSource = this.reports;
            // 
            // reports
            // 
            this.reports.DataSetName = "Reports";
            this.reports.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // reportViewer1
            // 
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "EvalReportDataset";
            reportDataSource1.Value = this.evaluationReportBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "EvaluationSystem.Reports.EvalReport.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 0);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(924, 537);
            this.reportViewer1.TabIndex = 0;
            // 
            // evaluationReportTableAdapter
            // 
            this.evaluationReportTableAdapter.ClearBeforeFill = true;
            // 
            // EvalReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(924, 537);
            this.Controls.Add(this.reportViewer1);
            this.Name = "EvalReport";
            this.Text = "Employee Evaluation Report";
            this.Load += new System.EventHandler(this.EvalReport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.evaluationReportBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reports)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource evaluationReportBindingSource;
        private Reports reports;
        private ReportsTableAdapters.evaluationReportTableAdapter evaluationReportTableAdapter;
    }
}