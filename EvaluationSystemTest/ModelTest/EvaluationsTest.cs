﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EvaluationSystem.Model;

namespace EvaluationSystemTest.ModelTest
{
    [TestClass]
    public class EvaluationsTest
    {
        //Test Evaluations
        private Evaluations evaluations = new Evaluations();
        private int evaluationID = 1;
        private int stageID = 1;
        private int? roleID = 1;
        private DateTime? completionDate = DateTime.Now.Date;
        private int typeID = 1;
        private int participantID = 1;
        private int? evaluatorID = 1;
        private string roleName = "Test Role";
        private string evaluatorName = "Test Evaluator";
        private string participantName = "Test Particpant";
        private string evaluationInfo;
        private string evaluationInfoNoID;

        /// <summary>
        /// Test for newly created Evaluations, all gets show return null
        /// </summary>
        [TestMethod]
        public void TestNullEvaluations()
        {
            //Create Evaluation
            this.evaluationInfo = "0(Type: 0)(Stage:0)";
            this.evaluationInfoNoID = "Type: 0(Stage:0)";

            //Return Evaluation Data
            Assert.AreEqual(0, this.evaluations.EvaluationID, 0, "Evaluations's EvaluationID not 0.");
            Assert.AreEqual(0, this.evaluations.StageID, 0, "Evaluations's StageID not 0.");
            Assert.IsNull(this.evaluations.RoleID, "Evaluations's RoleID not null.");
            Assert.IsNull(this.evaluations.CompletionDate, "Evaluations's CompletionDate not null.");
            Assert.AreEqual(0, this.evaluations.TypeID, 0, "Evaluations's TypeID not 0.");
            Assert.AreEqual(0, this.evaluations.ParticipantID, 0, "Evaluations's ParticipantID not 0.");
            Assert.IsNull(this.evaluations.EvaluatorID, "Evaluations's EvaluatorID not null.");
            Assert.IsNull(this.evaluations.RoleName, "Evaluations's RoleName not null.");
            Assert.IsNull(this.evaluations.EvaluatorName, "Evaluations's EvaluatorName not null.");
            Assert.IsNull(this.evaluations.ParticipantName, "Evaluations's participantName not null.");
            Assert.AreEqual(this.evaluationInfo, this.evaluations.EvaluationInfo, "Evaluations's EvaluationInfo not " + this.evaluationInfo + ".");
            Assert.AreEqual(this.evaluationInfoNoID, this.evaluations.EvaluationInfoNoID, "Evaluation's EvaluatorInfo not " + this.evaluationInfoNoID + ".");
        }

        /// <summary>
        /// Test for set and get of an evaluation [Evaluations class]
        /// </summary>
        [TestMethod]
        public void TestActiveEvaluation()
        {
            //Create Evaluation
            this.evaluations.EvaluationID = this.evaluationID;
            this.evaluations.StageID = this.stageID;
            this.evaluations.RoleID = this.roleID;
            this.evaluations.CompletionDate = this.completionDate;
            this.evaluations.TypeID = this.typeID;
            this.evaluations.ParticipantID = this.participantID;
            this.evaluations.EvaluatorID = this.evaluatorID;
            this.evaluations.RoleName = this.roleName;
            this.evaluations.EvaluatorName = this.evaluatorName;
            this.evaluations.ParticipantName = this.participantName;
            this.evaluationInfo = this.evaluationID + "(Type: " + this.typeID + ")(Stage:" + this.stageID + ")";
            this.evaluationInfoNoID = "Type: " + this.typeID + "(Stage:" + this.stageID + ")";

            //Return Evaluation Data
            Assert.AreEqual(this.evaluationID, this.evaluations.EvaluationID, 0, "Evaluation's EvaluationID not " + this.evaluationID + ".");
            Assert.AreEqual(this.stageID, this.evaluations.StageID, 0, "Evaluation's StageID not " + this.stageID + ".");
            Assert.AreEqual(this.roleID, this.evaluations.RoleID, "Evaluation's RoleID not " + this.roleID + ".");
            Assert.AreEqual(this.completionDate, this.evaluations.CompletionDate, "Evaluation's CompletionDate not " + this.completionDate + ".");
            Assert.AreEqual(this.typeID, this.evaluations.TypeID, 0, "Evaluation's TypeID not " + this.typeID + ".");
            Assert.AreEqual(this.participantID, this.evaluations.ParticipantID, 0, "Evaluation's ParticipantID not " + this.participantID + ".");
            Assert.AreEqual(this.evaluatorID, this.evaluations.EvaluatorID, "Evaluation's EvaluatorID not " + this.evaluatorID + ".");
            Assert.AreEqual(this.roleName, this.evaluations.RoleName, "Evaluation's RoleName not not " + this.roleName + ".");
            Assert.AreEqual(this.evaluatorName, this.evaluations.EvaluatorName, "Evaluation's Evaluator not not " + this.evaluatorName + ".");
            Assert.AreEqual(this.participantName, this.evaluations.ParticipantName, "Evaluation's Evaluator not not " + this.participantName + ".");
            Assert.AreEqual(this.evaluationInfo, this.evaluations.EvaluationInfo, "Evaluation's ParticipantInfo not " + this.evaluationInfo + ".");
            Assert.AreEqual(this.evaluationInfoNoID, this.evaluations.EvaluationInfoNoID, "Evaluation's EvaluatorInfo not " + this.evaluationInfoNoID + ".");
        }
    }
}