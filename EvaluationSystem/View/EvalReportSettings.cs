﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EvaluationSystem.View
{
    public partial class EvalReportSettings : Form
    {
        public EvalReportSettings()
        {
            InitializeComponent();
        }

        private void evalReportSettings_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'reports.employees' table. You can move, or remove it, as needed.
            this.employeesTableAdapter.Fill(this.reports.employees);
            // TODO: This line of code loads data into the 'reports.types' table. You can move, or remove it, as needed.
            this.typesTableAdapter.Fill(this.reports.types);
            // TODO: This line of code loads data into the 'reports.stage' table. You can move, or remove it, as needed.
            this.stageTableAdapter.Fill(this.reports.stage);


        }

        /// <summary>
        /// Closes the form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Submits the form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void submitButton_Click(object sender, EventArgs e)
        {
            int employeeID, stageID, typeID;
            employeeID = (int)employeeBox.SelectedValue;
            stageID = (int)stageBox.SelectedValue;
            typeID = (int)typeBox.SelectedValue;

            //Builds the form.
            EvalReport thisEvalReport = new EvalReport();
            //Loads the form.
            thisEvalReport.load(employeeID, stageID, typeID);
            //Sets the MDI Parent for the form.
            thisEvalReport.MdiParent = MdiParent;
            //Shows the form and closes the current window.
            thisEvalReport.Show();
            this.Close();
        }
    }
}
