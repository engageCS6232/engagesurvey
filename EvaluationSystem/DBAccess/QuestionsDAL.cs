﻿using EvaluationSystem.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace EvaluationSystem.DBAccess
{
   public class QuestionsDAL
    {

        public static List<Questions> getQuestionsByType(int typeID)
        {
            List<Questions> questionList = new List<Questions>();

            String selectStatement =
                "SELECT questionID, question, description, typeID, categoryID" +
                " FROM questions" +
                " WHERE typeID = @typeID";

            try
            {
                using (SqlConnection connection = EngageDBConnection.GetConnection())
                {
                    connection.Open();

                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        selectCommand.Parameters.AddWithValue("@typeID", typeID);
                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {

                            int questionIDOrd = reader.GetOrdinal("questionID");
                            int questionOrd = reader.GetOrdinal("question");
                            int descriptionOrd = reader.GetOrdinal("description");
                            int typeIDOrd = reader.GetOrdinal("typeID");
                            int categoryIDORd = reader.GetOrdinal("categoryID");

                            while (reader.Read())
                            {
                                Questions question = new Questions();
                                question.QuestionId = reader.GetInt32(questionIDOrd);
                                question.Question = reader.GetString(questionOrd);
                                question.Description = reader.GetString(descriptionOrd);
                                question.TypeId = reader.GetInt32(typeIDOrd);
                                question.CategoryId = reader.GetInt32(categoryIDORd);
                                questionList.Add(question);
                            }
                        }
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            return questionList;

        }
    }
}
