﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EvaluationSystem.Model;
using System.Collections.Generic;
using EvaluationSystem.DBAccess;

namespace EvaluationSystemTest.DBAccessTest
{
    [TestClass]
    public class QuestionsDALTest
    {
        /// <summary>
        /// Test return a Question List of given Type
        /// </summary>
        [TestMethod]
        public void TestGetQuestionsByType()
        {
            //Correct Results
            int questionCount = 15;
            int indexQuestion = 0;
            int questionId = 1;
            string question = "Question 1 - Category 1 - Type 1";
            string description = "Question 1 Description";
            int typeId = 1;
            int categoryId = 1;

            //Get Questions List
            List<Questions> questionListType1 = QuestionsDAL.getQuestionsByType(typeId);
            Questions questions = questionListType1[indexQuestion];

            //Check Question Results
            Assert.AreEqual(questionCount, questionListType1.Count, 0, "Questions count not " + questionCount + ".");
            Assert.AreEqual(questionId, questions.QuestionId, 0, "Questions's QuestionId not " + questionId + ".");
            Assert.AreEqual(question, questions.Question, "Questions's Question not " + question + ".");
            Assert.AreEqual(description, questions.Description, "Questions's Description not " + description + ".");
            Assert.AreEqual(typeId, questions.TypeId, 0, "Questions's TypeId not " + typeId + ".");
            Assert.AreEqual(categoryId, questions.CategoryId, 0, "Questions's CategoryId not " + categoryId + ".");
        }
    }
}