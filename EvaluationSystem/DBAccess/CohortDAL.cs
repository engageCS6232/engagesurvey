﻿using EvaluationSystem.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Transactions;

namespace EvaluationSystem.DBAccess
{
    public class CohortDAL
    {/// <summary>
        /// Adds a new cohort to the Cohorts list
        /// </summary>
        /// <param name="cohortName">Name of cohort retrieved from form</param>
        /// <returns></returns>
        public static int CreateCohort(String cohortName, BindingList<Employees> selectedList)
        {
            string insertStatement = "INSERT INTO cohort (cohortName)" +
                                     "VALUES (@cohortName)";

            string employeeNoCohortSelectStatement = "SELECT COUNT(cohortID)" +
                                                     " FROM selected" +
                                                     " WHERE employeeID = @employeeID";
            try
            {
                using (SqlConnection connection = EngageDBConnection.GetConnection())
                {
                    using (SqlCommand insertCmd = new SqlCommand())
                    {
                        insertCmd.Connection = connection;
                        connection.Open();

                        // Check if employee are still not assigned to a cohort
                        insertCmd.Parameters.Add("@employeeID", SqlDbType.Int);
                        try
                        {
                            foreach (Employees current in selectedList)
                            {

                                insertCmd.CommandText = employeeNoCohortSelectStatement;
                                insertCmd.Parameters["@employeeID"].Value = current.EmployeeID;
                                int employeesWithCohort = (Int32)insertCmd.ExecuteScalar();

                                if (employeesWithCohort != 0)
                                {
                                    return 0;
                                }
                            }
                        }
                        catch (SqlException)
                        {
                            throw;
                        }
                        catch (Exception)
                        {
                            throw;
                        }



                        insertCmd.CommandText = insertStatement;
                        insertCmd.Parameters.AddWithValue("@cohortName", cohortName);
                        //insertCmd.Connection = connection;
                        //connection.Open();
                        int results = insertCmd.ExecuteNonQuery();

                        insertCmd.CommandText = "SELECT IDENT_CURRENT('cohort')";
                        int cohortIdInserted = Convert.ToInt32(insertCmd.ExecuteScalar());
                        return cohortIdInserted;
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Loops through a list of Employees and adds them to the Selected table
        /// </summary>
        /// <param name="selectedList"></param>
        /// <returns></returns>
        private static Boolean AddEmployeesToCohort(BindingList<Employees> selectedList, SqlConnection connection)
        {
            Boolean insertSuccessful = true;
            String insertStatement = "INSERT INTO selected (employeeID, cohortID) VALUES(@employeeID, @cohortID)";
                using (SqlCommand insertCommand = new SqlCommand())
                {
                    insertCommand.Connection = connection;
                    insertCommand.Parameters.Add("@employeeID", SqlDbType.Int);
                    insertCommand.Parameters.Add("@cohortID", SqlDbType.Int);
                    try
                    {                        
                        foreach (Employees current in selectedList)
                        {
                           
                            insertCommand.CommandText = insertStatement;
                            
                            insertCommand.Parameters["@employeeID"].Value = current.EmployeeID;
                            insertCommand.Parameters["@cohortID"].Value = current.CohortID;
                            
                            int affectedRows = insertCommand.ExecuteNonQuery();
                            if (affectedRows < 0)
                            {
                                insertSuccessful = false;
                            }                            
                        }
                    }
                    catch (SqlException)
                    {
                        throw;
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            
            return insertSuccessful;
        }    

        /// <summary>
        /// Returns a list of employees in a given cohort
        /// </summary>
        /// <param name="cohortID"></param>
        /// <returns></returns>
        public static List<Employees> GetMembersOfCohort(int cohortID)
        {
            List<Employees> employeeList = new List<Employees>();
            String selectStatement = "SELECT s.employeeID, fName, lName " +
                                     " FROM selected s JOIN employees e ON s.employeeID = e.employeeID " +
                                     " WHERE cohortID = @cohortID";
            try
            {
                using (SqlConnection connection = EngageDBConnection.GetConnection())
                {
                    connection.Open();

                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        selectCommand.Parameters.AddWithValue("@cohortID", cohortID);
                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            int eIDOrd = reader.GetOrdinal("employeeID");
                            int fNameOrd = reader.GetOrdinal("fName");
                            int lNameOrd = reader.GetOrdinal("lName");
                            while (reader.Read())
                            {
                                Employees employee = new Employees();
                                employee.EmployeeID = reader.GetInt32(eIDOrd);
                                employee.FName = reader.GetString(fNameOrd);
                                employee.LName = reader.GetString(lNameOrd);
                                employeeList.Add(employee);
                            }
                        }
                    }
                }
                return employeeList;
            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Deletes the cohort with the passed cohortID
        /// </summary>
        /// <param name="cohortID"> ID of cohort to delete</param>
        /// <returns></returns>
        public static bool DeleteCohort(int cohortID)
        {
            bool isDeleted = true;
            string deleteStatement = "DELETE FROM cohort WHERE cohortID = @cohortID";
            

            try
            {
                using (SqlConnection connection = EngageDBConnection.GetConnection())
                {
                    connection.Open();
                    using (SqlCommand deleteCommand = new SqlCommand(deleteStatement, connection))
                    {
                        deleteCommand.Parameters.AddWithValue("@cohortID", cohortID);
                        int deleteCount = deleteCommand.ExecuteNonQuery();

                        if (deleteCount <= 0)
                        {
                            isDeleted = false;
                            throw new Exception("Failed to Delete cohort");
                        }
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }

            return isDeleted;
        }

        /// <summary>
        /// Updates the cohort by removing and adding employees back into the selected table
        /// </summary>
        /// <param name="cohortID"></param>
        /// <param name="employeesList"></param>
        /// <returns></returns>
        public static bool UpdateCohort(int cohortID, BindingList<Employees> employeesList) 
        {
            bool isUpdated = false ;
            string deleteStatement = "DELETE FROM selected WHERE cohortID = @cohortID";
            string deleteCohortEvalTracker = "DELETE FROM cohort_eval_tracker WHERE cohortID = @cohortID";
            
            try
            {
                using (TransactionScope transactionScope = new TransactionScope()) {
                    using (SqlConnection connection = EngageDBConnection.GetConnection())
                    {
                        connection.Open();
                        using (SqlCommand command = new SqlCommand(deleteStatement, connection))
                        {

                            command.Parameters.AddWithValue("@cohortID", cohortID);
                            int deleteCount = command.ExecuteNonQuery();  
                            bool success = AddEmployeesToCohort(employeesList, connection);

                            command.CommandText = deleteCohortEvalTracker;
                            int deleteTrackerCount = command.ExecuteNonQuery();

                            if (!success || deleteCount < 0 || deleteTrackerCount < 0)
                            {
                                throw new Exception("Updating cohort failed");
                            }
                            transactionScope.Complete();
                            isUpdated = true;
                        }
                    }
                }
            }
            catch (SqlException)
            {
  
                throw;
            }
            catch (Exception)
            {

                throw;
            }

            return isUpdated;
        }


        /// <summary>
        /// Gets a list of the cohorts
        /// </summary>
        /// <returns></returns>
        public static List<Cohorts> GetCohortsList()
        {
            List<Cohorts> cohortList = new List<Cohorts>();

            string selecteStatement = "SELECT cohortID, cohortName " +
                "FROM cohort";

            try
            {
                using (SqlConnection connection = EngageDBConnection.GetConnection())
                {
                    connection.Open();
                    using (SqlCommand selectCommand = new SqlCommand(selecteStatement, connection))
                    {
                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Cohorts cohort = new Cohorts();
                                cohort.CohortID = (int) reader["cohortID"];
                                cohort.CohortName = reader["cohortName"].ToString();
                                cohortList.Add(cohort);
                            }
                        }
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }

            return cohortList;
        }

    }
}
