﻿namespace EvaluationSystem.View
{
    partial class GraphReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.graphReportBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.reports = new EvaluationSystem.Reports();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.graphReportTableAdapter = new EvaluationSystem.ReportsTableAdapters.graphReportTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.graphReportBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reports)).BeginInit();
            this.SuspendLayout();
            // 
            // graphReportBindingSource
            // 
            this.graphReportBindingSource.DataMember = "graphReport";
            this.graphReportBindingSource.DataSource = this.reports;
            // 
            // reports
            // 
            this.reports.DataSetName = "Reports";
            this.reports.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // reportViewer1
            // 
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "GraphDataSet";
            reportDataSource1.Value = this.graphReportBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "EvaluationSystem.Reports.GraphReport.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 0);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(878, 632);
            this.reportViewer1.TabIndex = 0;
            // 
            // graphReportTableAdapter
            // 
            this.graphReportTableAdapter.ClearBeforeFill = true;
            // 
            // GraphReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(878, 632);
            this.Controls.Add(this.reportViewer1);
            this.Name = "GraphReport";
            this.Text = "Cohort Performance Report";
            this.Load += new System.EventHandler(this.GraphReport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.graphReportBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reports)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource graphReportBindingSource;
        private Reports reports;
        private ReportsTableAdapters.graphReportTableAdapter graphReportTableAdapter;
    }
}