﻿using EvaluationSystem.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluationSystem.DBAccess
{
    public class EmployeesDAL
    {
        private static Validation validator = new Validation();

        /// <summary>
        /// Gets the employee's user name from the employee ID.
        /// </summary>
        /// <param name="employeeID"></param>
        /// <returns></returns>
        public static String getEmployeeUserName(int employeeID)
        {
            String userName ="";

            String selectStatement =
              "SELECT Employees.username  " +
              "FROM Employees " +
              "WHERE Employees.employeeID = @employeeID";

            try
            {
                using (SqlConnection connection = EngageDBConnection.GetConnection())
                {
                    connection.Open();

                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        selectCommand.Parameters.AddWithValue("@employeeID", employeeID);
                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            int username = reader.GetOrdinal("username");
                           
                            while (reader.Read())
                            {
                                userName = reader.GetString(username);

                            }
                        }
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {   
                throw;
            }

            return userName;
        }

       /// <summary>
       /// Gets the Employee data.
       /// </summary>
       /// <param name="username"></param>
       /// <returns>An Employee Object</returns>
        public static Employees getEmployeeData(String username) 
        {
            Employees newEmployee = new Employees();

            String selectStatement =
                "SELECT Employees.employeeID, Employees.fName, Employees.lName, Employees.email, Employees.administrator, Selected.cohortID " +
                "FROM Employees FULL OUTER JOIN Selected ON employees.employeeID = Selected.employeeID " +
                "WHERE Employees.username = @username";

            try
            {
                using (SqlConnection connection = EngageDBConnection.GetConnection())
                {
                    connection.Open();

                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        selectCommand.Parameters.AddWithValue("@username", username);
                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            int eID = reader.GetOrdinal("employeeID");
                            int fName = reader.GetOrdinal("fName");
                            int lName = reader.GetOrdinal("lName");
                            int email = reader.GetOrdinal("email");
                            int admin = reader.GetOrdinal("administrator");
                            int cID = reader.GetOrdinal("cohortID");

                            while (reader.Read())
                            {
                                newEmployee.EmployeeID = reader.GetInt32(eID);
                                newEmployee.FName = reader.GetString(fName);
                                newEmployee.LName = reader.GetString(lName);
                                newEmployee.Email = reader.GetString(email);
                                newEmployee.Administrator = reader.GetBoolean(admin);
                                if (!reader.IsDBNull(cID))
                                {
                                    newEmployee.CohortID = reader.GetInt32(cID);
                                }
                            }
                        }
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            return newEmployee;

        }

       /// <summary>
        /// Gets the hashed password from the DB, hashes the input password, and validates that the two match.
       /// </summary>
       /// <param name="username"></param>
       /// <param name="password"></param>
       /// <returns>A boolean result. True if match, else false.</returns>
        public static Boolean ComparePassword(String username, String password)
        {
            String hashedPassword = "";

            String selectStatement =
                "SELECT Employees.password " +
                "FROM Employees " +
                "WHERE Employees.username = @username";

            try
            {
                using (SqlConnection connection = EngageDBConnection.GetConnection())
                {
                    connection.Open();

                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        selectCommand.Parameters.AddWithValue("@username", username);
                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            int passwordOrdinal = reader.GetOrdinal("password");

                            while (reader.Read())
                            {
                                hashedPassword = reader.GetString(passwordOrdinal);
                            }
                        }
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }

            return validator.VerifySHA1Hash(password, hashedPassword);
        }

        /// <summary>
        /// Retrieves a list of employees within a cohort.
        /// </summary>
        /// <param name="cohortID"></param>
        /// <returns>A list of employees.</returns>
        public static List<Employees> getEmployeeList(int cohortID, int employeeID, int supervisorID)
        {
            List<Employees> employeeList = new List<Employees>();


            String selectStatement =
                "SELECT " +
                "Employees.lName , Employees.fName , Employees.employeeID " +
                "FROM Employees " +
                "WHERE Employees.employeeID != @employeeID " +
                "AND Employees.employeeID != @supervisorID " +
                /*"FROM Employees, Selected " +
                "WHERE Employees.employeeID = Selected.employeeID " +
                "AND Selected.cohortID = @cohortID " +
                "AND Selected.employeeID != @employeeID " +
                "AND Selected.employeeID != @supervisorID " +*/
                "ORDER BY lName , fName";

            try
            {
                using (SqlConnection connection = EngageDBConnection.GetConnection())
                {
                    connection.Open();

                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        selectCommand.Parameters.AddWithValue("@cohortID", cohortID);
                        selectCommand.Parameters.AddWithValue("@employeeID", employeeID);
                        selectCommand.Parameters.AddWithValue("@supervisorID", supervisorID);

                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            int eID = reader.GetOrdinal("employeeID");
                            int fName = reader.GetOrdinal("fName");
                            int lName = reader.GetOrdinal("lName");

                            while (reader.Read())
                            {
                                Employees newEmployee = new Employees();
                                newEmployee.EmployeeID = reader.GetInt32(eID);
                                newEmployee.FName = reader.GetString(fName);
                                newEmployee.LName = reader.GetString(lName);
                                employeeList.Add(newEmployee);
                            }
                        }
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            return employeeList;
        }

        public static List<Employees> GetEmployeesWithoutCohorts()
        {
            List<Employees> employeeList = new List<Employees>();

            string selectStatement =
                "SELECT e.fName, e.lName, e.employeeID, s.cohortID" +
                " FROM Employees e " +
                " LEFT JOIN Selected s " +
                " ON e.employeeID = s.employeeID" +
                " WHERE s.cohortID IS NULL";

            try
            {
                using (SqlConnection connection = EngageDBConnection.GetConnection())
                {
                    connection.Open();
                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            int fNameOrd = reader.GetOrdinal("fName");
                            int lNameOrd = reader.GetOrdinal("lName");
                            int employeeIDOrd = reader.GetOrdinal("employeeID");
                            int cohortIDOrd = reader.GetOrdinal("cohortID");
                            while (reader.Read())
                            {
                                Employees employee = new Employees();
                                employee.FName = reader.GetString(fNameOrd);
                                employee.LName = reader.GetString(lNameOrd);
                                employee.EmployeeID = reader.GetInt32(employeeIDOrd);
                                if (reader[cohortIDOrd] == DBNull.Value)
                                {
                                    employee.CohortID = null;
                                }
                                else
                                {
                                    employee.CohortID = reader.GetInt32(cohortIDOrd);
                                }
                                employeeList.Add(employee);
                            }
                        }
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            return employeeList;
        }

        public static Employees GetEmployeesByID(int employeeID)
        {
            Employees employee = new Employees();
            string selectString = "SELECT lName, fName FROM employees WHERE employeeID = @employeeID";

            try
            {
                using (SqlConnection connection = EngageDBConnection.GetConnection())
                {
                    using (SqlCommand selectCommand = new SqlCommand(selectString, connection))
                    {
                        selectCommand.Parameters.AddWithValue("@employeeID", employeeID);
                        connection.Open();
                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                employee.LName = reader["lName"].ToString();
                                employee.FName = reader["fName"].ToString();
                            }
 
                        }
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            return employee;
        }
    }
}
