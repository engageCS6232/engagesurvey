﻿using EvaluationSystem.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace EvaluationSystem.DBAccess
{
    public class CategoriesDAL
    {
        /// <summary>
        /// Returns a list of employees
        /// </summary>
        /// <param name="typeID"></param>
        /// <returns></returns>
        public static List<Categories> getCategoriesByType(int typeID)
        {
            List<Categories> categoryList = new List<Categories>();

            String selectStatement =
                "SELECT categoryID, categoryName, description, categoryNum" +
                " FROM categories" +
                " WHERE typeID = @typeID";

            try
            {
                using (SqlConnection connection = EngageDBConnection.GetConnection())
                {
                    connection.Open();

                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        selectCommand.Parameters.AddWithValue("@typeID", typeID);
                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            int categoryIDOrd = reader.GetOrdinal("categoryID");
                            int categoryNameOrd = reader.GetOrdinal("categoryName");
                            int descriptionOrd = reader.GetOrdinal("description");
                            int categoryNumOrd = reader.GetOrdinal("categoryNum");

                            while (reader.Read())
                            {
                                Categories category = new Categories();
                                category.CategoryId = reader.GetInt32(categoryIDOrd);
                                category.CategoryName = reader.GetString(categoryNameOrd);
                                category.Description = reader.GetString(descriptionOrd);
                                category.CategoryNum = reader.GetInt32(categoryNumOrd);
                                categoryList.Add(category);
                            }
                        }
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            return categoryList;

        }
    }
}
