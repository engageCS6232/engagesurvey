﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EvaluationSystem.Model;
using EvaluationSystem.DBAccess;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;

namespace EvaluationSystemTest.DBAccessTest
{
    [TestClass]
    public class CohortDALTest
    {

        /// <summary>
        /// Test for get members of a cohort, return List<Employees>
        /// </summary>
        [TestMethod]
        public void TestGetMembersOfCohort()
        {
            //Correct Results
            int employeesCount = 4;
            int indexEmployees = 0;
            int cohortID = 1;
            int employeeID = 4;
            string fName = "Olivia";
            string lName = "Jones";

            //Get Members List
            List<Employees> employeesList = CohortDAL.GetMembersOfCohort(cohortID);
            Employees employee = employeesList[indexEmployees];
            string employeeInfo = lName + ", " + fName + "(" + employeeID + ")";

            //Check Members Results
            Assert.AreEqual(employeesCount, employeesList.Count, 0, "Categories count not " + employeesCount + ".");
            Assert.AreEqual(employeeID, employee.EmployeeID, 0, "Employees's EmployeeID not " + employeeID + ".");
            Assert.AreEqual(fName, employee.FName, "Employees's FName not " + fName + ".");
            Assert.AreEqual(lName, employee.LName, "Employees's LName not " + lName + ".");
            Assert.AreEqual(employeeInfo, employee.EmployeeInfo, "Employee's EmployeeInfo not " + employeeInfo + ".");
        }

        /// <summary>
        /// Test for get a list cohort, return List<Cohorts>
        /// </summary>
        [TestMethod]
        public void TestGetCohortsList()
        {
            //Correct Results
            int cohortCount = 2;
            int indexCohort = 0;
            int cohortID = 1;
            string cohortName = "Cohort Test 1";

            //Get Cohort List
            List<Cohorts> cohortsList = CohortDAL.GetCohortsList();
            Cohorts cohorts = cohortsList[indexCohort];
            string cohortInfo = cohortName + "(" + cohortID + ")";

            //Check Cohort Results
            Assert.AreEqual(cohortCount, cohortsList.Count, 0, "Categories count not " + cohortCount + ".");
            Assert.AreEqual(cohortID, cohorts.CohortID, 0, "Cohorts's CohortID not " + cohortID + ".");
            Assert.AreEqual(cohortName, cohorts.CohortName, "Cohorts's CohortName not " + cohortName + ".");
            Assert.AreEqual(cohortInfo, cohorts.CohortInfo, "Cohorts's CohortInfo not correct.");
        }
        
        /// <summary>
        /// Test for create a cohort, return cohortID,
        /// then deletes it.
        /// </summary>
        [TestMethod]
        public void TestCreateDeleteCohort()
        {
            //Correct Results
            BindingList<Employees> selectedList = new BindingList<Employees>();
            Employees employees = new Employees();
            int employeeID = 13;
            int employeesCount = 1;
            int indexEmployees = 0;
            string lName = "Mia";
            string fName = "Taylor";

            //Get Selected List
            employees.EmployeeID = employeeID;
            selectedList.Add(employees);

            //Add Cohort
            int cohortID = 0;
            cohortID = CohortDAL.CreateCohort("CohortNewTest", selectedList);

            //Return Cohort Data
            Assert.AreNotEqual(0, cohortID, 0, "New Cohort ID not created.");

            //Get Members List
            List<Employees> employeesList = CohortDAL.GetMembersOfCohort(cohortID);
            Employees employee = employeesList[indexEmployees];
            string employeeInfo = lName + ", " + fName + "(" + employeeID + ")";

            //Check Members Results
            Assert.AreEqual(employeesCount, employeesList.Count, 0, "Cohort count not " + employeesCount + ".");
            Assert.AreEqual(employeeID, employee.EmployeeID, 0, "Employees's EmployeeID not " + employeeID + ".");
            Assert.AreEqual(fName, employee.FName, "Employees's FName not " + fName + ".");
            Assert.AreEqual(lName, employee.LName, "Employees's LName not " + lName + ".");
            Assert.AreEqual(employeeInfo, employee.EmployeeInfo, "Employee's EmployeeInfo not " + employeeInfo + ".");

            //Delete Test Cohort
            Boolean checkDelete = CohortDAL.DeleteCohort(cohortID);

            //Check Delete
            Assert.IsTrue(checkDelete, "Cohort not deleted.");
        }

        /// <summary>
        /// Test for additional employees
        /// </summary>
        [TestMethod]
        public void TestUpdateCohort()
        {
            //Correct Results
            BindingList<Employees> selectedList = new BindingList<Employees>();
            Employees employees = new Employees();
            int employeeID = 11;
            int cohortID = 3;
            int employeesCount = 4;
            int indexEmployees = 3;
            string lName = "Logan";
            string fName = "Anderson";

            //Get Selected List
            employees.EmployeeID = employeeID;
            employees.CohortID = cohortID;
            selectedList.Add(employees);

            //Updates members
            Boolean checkUpdates = CohortDAL.UpdateCohort(cohortID, selectedList);

            //Check Updates
            Assert.IsTrue(checkUpdates, "Employees not updated.");

            //Get Members List
            List<Employees> employeesList = CohortDAL.GetMembersOfCohort(cohortID);
            Employees employee = employeesList[indexEmployees];
            string employeeInfo = lName + ", " + fName + "(" + employeeID + ")";

            //Check Members Results
            Assert.AreEqual(employeesCount, employeesList.Count, 0, "Cohort count not " + employeesCount + ".");
            Assert.AreEqual(employeeID, employee.EmployeeID, 0, "Employees's EmployeeID not " + employeeID + ".");
            Assert.AreEqual(fName, employee.FName, "Employees's FName not " + fName + ".");
            Assert.AreEqual(lName, employee.LName, "Employees's LName not " + lName + ".");
            Assert.AreEqual(employeeInfo, employee.EmployeeInfo, "Employee's EmployeeInfo not " + employeeInfo + ".");
        }
    }
}