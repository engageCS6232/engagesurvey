﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluationSystem.Model
{
    public class Cohorts
    {
        /// <summary>
        /// Cohort table class
        /// </summary>
        /// 
        public int CohortID { get; set; }
        public string CohortName { get; set; }

        /// <summary>
        /// Returns the concatenated string of first name, last name (employeeID).
        /// </summary>
        public string CohortInfo
        {
            get
            {
                return this.CohortName + "(" + this.CohortID + ")";
            }
        }
    }
}