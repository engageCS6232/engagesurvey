﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EvaluationSystem.Model;
using EvaluationSystem.DBAccess;
using System.Collections.Generic;

namespace EvaluationSystemTest.DBAccessTest
{
    [TestClass]
    public class EvaluationDALTest
    {
        //Insert Supervisor [D]
        private int employeeIDD = 15;
        private int supervisorIDD = 10;
        private int coworkerIDD = 20;
        private int stageIDD = 2;
        private int typeIDD = 2;
        
        /// <summary>
        /// Test to get evaluations
        /// </summary>
        [TestMethod]
        public void TestGetEvaluation()
        {
            //Correct Results
            int evaluationID = 17;
            int stageID = 2;
            int roleID = 1;
            DateTime? completionDate = Convert.ToDateTime("03/06/2016");
            int typeID = 1;
            int participantID = 3;
            int evaluatorID = 3;
            DateTime? dueDate = DateTime.Now.AddDays(2).Date;
            string participantFirstName = "TestFirstName";
            string participantLastName = "TestLastName";
            string evaluatorFirstName = "TestFirstName";
            string evaluatorLastName = "TestLastName";

            //Create Evaluation Data
            String evaluationInfo = evaluationID + "(Type: " + typeID + ")(Stage:" + stageID + ")";
            String evaluationInfoNoID = "Type: " + typeID + "(Stage:" + stageID + ")";
            String participantInfo = participantFirstName + ", " + participantLastName + "(" + participantID + ")";
            String evaluatorInfo = evaluatorFirstName + ", " + evaluatorLastName + "(" + evaluatorID + ")";
            
            //Get Evaluation
            Evaluations evaluations = new Evaluations();
            evaluations = EvaluationsDAL.getEvaluation(evaluationID);

            //Check Evaluation Data
            Assert.AreEqual(evaluationID, evaluations.EvaluationID, 0, "Evaluation's EvaluationID not " + evaluationID + ".");
            Assert.AreEqual(stageID, evaluations.StageID, 0, "Evaluation's StageID not " + stageID + ".");
            Assert.AreEqual(roleID, evaluations.RoleID, "Evaluation's RoleID not " + roleID + ".");
            Assert.IsNull(evaluations.CompletionDate, "Evaluation's CompletionDate not null.");
            Assert.AreEqual(typeID, evaluations.TypeID, 0, "Evaluation's TypeID not " + typeID + ".");
            Assert.AreEqual(participantID, evaluations.ParticipantID, 0, "Evaluation's ParticipantID not " + participantID + ".");
            Assert.AreEqual(evaluatorID, evaluations.EvaluatorID, "Evaluation's EvaluatorID not " + evaluatorID + ".");
            Assert.IsNull(evaluations.RoleName, "Evaluation's RoleName not not null.");
        }

        /// <summary>
        /// Test to get evaluations open for employee to assign
        /// </summary>
        [TestMethod]
        public void TestgetEvaluationsOpenForEmployeeToAssign()
        {
            //Correct Results
            int employeeID = 3;
            int evaluationID = 17;
            int stageID = 2;
            int typeID = 1;
            int participantID = 0;
            DateTime? completionDate = Convert.ToDateTime("03/06/2016");
            DateTime? dueDate = DateTime.Now.AddDays(2).Date;
                        
            //Create Evaluation Data
            String evaluationInfo = evaluationID + "(Type: " + typeID + ")(Stage:" + stageID + ")";
            String evaluationInfoNoID = "Type: " + typeID + "(Stage:" + stageID + ")";

            //Get Evaluation
            List<Evaluations> evaluationsList = new List<Evaluations>();
            evaluationsList = EvaluationsDAL.getEvaluationsOpenForEmployeeToAssign(employeeID);
            Evaluations evaluations = new Evaluations();
            evaluations = evaluationsList[0];

            //Return Evaluation Data
            Assert.AreEqual(evaluationID, evaluations.EvaluationID, 0, "Evaluation's EvaluationID not " + evaluationID + ".");
            Assert.AreEqual(stageID, evaluations.StageID, 0, "Evaluation's StageID not " + stageID + ".");
            Assert.IsNull(evaluations.RoleID, "Evaluation's RoleID not null.");
            Assert.IsNull(evaluations.CompletionDate, "Evaluation's CompletionDate not null.");
            Assert.AreEqual(typeID, evaluations.TypeID, 0, "Evaluation's TypeID not " + typeID + ".");
            Assert.AreEqual(participantID, evaluations.ParticipantID, 0, "Evaluation's ParticipantID not " + participantID + ".");
            Assert.IsNull(evaluations.EvaluatorID, "Evaluation's EvaluatorID not null.");
        }

        /// <summary>
        /// Test to get evaluations open for employee to assign
        /// </summary>
        [TestMethod]
        public void TestgetEvaluationsOpenWithEmployee()
        {
            //Correct Results
            int employeeID = 1;
            int evaluationID = 11;
            int stageID = 1;
            int typeID = 1;
            int? roleID = 1;
            int? evaluatorID = 1;
            int participantID = 1;
            int elements = 2;

            //Create Evaluation Data
            String evaluationInfo = evaluationID + "(Type: " + typeID + ")(Stage:" + stageID + ")";
            String evaluationInfoNoID = "Type: " + typeID + "(Stage:" + stageID + ")";

            //Get Evaluation
            List<Evaluations> evaluationsTestList = new List<Evaluations>();
            evaluationsTestList = EvaluationsDAL.getEvaluationsOpenWithEmployee(employeeID, true);
            Evaluations evaluationsTest = new Evaluations();
            evaluationsTest = evaluationsTestList[0];

            //Return Evaluation Data
            Assert.AreEqual(evaluationID, evaluationsTest.EvaluationID, 0, "Evaluation ID not " + evaluationID + ".");
            Assert.AreEqual(stageID, evaluationsTest.StageID, 0, "Stage ID not " + stageID + ".");
            Assert.AreEqual(roleID, evaluationsTest.RoleID, "Role ID not " + roleID +".");
            Assert.IsNull(evaluationsTest.CompletionDate, "Completion Date not null.");
            Assert.AreEqual(typeID, evaluationsTest.TypeID, 0, "Type ID not " + typeID + ".");
            Assert.AreEqual(participantID, evaluationsTest.ParticipantID, 0, "Participant ID not " + participantID + ".");
            Assert.AreEqual(evaluatorID, evaluationsTest.EvaluatorID, "Evaluator ID not " + evaluatorID + ".");
            Assert.AreEqual(evaluationInfo, evaluationsTest.EvaluationInfo, "Evaluation info not correct.");
            Assert.AreEqual(evaluationInfoNoID, evaluationsTest.EvaluationInfoNoID, "Evaluation info number ID not correct.");
            Assert.AreEqual(elements, evaluationsTestList.Count, 0, "Evaluations List not the correct size.");
        }

        /// <summary>
        /// Test to get evaluations open for employee to assign
        /// </summary>
        [TestMethod]
        public void TestInsertEmployeesSupervisor()
        {
            //Insert Supervisor
            Boolean passed = EvaluationsDAL.insertEmployeesSupervisor(employeeIDD, supervisorIDD, stageIDD, typeIDD);
        
            //Check Insert
            Assert.IsTrue(passed, "Supervisor not inserted.");
        }

        /// <summary>
        /// Test to get evaluations open for employee to assign
        /// </summary>
        [TestMethod]
        public void TestInsertEmployeesCoworker()
        {
            //Insert Coworker
            Boolean passed = EvaluationsDAL.insertEmployeesCoworker(employeeIDD, coworkerIDD, stageIDD, typeIDD);

            //Check Insert
            Assert.IsTrue(passed, "Coworker not inserted.");
        }

        /// <summary>
        /// Test update evaluation
        /// </summary>
        [TestMethod]
        public void TestupdateEvaluation()
        {
            //Correct Results
            int evaluationID = 15;
            int typeID = 1;

            //Get Evaluation
            Evaluations evaluationsTest = new Evaluations();
            evaluationsTest = EvaluationsDAL.getEvaluation(evaluationID);
            Evaluations evaluationsTestOld = new Evaluations();
            evaluationsTestOld = EvaluationsDAL.getEvaluation(evaluationID);

            //Test Type
            Assert.AreEqual(typeID, evaluationsTest.TypeID, 0, "Type ID not " + typeID + ".");

            //Change
            evaluationsTest.TypeID = 2;
            Boolean passed = EvaluationsDAL.updateEvaluation(evaluationsTestOld, evaluationsTest);
            Evaluations evaluationsTestNew = new Evaluations();
            evaluationsTestNew = EvaluationsDAL.getEvaluation(evaluationID);

            //Retest Type
            Assert.IsTrue(passed, "Evaluation not updated.");
            Assert.AreEqual(2, evaluationsTestNew.TypeID, "Type not right.");

        }
    }
}
