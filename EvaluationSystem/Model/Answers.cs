﻿
namespace EvaluationSystem.Model
{
    public class Answers
    {
        public int EvaluationId { get; set; }
        public int Answer { get; set; }
        public int QuestionId { get; set; }
    }
}
