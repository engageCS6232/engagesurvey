﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EvaluationSystem.Model;

namespace EvaluationSystemTest.ModelTest
{
    [TestClass]
    public class CategoriesTest
    {
        private Categories categories = new Categories();
        private int categoryId = 1;
        private string categoryName = "Test Category";
        private string description = "Test Description";
        private int categoryNum = 1;

        /// <summary>
        /// Test for newly created Categories, all gets show return null
        /// </summary>
        [TestMethod]
        public void TestNullCategories()
        {
            //Return Categories Data
            Assert.AreEqual(0, this.categories.CategoryId, 0, "Categories's CategoryId not 0.");
            Assert.IsNull(this.categories.CategoryName, "Categories's CategoryName not null.");
            Assert.IsNull(this.categories.Description, "Categories's Description not null.");
            Assert.AreEqual(0, this.categories.CategoryNum, 0, "Categories's CategoryNum not 0.");
        }

        /// <summary>
        /// Test for set and get of an Categories [Categories class]
        /// </summary>
        [TestMethod]
        public void TestActiveCategories()
        {
            //Create Categories
            this.categories.CategoryId = this.categoryId;
            this.categories.CategoryName = this.categoryName;
            this.categories.Description = this.description;
            this.categories.CategoryNum = this.categoryNum;

            //Return Categories Data
            Assert.AreEqual(this.categoryId, this.categories.CategoryId, 0, "Categories's CategoryId not " + this.categoryId + ".");
            Assert.AreEqual(this.categoryName, this.categories.CategoryName, "Categories's CategoryName not " + this.categoryName + ".");
            Assert.AreEqual(this.description, this.categories.Description, "Categories's Description not " + this.description + ".");
            Assert.AreEqual(this.categoryNum, this.categories.CategoryNum, 0, "Categories's CategoryNum not " + this.categoryNum + ".");
        }
    }
}