﻿using EvaluationSystem.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluationSystem.DBAccess
{
    public static class StageDAL
    {
        public static List<Stage> GetStageList()
        {
            List<Stage> stageList = new List<Stage>();

            string selectStatment = "SELECT stageID, stageName FROM stage";

            try
            {
                using (SqlConnection connection = EngageDBConnection.GetConnection())
                {
                    using (SqlCommand selectCommand = new SqlCommand(selectStatment, connection))
                    {
                        connection.Open();
                        using (SqlDataReader reader = selectCommand.ExecuteReader()) 
                        {
                            while (reader.Read())
                            {
                                Stage stage = new Stage();
                                stage.StageID = (int)reader["stageID"];
                                stage.StageName = reader["stageName"].ToString();
                                stageList.Add(stage);
                            }
                        }
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            return stageList;
        }
    }
}
