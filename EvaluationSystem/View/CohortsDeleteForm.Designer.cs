﻿namespace EvaluationSystem.View
{
    partial class CohortsDeleteForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.cohortLabel = new System.Windows.Forms.Label();
            this.cbxCohort = new System.Windows.Forms.ComboBox();
            this.cohortBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this._CS6232_g3DataSet = new EvaluationSystem._CS6232_g3DataSet();
            this.deleteButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.modifyButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.employeeButton = new System.Windows.Forms.Button();
            this.cohortButton = new System.Windows.Forms.Button();
            this.employeeListBox = new System.Windows.Forms.ListBox();
            this.employeesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.editCohortBox = new System.Windows.Forms.ListBox();
            this.employeesBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.cohortTableAdapter = new EvaluationSystem._CS6232_g3DataSetTableAdapters.cohortTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.cohortBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._CS6232_g3DataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeesBindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // cohortLabel
            // 
            this.cohortLabel.AutoSize = true;
            this.cohortLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cohortLabel.Location = new System.Drawing.Point(123, 15);
            this.cohortLabel.Name = "cohortLabel";
            this.cohortLabel.Size = new System.Drawing.Size(71, 25);
            this.cohortLabel.TabIndex = 15;
            this.cohortLabel.Text = "Cohort";
            // 
            // cbxCohort
            // 
            this.cbxCohort.DataSource = this.cohortBindingSource;
            this.cbxCohort.DisplayMember = "cohortName";
            this.cbxCohort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxCohort.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxCohort.FormattingEnabled = true;
            this.cbxCohort.Location = new System.Drawing.Point(200, 12);
            this.cbxCohort.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbxCohort.Name = "cbxCohort";
            this.cbxCohort.Size = new System.Drawing.Size(335, 33);
            this.cbxCohort.TabIndex = 14;
            this.cbxCohort.ValueMember = "cohortID";
            // 
            // cohortBindingSource
            // 
            this.cohortBindingSource.DataMember = "cohort";
            this.cohortBindingSource.DataSource = this._CS6232_g3DataSet;
            // 
            // _CS6232_g3DataSet
            // 
            this._CS6232_g3DataSet.DataSetName = "_CS6232_g3DataSet";
            this._CS6232_g3DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // deleteButton
            // 
            this.deleteButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteButton.Location = new System.Drawing.Point(517, 330);
            this.deleteButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(101, 36);
            this.deleteButton.TabIndex = 16;
            this.deleteButton.Text = "Delete";
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelButton.Location = new System.Drawing.Point(633, 330);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(101, 36);
            this.cancelButton.TabIndex = 24;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // modifyButton
            // 
            this.modifyButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modifyButton.Location = new System.Drawing.Point(395, 330);
            this.modifyButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.modifyButton.Name = "modifyButton";
            this.modifyButton.Size = new System.Drawing.Size(101, 36);
            this.modifyButton.TabIndex = 23;
            this.modifyButton.Text = "Update";
            this.modifyButton.UseVisualStyleBackColor = true;
            this.modifyButton.Click += new System.EventHandler(this.modifyButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(114, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 25);
            this.label2.TabIndex = 22;
            this.label2.Text = "Employees";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(503, 81);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(158, 25);
            this.label1.TabIndex = 21;
            this.label1.Text = "Cohort Members";
            // 
            // employeeButton
            // 
            this.employeeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employeeButton.Location = new System.Drawing.Point(331, 197);
            this.employeeButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.employeeButton.Name = "employeeButton";
            this.employeeButton.Size = new System.Drawing.Size(75, 36);
            this.employeeButton.TabIndex = 20;
            this.employeeButton.Text = "< ";
            this.employeeButton.UseVisualStyleBackColor = true;
            this.employeeButton.Click += new System.EventHandler(this.cohortButton_Click);
            // 
            // cohortButton
            // 
            this.cohortButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cohortButton.Location = new System.Drawing.Point(331, 155);
            this.cohortButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cohortButton.Name = "cohortButton";
            this.cohortButton.Size = new System.Drawing.Size(75, 36);
            this.cohortButton.TabIndex = 19;
            this.cohortButton.Text = ">";
            this.cohortButton.UseVisualStyleBackColor = true;
            this.cohortButton.Click += new System.EventHandler(this.employeeButton_Click);
            // 
            // employeeListBox
            // 
            this.employeeListBox.DataSource = this.employeesBindingSource;
            this.employeeListBox.DisplayMember = "EmployeeInfo";
            this.employeeListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employeeListBox.FormattingEnabled = true;
            this.employeeListBox.ItemHeight = 25;
            this.employeeListBox.Location = new System.Drawing.Point(5, 109);
            this.employeeListBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.employeeListBox.Name = "employeeListBox";
            this.employeeListBox.Size = new System.Drawing.Size(311, 154);
            this.employeeListBox.TabIndex = 18;
            this.employeeListBox.ValueMember = "EmployeeID";
            // 
            // employeesBindingSource
            // 
            this.employeesBindingSource.DataSource = typeof(EvaluationSystem.Model.Employees);
            // 
            // editCohortBox
            // 
            this.editCohortBox.DataSource = this.employeesBindingSource;
            this.editCohortBox.DisplayMember = "EmployeeInfo";
            this.editCohortBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editCohortBox.FormattingEnabled = true;
            this.editCohortBox.ItemHeight = 25;
            this.editCohortBox.Location = new System.Drawing.Point(424, 109);
            this.editCohortBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.editCohortBox.Name = "editCohortBox";
            this.editCohortBox.Size = new System.Drawing.Size(300, 154);
            this.editCohortBox.TabIndex = 17;
            this.editCohortBox.ValueMember = "EmployeeID";
            // 
            // employeesBindingSource1
            // 
            this.employeesBindingSource1.DataSource = typeof(EvaluationSystem.Model.Employees);
            // 
            // cohortTableAdapter
            // 
            this.cohortTableAdapter.ClearBeforeFill = true;
            // 
            // CohortsDeleteForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(747, 391);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.modifyButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.employeeButton);
            this.Controls.Add(this.cohortButton);
            this.Controls.Add(this.employeeListBox);
            this.Controls.Add(this.editCohortBox);
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.cohortLabel);
            this.Controls.Add(this.cbxCohort);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "CohortsDeleteForm";
            this.Text = "Edit/Delete Cohort";
            this.Load += new System.EventHandler(this.CohortsDeleteForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cohortBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._CS6232_g3DataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeesBindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label cohortLabel;
        private System.Windows.Forms.ComboBox cbxCohort;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button modifyButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button employeeButton;
        private System.Windows.Forms.Button cohortButton;
        private System.Windows.Forms.ListBox employeeListBox;
        private System.Windows.Forms.ListBox editCohortBox;
        private _CS6232_g3DataSet _CS6232_g3DataSet;
        private System.Windows.Forms.BindingSource cohortBindingSource;
        private _CS6232_g3DataSetTableAdapters.cohortTableAdapter cohortTableAdapter;
        private System.Windows.Forms.BindingSource employeesBindingSource;
        private System.Windows.Forms.BindingSource employeesBindingSource1;
    }
}