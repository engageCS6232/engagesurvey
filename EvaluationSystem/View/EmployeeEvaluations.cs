﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EvaluationSystem.Controller;
using EvaluationSystem.Model;
using System.Data.SqlClient;

namespace EvaluationSystem.View
{
    public partial class EmployeeEvaluations : Form
    {
        private EvaluationControl controller = new EvaluationControl();
        private Employees theEmployee;
        private Form parentForm;

        public EmployeeEvaluations(Employees employee, Form parent)
        {
            InitializeComponent();
            this.theEmployee = employee;
            this.parentForm = parent;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void EmployeeEvaluations_Load(object sender, EventArgs e)
        {
            try
            {
                List<Evaluation> evalList = this.controller.getEvaluationsByEmployee(this.theEmployee.EmployeeID);
                foreach (Evaluation eval in evalList)
                {
                    eval.EvaluatorFirstName = this.theEmployee.FName;
                    eval.EvaluatorLastName = this.theEmployee.LName;
                }

                this.evaluationDataGridView.DataSource = evalList;
                DataGridViewRowCollection rows = this.evaluationDataGridView.Rows;
                foreach (DataGridViewRow row in rows) {
                    DataGridViewCellCollection cells = row.Cells;
                    if (cells["roleName"].Value.ToString() == "Self")
                    {
                        cells["roleName"].Value = "Self Evaluation";
                    }
                    else if (cells["roleName"].Value.ToString() == "Supervisor")
                    {
                        cells["roleName"].Value = "Assigned as Supervisor";
                    }
                    else
                    {
                        cells["roleName"].Value = "Assigned as Co-Worker";
                    }
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        private void evaluationDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 6)
            {
                //MessageBox.Show("Clicked");
                int rowID =  e.RowIndex;
                DataGridViewRow row = evaluationDataGridView.Rows[rowID];
                DataGridViewCell cell = row.Cells[0];
                int evaluationID = (int)cell.Value;
                try
                {
                    TypeOneEvaluation frmTypeOneEval = new TypeOneEvaluation(evaluationID);
                    frmTypeOneEval.MdiParent = parentForm;
                    frmTypeOneEval.Show();
                    frmTypeOneEval.FormClosed += frmTypeOneEval_FormClosed;
                  
                    this.Hide();
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Message, ex.GetType().ToString());
                } 
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, ex.GetType().ToString());
                }

               
               
            }
        }

        void frmTypeOneEval_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Show();
            this.EmployeeEvaluations_Load(sender, e);
        }

    }
}
