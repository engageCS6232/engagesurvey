﻿using System;

namespace EvaluationSystem.Model
{
    public class Evaluation
    {
        public int EvaluationID { get; set; }
        public string StageName { get; set; }
        public string RoleName { get; set; }
        public DateTime? CompletionDate { get; set; }
        public string TypeName { get; set; }
        public string ParticipantFirstName { get; set; }
        public string ParticipantLastName { get; set; }
        public string Evaluator { get; set; }
        public DateTime? DueDate { get; set; }
        public int StageID { get; set; }
        public int RoleID { get; set; }
        public int TypeID { get; set; }
        public int ParticipantID { get; set; }
        public int EvaluatorID { get; set; }
        public string EvaluatorFirstName { get; set; }
        public string EvaluatorLastName { get; set; }


        public string ParticipantInfo
        {
            get
            {
                String employeeInfo = this.ParticipantFirstName+ ", " + this.ParticipantLastName + "(" + this.ParticipantID + ")";
                return employeeInfo;
            }
        }

        public string EvaluatorInfo
        {
            get
            {
                String employeeInfo = this.EvaluatorFirstName + ", " + this.EvaluatorLastName + "(" + this.EvaluatorID + ")";
                return employeeInfo;
            }
        }
    }

}
