﻿using EvaluationSystem.Controller;
using EvaluationSystem.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;

namespace EvaluationSystem.View
{
    public partial class EvaluatorForm : Form
    {
        private EvaluationControl evaluationControl;
        private Employees newEmployee;
        private Evaluations newEvalutation;
        private BindingList<Employees> coworkerMemberList;
        private List<Employees> employeeList;
        private int supervisorID;
        private int coworkerID;
        private int evaluationID;


        public EvaluatorForm()
        {
            InitializeComponent();
            this.evaluationControl = new EvaluationControl();
        }

        private void EvaluatorForm_Load(object sender, EventArgs e)
        {
            if (this.newEmployee.CohortID != null && this.newEmployee.EmployeeID != 0)
            {
                this.LoadEvaluationsBox(this.newEmployee.EmployeeID);
                this.LoadComboBox(this.newEmployee.CohortID.Value);
            }
            this.employeeList = new List<Employees>();
            this.coworkerMemberList = new BindingList<Employees>();

        }

        /// <summary>
        /// Loads the evaluations into the combo box.
        /// </summary>
        /// <param name="employeeID"></param>
        private void LoadEvaluationsBox(int employeeID)
        {
            List<Evaluations> tempList = new List<Evaluations>();
            List<Evaluations> dataList = new List<Evaluations>();
            Evaluations lastEvaluation = new Evaluations();
            try
            {
                //Gets a list of evaluations from the database that are assigned to the cohort.
                evaluationBox.DataSource = this.evaluationControl.getEvaluationsOpenForEmployeeToAssign(employeeID);
               
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }

            
            evaluationBox.DisplayMember = "EvaluationInfoNoID";
            evaluationBox.ValueMember = "EvaluationID";
            if (evaluationBox.SelectedValue != null)
            {
                bool parseOK = Int32.TryParse(evaluationBox.SelectedValue.ToString(), out evaluationID);
            }
        }

        /// <summary>
        /// Resets the evaluationID to the value selected in the combo box.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void evaluationsListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (evaluationBox.SelectedValue != null)
            {
                bool parseOK = Int32.TryParse(evaluationBox.SelectedValue.ToString(), out evaluationID);
            }
            this.setEvaluationFromSelected();
        }

        /// <summary>
        /// Gets the evaluation based on the evaluationID.
        /// </summary>
        private void setEvaluationFromSelected()
        {
            this.newEvalutation = new Evaluations();

            try
            {
                this.newEvalutation = this.evaluationControl.getEvaluation(evaluationID);
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        /// <summary>
        /// Loads the cohort members into the supervisor combo box (minus the user).
        /// </summary>
        /// <param name="cohortID"></param>
        private void LoadComboBox(int cohortID)
        {
            try
            {
                supervisorBox.DataSource = this.evaluationControl.getEmployeeList(cohortID, newEmployee.EmployeeID, 0);
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
            supervisorBox.DisplayMember = "employeeInfo";
            supervisorBox.ValueMember = "EmployeeID";
            //Adds blank field to combo box so a default value does not exist, thus preventing the listbox from populating prematurely.  
            supervisorBox.SelectedIndex = -1;
            if (supervisorBox.SelectedValue != null)
            {
                bool parseOK = Int32.TryParse(supervisorBox.SelectedValue.ToString(), out this.supervisorID);
            }

        }

        /// <summary>
        /// Resets the supervisorID to the value selected in the combo box.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void supervisorBox_SelectedIndexChange(object sender, EventArgs e)
        {
            if (supervisorBox.SelectedIndex != -1 && supervisorBox.SelectedValue != null)
            {
                bool parseOK = Int32.TryParse(supervisorBox.SelectedValue.ToString(), out this.supervisorID);
                this.loadListBox();
                this.updateCoworkerListBoxBinding();
                this.updateEmployeeListBoxBinding();
            }
            else
            {
                employeeListBox.DataSource = null;
                employeeListBox.Items.Clear();
            }
        }

        /// <summary>
        ///  Loads the employeeList box.
        /// </summary>
        private void loadListBox()
        {
            try
            {
                this.employeeList = this.evaluationControl.getEmployeeList(this.newEmployee.CohortID.Value, this.newEmployee.EmployeeID, this.supervisorID);
                employeeListBox.DataSource = this.employeeList;

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        /// <summary>
        /// Adds employee to the coworkerMemberList box and removes the employee from the employeeList box.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void coworkerButton_Click(object sender, EventArgs e)
        {
            if (this.employeeList.Count() > 0)
            {
                this.coworkerMemberList.Add(this.employeeList[employeeListBox.SelectedIndex]);
                this.employeeList.Remove(this.employeeList[employeeListBox.SelectedIndex]);
                this.updateEmployeeListBoxBinding();
            }
            //Disables and enables buttons.  This number would need to be changed if the number of coworkers required changes per the client.
            if (this.coworkerMemberList.Count() >= 1 && this.evaluationBox.SelectedText == "")
            {
                coworkerButton.Enabled = false;
                submitButton.Enabled = true;
            }
        }


        /// <summary>
        /// Adds an employee to the employeeList box and removes it from the coworkerMemberList box.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void employeeButton_Click(object sender, EventArgs e)
        {
            if (this.coworkerMemberList.Count() > 0)
            {
                this.employeeList.Add(this.coworkerMemberList[coworkerListBox.SelectedIndex]);
                this.coworkerMemberList.Remove(this.coworkerMemberList[coworkerListBox.SelectedIndex]);
                this.updateEmployeeListBoxBinding();
            }
            if (this.coworkerMemberList.Count() <= 0)
            {
                coworkerButton.Enabled = true;
                submitButton.Enabled = false;
            }
        }

        /// <summary>
        /// Updates the binding on the employeeList box so the display will update when
        /// items are added to or removed from the list. 
        /// </summary>
        private void updateEmployeeListBoxBinding()
        {
            employeeListBox.DataSource = null;
            employeeListBox.Items.Clear();
            employeeListBox.DataSource = this.employeeList;
            employeeListBox.DisplayMember = "employeeInfo";
            employeeListBox.ValueMember = "EmployeeID";
            coworkerListBox.DataSource = this.coworkerMemberList;
        }

        /// <summary>
        /// Updates the binding on the coworkerList box so the display will update when
        /// items are added to or removed from the list. 
        /// </summary>
        private void updateCoworkerListBoxBinding()
        {
            this.coworkerMemberList = new BindingList<Employees>();
            coworkerListBox.DataSource = this.coworkerMemberList;
            coworkerListBox.DisplayMember = "employeeInfo";
            coworkerListBox.ValueMember = "EmployeeID";
            coworkerButton.Enabled = true;
        }

        /// <summary>
        /// Closes the Evaluator Form without performing any other action. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Subits the Evaluator Form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void submitButton_Click(object sender, EventArgs e)
        {
            this.setEvaluationFromSelected();
            if (this.newEmployee != null && this.supervisorID != 0 && this.coworkerMemberList != null && this.newEvalutation != null)
            {
                try
                {
                    bool isSuccesful = evaluationControl.insertEmployeesSupervisor(this.newEmployee.EmployeeID, this.supervisorID, this.newEvalutation.StageID, this.newEvalutation.TypeID);
                    foreach (Employees current in this.coworkerMemberList)
                    {
                       this.coworkerID = current.EmployeeID;
                       isSuccesful = evaluationControl.insertEmployeesCoworker(this.newEmployee.EmployeeID, this.coworkerID, this.newEvalutation.StageID, this.newEvalutation.TypeID);
                    }
                    if (isSuccesful)
                    {
                        this.Close();
                        MessageBox.Show("Supervisor and Coworker assigned for the selected evaluation.", "Update Successful");
                    }
                    else
                    {
                        MessageBox.Show("Unable to assign Supervisor and Coworker for the selected evaluation.", "Error");
                    }

                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Message.ToString());
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message.ToString());
                }
            }
            else
            {
                MessageBox.Show("Unable To assign Supervisor and Coworker for the selected evaluation.  Please select all required fields and click Submit again.", "Error");
            }

        }

        /// <summary>
        /// Loads the employee's information into the system.  
        /// </summary>
        /// <param name="passedEmployee"></param>
        public void LoadEmployee(Employees passedEmployee)
        {
            this.newEmployee = passedEmployee;
        }

    }
}
