﻿namespace EvaluationSystem.View
{
    partial class EvaluationCreateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.cbxType = new System.Windows.Forms.ComboBox();
            this.evaluationTypesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.evalTrackerTypeAndStageDataSet = new EvaluationSystem.EvalTrackerTypeAndStageDataSet();
            this.startDatePicker = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.endDatePicker = new System.Windows.Forms.DateTimePicker();
            this.cbxCohort = new System.Windows.Forms.ComboBox();
            this.cohortBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this._CS6232_g3DataSet = new EvaluationSystem._CS6232_g3DataSet();
            this.label6 = new System.Windows.Forms.Label();
            this.submitButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.cohortTableAdapter = new EvaluationSystem._CS6232_g3DataSetTableAdapters.cohortTableAdapter();
            this.fKcohortevaltrackerstageBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.stageBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.evalTrackerTypeAndStageDataSet1 = new EvaluationSystem.EvalTrackerTypeAndStageDataSet();
            this.stageLabel = new System.Windows.Forms.Label();
            this.cohortevaltrackerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.typesTableAdapter = new EvaluationSystem.EvalTrackerTypeAndStageDataSetTableAdapters.typesTableAdapter();
            this.stageTableAdapter = new EvaluationSystem.EvalTrackerTypeAndStageDataSetTableAdapters.stageTableAdapter();
            this.cohort_eval_trackerTableAdapter = new EvaluationSystem.EvalTrackerTypeAndStageDataSetTableAdapters.cohort_eval_trackerTableAdapter();
            this.cbxStage = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.evaluationTypesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.evalTrackerTypeAndStageDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cohortBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._CS6232_g3DataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKcohortevaltrackerstageBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stageBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.evalTrackerTypeAndStageDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cohortevaltrackerBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // cbxType
            // 
            this.cbxType.DataSource = this.evaluationTypesBindingSource;
            this.cbxType.DisplayMember = "typeName";
            this.cbxType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxType.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxType.FormattingEnabled = true;
            this.cbxType.Location = new System.Drawing.Point(16, 107);
            this.cbxType.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cbxType.Name = "cbxType";
            this.cbxType.Size = new System.Drawing.Size(117, 28);
            this.cbxType.TabIndex = 2;
            this.cbxType.ValueMember = "typeID";
            // 
            // evaluationTypesBindingSource
            // 
            this.evaluationTypesBindingSource.DataMember = "types";
            this.evaluationTypesBindingSource.DataSource = this.evalTrackerTypeAndStageDataSet;
            // 
            // evalTrackerTypeAndStageDataSet
            // 
            this.evalTrackerTypeAndStageDataSet.DataSetName = "EvalTrackerTypeAndStageDataSet";
            this.evalTrackerTypeAndStageDataSet.EnforceConstraints = false;
            this.evalTrackerTypeAndStageDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // startDatePicker
            // 
            this.startDatePicker.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startDatePicker.Location = new System.Drawing.Point(16, 181);
            this.startDatePicker.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.startDatePicker.Name = "startDatePicker";
            this.startDatePicker.Size = new System.Drawing.Size(332, 26);
            this.startDatePicker.TabIndex = 3;
            this.startDatePicker.ValueChanged += new System.EventHandler(this.updateEndDate);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(13, 84);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 20);
            this.label2.TabIndex = 6;
            this.label2.Text = "Evaluation Type";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(184, 84);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(134, 20);
            this.label3.TabIndex = 7;
            this.label3.Text = "Evaluation Stage:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(13, 158);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 20);
            this.label4.TabIndex = 8;
            this.label4.Text = "Start Date";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(13, 211);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 20);
            this.label5.TabIndex = 9;
            this.label5.Text = "End Date";
            // 
            // endDatePicker
            // 
            this.endDatePicker.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.endDatePicker.Location = new System.Drawing.Point(16, 237);
            this.endDatePicker.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.endDatePicker.Name = "endDatePicker";
            this.endDatePicker.Size = new System.Drawing.Size(332, 26);
            this.endDatePicker.TabIndex = 4;
            // 
            // cbxCohort
            // 
            this.cbxCohort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxCohort.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxCohort.FormattingEnabled = true;
            this.cbxCohort.Location = new System.Drawing.Point(16, 31);
            this.cbxCohort.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cbxCohort.Name = "cbxCohort";
            this.cbxCohort.Size = new System.Drawing.Size(337, 28);
            this.cbxCohort.TabIndex = 1;
            // 
            // cohortBindingSource
            // 
            this.cohortBindingSource.DataMember = "cohort";
            this.cohortBindingSource.DataSource = this._CS6232_g3DataSet;
            // 
            // _CS6232_g3DataSet
            // 
            this._CS6232_g3DataSet.DataSetName = "_CS6232_g3DataSet";
            this._CS6232_g3DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(13, 8);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 20);
            this.label6.TabIndex = 13;
            this.label6.Text = "Cohort";
            // 
            // submitButton
            // 
            this.submitButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.submitButton.Location = new System.Drawing.Point(188, 280);
            this.submitButton.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.submitButton.Name = "submitButton";
            this.submitButton.Size = new System.Drawing.Size(73, 34);
            this.submitButton.TabIndex = 5;
            this.submitButton.Text = "Submit";
            this.submitButton.UseVisualStyleBackColor = true;
            this.submitButton.Click += new System.EventHandler(this.submitButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelButton.Location = new System.Drawing.Point(274, 280);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(73, 34);
            this.cancelButton.TabIndex = 6;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // cohortTableAdapter
            // 
            this.cohortTableAdapter.ClearBeforeFill = true;
            // 
            // fKcohortevaltrackerstageBindingSource
            // 
            this.fKcohortevaltrackerstageBindingSource.DataMember = "FK_cohort_eval_tracker_stage";
            this.fKcohortevaltrackerstageBindingSource.DataSource = this.stageBindingSource;
            // 
            // stageBindingSource
            // 
            this.stageBindingSource.DataMember = "stage";
            this.stageBindingSource.DataSource = this.evalTrackerTypeAndStageDataSet1;
            // 
            // evalTrackerTypeAndStageDataSet1
            // 
            this.evalTrackerTypeAndStageDataSet1.DataSetName = "EvalTrackerTypeAndStageDataSet";
            this.evalTrackerTypeAndStageDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // stageLabel
            // 
            this.stageLabel.AutoSize = true;
            this.stageLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stageLabel.Location = new System.Drawing.Point(296, 110);
            this.stageLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.stageLabel.Name = "stageLabel";
            this.stageLabel.Size = new System.Drawing.Size(0, 20);
            this.stageLabel.TabIndex = 17;
            // 
            // cohortevaltrackerBindingSource
            // 
            this.cohortevaltrackerBindingSource.DataMember = "cohort_eval_tracker";
            this.cohortevaltrackerBindingSource.DataSource = this.evalTrackerTypeAndStageDataSet1;
            // 
            // typesTableAdapter
            // 
            this.typesTableAdapter.ClearBeforeFill = true;
            // 
            // stageTableAdapter
            // 
            this.stageTableAdapter.ClearBeforeFill = true;
            // 
            // cohort_eval_trackerTableAdapter
            // 
            this.cohort_eval_trackerTableAdapter.ClearBeforeFill = true;
            // 
            // cbxStage
            // 
            this.cbxStage.DataSource = this.evaluationTypesBindingSource;
            this.cbxStage.DisplayMember = "typeName";
            this.cbxStage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxStage.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxStage.FormattingEnabled = true;
            this.cbxStage.Location = new System.Drawing.Point(188, 107);
            this.cbxStage.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cbxStage.Name = "cbxStage";
            this.cbxStage.Size = new System.Drawing.Size(130, 28);
            this.cbxStage.TabIndex = 18;
            this.cbxStage.ValueMember = "typeID";
            // 
            // EvaluationCreateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(364, 336);
            this.Controls.Add(this.cbxStage);
            this.Controls.Add(this.stageLabel);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.submitButton);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cbxCohort);
            this.Controls.Add(this.endDatePicker);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.startDatePicker);
            this.Controls.Add(this.cbxType);
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Name = "EvaluationCreateForm";
            this.Text = "Create Evaluation";
            this.Load += new System.EventHandler(this.CreateCohortForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.evaluationTypesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.evalTrackerTypeAndStageDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cohortBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._CS6232_g3DataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKcohortevaltrackerstageBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stageBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.evalTrackerTypeAndStageDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cohortevaltrackerBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbxType;
        private System.Windows.Forms.DateTimePicker startDatePicker;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker endDatePicker;
        private System.Windows.Forms.ComboBox cbxCohort;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button submitButton;
        private System.Windows.Forms.Button cancelButton;
        private _CS6232_g3DataSet _CS6232_g3DataSet;
        private System.Windows.Forms.BindingSource cohortBindingSource;
        private _CS6232_g3DataSetTableAdapters.cohortTableAdapter cohortTableAdapter;
        private EvalTrackerTypeAndStageDataSet evalTrackerTypeAndStageDataSet;
        private EvalTrackerTypeAndStageDataSetTableAdapters.typesTableAdapter typesTableAdapter;
        private EvalTrackerTypeAndStageDataSet evalTrackerTypeAndStageDataSet1;
        private System.Windows.Forms.BindingSource stageBindingSource;
        private EvalTrackerTypeAndStageDataSetTableAdapters.stageTableAdapter stageTableAdapter;
        private System.Windows.Forms.BindingSource fKcohortevaltrackerstageBindingSource;
        private EvalTrackerTypeAndStageDataSetTableAdapters.cohort_eval_trackerTableAdapter cohort_eval_trackerTableAdapter;
        private System.Windows.Forms.BindingSource cohortevaltrackerBindingSource;
        private System.Windows.Forms.Label stageLabel;
        private System.Windows.Forms.BindingSource evaluationTypesBindingSource;
        private System.Windows.Forms.ComboBox cbxStage;

    }
}