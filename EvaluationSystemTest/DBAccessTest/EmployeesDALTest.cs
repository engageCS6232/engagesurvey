﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EvaluationSystem.Model;
using EvaluationSystem.DBAccess;
using System.Collections.Generic;

namespace EvaluationSystemTest.DBAccessTest
{
    [TestClass]
    public class EmployeesDALTest
    {
        /// <summary>
        /// Test for return of matching employee data
        /// </summary>
        [TestMethod]
        public void TestGetEmployeeData()
        {
            //Correct Results
            Employees employee = new Employees();
            List<Employees> employeeList = new List<Employees>();
            int employeeID = 1;
            String fName = "Liam";
            String lName = "Smith";
            String email = "lsmith@engage.com";
            String username = "lsmith";
            int cohortID = 2;
            Boolean administrator = false;
            String employeeInfo;

            //Get Employee
            employee = EmployeesDAL.getEmployeeData(username);
            employeeInfo = lName + ", " + fName + "(" + employeeID + ")";

            //Check Employee Results
            Assert.AreEqual(employeeID, employee.EmployeeID, 0, "Employees's EmployeeID not " + employeeID + ".");
            Assert.AreEqual(fName, employee.FName, "Employees's FName not " + fName + ".");
            Assert.AreEqual(lName, employee.LName, "Employees's LName not " + lName + ".");
            Assert.AreEqual(email, employee.Email, "Employees's Email not " + email + ".");
            Assert.IsNull(employee.Username, "Employees's Username not null.");
            Assert.AreEqual(administrator, employee.Administrator, "Employees's Administrator not " + administrator + ".");
            Assert.AreEqual(cohortID, employee.CohortID, "Employees's CohortID not " + cohortID + ".");
            Assert.AreEqual(employeeInfo, employee.EmployeeInfo, "Employee's EmployeeInfo not " + employeeInfo + ".");
        }

        /// <summary>
        /// Test for return comparing usernames and passwords
        /// </summary>
        [TestMethod]
        public void TestComparePassword()
        {
            //Correct Results
            Employees employee = new Employees();
            List<Employees> employeeList = new List<Employees>();
            String username = "lsmith";
            String password = "K53eYsGN";

            //Get Employee
            Boolean passed = EmployeesDAL.ComparePassword(username, password);
            Boolean failedUsername = EmployeesDAL.ComparePassword("JDoe", password);
            Boolean failedPassword = EmployeesDAL.ComparePassword(username, "password");

            //Check Username Password Results
            Assert.IsTrue(passed, "Correct Username and Password failed.");
            Assert.IsFalse(failedUsername, "Wrong Username passed.");
            Assert.IsFalse(failedPassword, "Wrong Password passed.");
        }

        /// <summary>
        /// Test for return list of employees
        /// </summary>
        [TestMethod]
        public void TestGetEmployeesList()
        {
            //Correct Results
            Employees employee = new Employees();
            List<Employees> employeeList = new List<Employees>();
            int employeeIDList = 27;
            int employeeID = 11;
            String fName = "Logan";
            String lName = "Anderson";
            Boolean administrator = false;
            String employeeInfo;
            int cohortID = 1;
            int supervisorID = 3;

            //Get EmployeesList and Sample Employee
            employeeList = EmployeesDAL.getEmployeeList(cohortID, employeeIDList, supervisorID);
            employeeInfo = lName + ", " + fName + "(" + employeeID + ")";
            employee = employeeList[0];

            //Check Employee List Employee
            Assert.AreEqual(employeeID, employee.EmployeeID, 0, "Employees's EmployeeID not " + employeeID + ".");
            Assert.AreEqual(fName, employee.FName, "Employees's FName not " + fName + ".");
            Assert.AreEqual(lName, employee.LName, "Employees's LName not " + lName + ".");
            Assert.IsNull(employee.Email, "Employees's Email not null.");
            Assert.IsNull(employee.Username, "Employees's Username not null.");
            Assert.AreEqual(administrator, employee.Administrator, "Employees's Administrator not " + administrator + ".");
            Assert.IsNull(employee.CohortID, "Employees's CohortID not null.");
            Assert.AreEqual(employeeInfo, employee.EmployeeInfo, "Employee's EmployeeInfo not " + employeeInfo + ".");
        }

        /// <summary>
        /// Test for return list of employees 
        /// </summary>
        [TestMethod]
        public void TestGetEmployeesWithoutCohorts()
        {
            //Correct Results
            Employees employee = new Employees();
            List<Employees> employeeList = new List<Employees>();
            int employeeID = 2;
            String fName = "Emma";
            String lName = "Johnson";
            Boolean administrator = false;
            String employeeInfo;

            //Get Employees Without Cohort
            employeeList = EmployeesDAL.GetEmployeesWithoutCohorts();
            employeeInfo = lName + ", " + fName + "(" + employeeID + ")";
            employee = employeeList[0];

            //Check Employee List Employee
            Assert.AreEqual(employeeID, employee.EmployeeID, 0, "Employees's EmployeeID not " + employeeID + ".");
            Assert.AreEqual(fName, employee.FName, "Employees's FName not " + fName + ".");
            Assert.AreEqual(lName, employee.LName, "Employees's LName not " + lName + ".");
            Assert.IsNull(employee.Email, "Employees's Email not null.");
            Assert.IsNull(employee.Username, "Employees's Username not null.");
            Assert.AreEqual(administrator, employee.Administrator, "Employees's Administrator not " + administrator + ".");
            Assert.IsNull(employee.CohortID, "Employees's CohortID not null.");
            Assert.AreEqual(employeeInfo, employee.EmployeeInfo, "Employee's EmployeeInfo not " + employeeInfo + ".");
        }
    }
}