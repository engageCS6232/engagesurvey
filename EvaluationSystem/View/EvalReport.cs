﻿using EvaluationSystem.Controller;
using EvaluationSystem.Model;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EvaluationSystem.View
{
    public partial class EvalReport : Form
    {
        private int employeeID;
        private int stageID;
        private int typeID;
        private EvaluationControl newControl;

        public EvalReport()
        {
            InitializeComponent();
            this.newControl = new EvaluationControl();
        }

        /// <summary>
        /// Loads the employeeID, stageID, and typeID to the report.
        /// </summary>
        /// <param name="employeeID"></param>
        /// <param name="stageID"></param>
        /// <param name="typeID"></param>
        public void load(int employeeID, int stageID, int typeID)
        {
            this.employeeID = employeeID;
            this.stageID = stageID;
            this.typeID = typeID;
            
        }

        /// <summary>
        /// Loads the data for the report.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EvalReport_Load(object sender, EventArgs e)
        {
            try
            {
                Employees newEmployee = new Employees();
                String username = "";

                //Sets the user name to the results of the first query.
                username = this.newControl.getEmployeeUserName(this.employeeID);

                //Builds the employee data based on the results of the first query.
                newEmployee = this.newControl.getEmployeeData(username);

                //Gets the report data from the dataset.
                this.evaluationReportTableAdapter.Fill(this.reports.evaluationReport, stageID, typeID, employeeID);

                //Builds the report parameters and sets them.
                ReportParameter[] parameters = new ReportParameter[3];
                parameters[0] = new ReportParameter("name", ""+newEmployee.FName+" "+newEmployee.LName+"");
                parameters[1] = new ReportParameter("stage", this.stageID.ToString());
                parameters[2] = new ReportParameter("type", this.typeID.ToString());

                this.reportViewer1.LocalReport.SetParameters(parameters);
                this.reportViewer1.RefreshReport();

            } catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }
    }
}
