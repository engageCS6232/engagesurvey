﻿namespace EvaluationSystem.View
{
    partial class EvaluatorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.employeeLabel = new System.Windows.Forms.Label();
            this.supervisorLabel = new System.Windows.Forms.Label();
            this.supervisorBox = new System.Windows.Forms.ComboBox();
            this.submitButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.employeeListBox = new System.Windows.Forms.ListBox();
            this.employeesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this._CS6232_g3DataSet = new EvaluationSystem._CS6232_g3DataSet();
            this.employeeButton = new System.Windows.Forms.Button();
            this.coworkerButton = new System.Windows.Forms.Button();
            this.coworkerListBox = new System.Windows.Forms.ListBox();
            this.coworkerLabel = new System.Windows.Forms.Label();
            this.employeesTableAdapter = new EvaluationSystem._CS6232_g3DataSetTableAdapters.employeesTableAdapter();
            this.evaluationLabel = new System.Windows.Forms.Label();
            this.evaluationBox = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.employeesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._CS6232_g3DataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // employeeLabel
            // 
            this.employeeLabel.AutoSize = true;
            this.employeeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employeeLabel.Location = new System.Drawing.Point(103, 127);
            this.employeeLabel.Name = "employeeLabel";
            this.employeeLabel.Size = new System.Drawing.Size(109, 25);
            this.employeeLabel.TabIndex = 24;
            this.employeeLabel.Text = "Employees";
            // 
            // supervisorLabel
            // 
            this.supervisorLabel.AutoSize = true;
            this.supervisorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.supervisorLabel.Location = new System.Drawing.Point(164, 73);
            this.supervisorLabel.Name = "supervisorLabel";
            this.supervisorLabel.Size = new System.Drawing.Size(106, 25);
            this.supervisorLabel.TabIndex = 23;
            this.supervisorLabel.Text = "Supervisor";
            // 
            // supervisorBox
            // 
            this.supervisorBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.supervisorBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.supervisorBox.FormattingEnabled = true;
            this.supervisorBox.Location = new System.Drawing.Point(276, 69);
            this.supervisorBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.supervisorBox.Name = "supervisorBox";
            this.supervisorBox.Size = new System.Drawing.Size(257, 33);
            this.supervisorBox.TabIndex = 21;
            this.supervisorBox.SelectedIndexChanged += new System.EventHandler(this.supervisorBox_SelectedIndexChange);
            // 
            // submitButton
            // 
            this.submitButton.Enabled = false;
            this.submitButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.submitButton.Location = new System.Drawing.Point(476, 346);
            this.submitButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.submitButton.Name = "submitButton";
            this.submitButton.Size = new System.Drawing.Size(107, 38);
            this.submitButton.TabIndex = 25;
            this.submitButton.Text = "Submit";
            this.submitButton.UseVisualStyleBackColor = true;
            this.submitButton.Click += new System.EventHandler(this.submitButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelButton.Location = new System.Drawing.Point(625, 346);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(107, 38);
            this.cancelButton.TabIndex = 26;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // employeeListBox
            // 
            this.employeeListBox.DataSource = this.employeesBindingSource;
            this.employeeListBox.DisplayMember = "fName";
            this.employeeListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employeeListBox.FormattingEnabled = true;
            this.employeeListBox.ItemHeight = 25;
            this.employeeListBox.Location = new System.Drawing.Point(13, 155);
            this.employeeListBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.employeeListBox.Name = "employeeListBox";
            this.employeeListBox.Size = new System.Drawing.Size(300, 154);
            this.employeeListBox.TabIndex = 27;
            this.employeeListBox.ValueMember = "employeeID";
            // 
            // employeesBindingSource
            // 
            this.employeesBindingSource.DataMember = "employees";
            this.employeesBindingSource.DataSource = this._CS6232_g3DataSet;
            // 
            // _CS6232_g3DataSet
            // 
            this._CS6232_g3DataSet.DataSetName = "_CS6232_g3DataSet";
            this._CS6232_g3DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // employeeButton
            // 
            this.employeeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employeeButton.Location = new System.Drawing.Point(336, 250);
            this.employeeButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.employeeButton.Name = "employeeButton";
            this.employeeButton.Size = new System.Drawing.Size(75, 36);
            this.employeeButton.TabIndex = 30;
            this.employeeButton.Text = "< ";
            this.employeeButton.UseVisualStyleBackColor = true;
            this.employeeButton.Click += new System.EventHandler(this.employeeButton_Click);
            // 
            // coworkerButton
            // 
            this.coworkerButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.coworkerButton.Location = new System.Drawing.Point(336, 208);
            this.coworkerButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.coworkerButton.Name = "coworkerButton";
            this.coworkerButton.Size = new System.Drawing.Size(75, 36);
            this.coworkerButton.TabIndex = 29;
            this.coworkerButton.Text = ">";
            this.coworkerButton.UseVisualStyleBackColor = true;
            this.coworkerButton.Click += new System.EventHandler(this.coworkerButton_Click);
            // 
            // coworkerListBox
            // 
            this.coworkerListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.coworkerListBox.FormattingEnabled = true;
            this.coworkerListBox.ItemHeight = 25;
            this.coworkerListBox.Location = new System.Drawing.Point(428, 155);
            this.coworkerListBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.coworkerListBox.Name = "coworkerListBox";
            this.coworkerListBox.Size = new System.Drawing.Size(311, 154);
            this.coworkerListBox.TabIndex = 28;
            // 
            // coworkerLabel
            // 
            this.coworkerLabel.AutoSize = true;
            this.coworkerLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.coworkerLabel.Location = new System.Drawing.Point(529, 127);
            this.coworkerLabel.Name = "coworkerLabel";
            this.coworkerLabel.Size = new System.Drawing.Size(133, 25);
            this.coworkerLabel.TabIndex = 31;
            this.coworkerLabel.Text = "Co-Worker(s)";
            // 
            // employeesTableAdapter
            // 
            this.employeesTableAdapter.ClearBeforeFill = true;
            // 
            // evaluationLabel
            // 
            this.evaluationLabel.AutoSize = true;
            this.evaluationLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.evaluationLabel.Location = new System.Drawing.Point(164, 17);
            this.evaluationLabel.Name = "evaluationLabel";
            this.evaluationLabel.Size = new System.Drawing.Size(103, 25);
            this.evaluationLabel.TabIndex = 33;
            this.evaluationLabel.Text = "Evaluation";
            // 
            // evaluationBox
            // 
            this.evaluationBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.evaluationBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.evaluationBox.FormattingEnabled = true;
            this.evaluationBox.Location = new System.Drawing.Point(276, 14);
            this.evaluationBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.evaluationBox.Name = "evaluationBox";
            this.evaluationBox.Size = new System.Drawing.Size(257, 33);
            this.evaluationBox.TabIndex = 32;
            this.evaluationBox.SelectedIndexChanged += new System.EventHandler(this.evaluationsListBox_SelectedIndexChanged);
            // 
            // EvaluatorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(749, 398);
            this.Controls.Add(this.evaluationLabel);
            this.Controls.Add(this.evaluationBox);
            this.Controls.Add(this.coworkerLabel);
            this.Controls.Add(this.employeeButton);
            this.Controls.Add(this.coworkerButton);
            this.Controls.Add(this.coworkerListBox);
            this.Controls.Add(this.employeeListBox);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.submitButton);
            this.Controls.Add(this.employeeLabel);
            this.Controls.Add(this.supervisorLabel);
            this.Controls.Add(this.supervisorBox);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "EvaluatorForm";
            this.Text = "Evaluators";
            this.Load += new System.EventHandler(this.EvaluatorForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.employeesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._CS6232_g3DataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label employeeLabel;
        private System.Windows.Forms.Label supervisorLabel;
        private System.Windows.Forms.ComboBox supervisorBox;
        private System.Windows.Forms.Button submitButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.ListBox employeeListBox;
        private System.Windows.Forms.Button employeeButton;
        private System.Windows.Forms.Button coworkerButton;
        private System.Windows.Forms.ListBox coworkerListBox;
        private System.Windows.Forms.Label coworkerLabel;
        private _CS6232_g3DataSet _CS6232_g3DataSet;
        private System.Windows.Forms.BindingSource employeesBindingSource;
        private _CS6232_g3DataSetTableAdapters.employeesTableAdapter employeesTableAdapter;
        private System.Windows.Forms.Label evaluationLabel;
        private System.Windows.Forms.ComboBox evaluationBox;
    }
}