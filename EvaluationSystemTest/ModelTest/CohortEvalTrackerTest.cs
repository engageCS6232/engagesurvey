﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EvaluationSystem.Model;

namespace EvaluationSystemTest.ModelTest
{
    [TestClass]
    public class CohortEvalTrackerTest
    {
        private CohortEvalTracker cohortEvalTracker = new CohortEvalTracker();
        private int cohortID = 1;
        private int typeID = 1;
        private int stageID = 1;
        private DateTime startDate = DateTime.Now.Date;
        private DateTime endDate = DateTime.Now.AddDays(2).Date;

        /// <summary>
        /// Test for newly created CohortEvalTracker, all gets show return null
        /// </summary>
        [TestMethod]
        public void TestNullCohortEvalTracker()
        {
            //Return CohortEvalTracker Data
            Assert.AreEqual(0, this.cohortEvalTracker.cohortID, 0, "CohortEvalTracker's cohortID not 0.");
            Assert.AreEqual(0, this.cohortEvalTracker.typeID, 0, "CohortEvalTracker's typeID not 0.");
            Assert.AreEqual(0, this.cohortEvalTracker.stageID, 0, "CohortEvalTracker's stageID not 0.");
            Assert.AreEqual("1/1/0001", this.cohortEvalTracker.startDate.ToShortDateString(), "CohortEvalTracker's startDate not 1/1/0001.");
            Assert.AreEqual("1/1/0001", this.cohortEvalTracker.endDate.ToShortDateString(), "CohortEvalTracker's endDate not 1/1/0001.");
        }

        /// <summary>
        /// Test for set and get of an cohortEvalTracker [CohortEvalTracker class]
        /// </summary>
        [TestMethod]
        public void TestActiveCohortEvalTracker()
        {
            //Create CohortEvalTracker
            this.cohortEvalTracker.cohortID = this.cohortID;
            this.cohortEvalTracker.typeID = this.typeID;
            this.cohortEvalTracker.stageID = this.stageID;
            this.cohortEvalTracker.startDate = this.startDate;
            this.cohortEvalTracker.endDate = this.endDate;

            //Return CohortEvalTracker Data
            Assert.AreEqual(this.cohortID, this.cohortEvalTracker.cohortID, 0, "CohortEvalTracker's cohortID not " + this.cohortID + ".");
            Assert.AreEqual(this.typeID, this.cohortEvalTracker.typeID, 0, "CohortEvalTracker's typeID not " + this.typeID + ".");
            Assert.AreEqual(this.stageID, this.cohortEvalTracker.stageID, 0, "CohortEvalTracker's stageID not " + this.stageID + ".");
            Assert.AreEqual(this.startDate, this.cohortEvalTracker.startDate, "CohortEvalTracker's startDate not " + this.startDate + ".");
            Assert.AreEqual(this.endDate, this.cohortEvalTracker.endDate, "CohortEvalTracker's endDate not " + this.endDate + ".");
        }
    }
}