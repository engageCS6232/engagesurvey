﻿using EvaluationSystem.Controller;
using EvaluationSystem.Model;
using EvaluationSystem.View;
using System;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace EvaluationSystem
{
    public partial class LoginForm : Form
    {
        private EvaluationControl evalControl = new EvaluationControl();

        public LoginForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Checks the user's login credentials against the information in the database.  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void loginButton_Click(object sender, EventArgs e)
        {
            String username, password;

            username = userBox.Text;
            password = passwordBox.Text;

            try
            {
                if (this.evalControl.ComparePassword(username, password))
                {
                    Employees newEmployee = this.evalControl.getEmployeeData(username);
                    UserForm userForm = new UserForm();
                    userForm.LoadEmployee(newEmployee);
                    userForm.Show();
                    //Hides the login form from view.
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("The username and/or password provided is invalid.  Please try again.",
                        "Invalid Login Credentials");
                    userBox.Text = "";
                    passwordBox.Text = "";
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        /// <summary>
        /// Closes the form if the user decides to "cancel" what (s)he is doing.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
