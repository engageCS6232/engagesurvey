﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EvaluationSystem.Model;

namespace EvaluationSystemTest.ModelTest
{
    [TestClass]
    public class AnswersTest
    {
        private Answers answers = new Answers();
        private int evaluationId = 1;
        private int answer = 1;
        private int questionId = 1;

        /// <summary>
        /// Test for newly created Answers, all gets show return null
        /// </summary>
        [TestMethod]
        public void TestNullAnswers()
        {
            //Return Answers Data
            Assert.AreEqual(0, this.answers.EvaluationId, 0, "Answer's EvaluationID not 0.");
            Assert.AreEqual(0, this.answers.Answer, 0, "Answer's Answer not 0.");
            Assert.AreEqual(0, this.answers.QuestionId, 0, "Answer's QuestionId not 0.");
        }

        /// <summary>
        /// Test for set and get of an Answers [Answers class]
        /// </summary>
        [TestMethod]
        public void TestActiveAnswers()
        {
            //Create Answers
            this.answers.EvaluationId = this.evaluationId;
            this.answers.Answer = this.answer;
            this.answers.QuestionId = this.questionId;

            //Return Answers Data
            Assert.AreEqual(this.evaluationId, this.answers.EvaluationId, 0, "Answer's EvaluationID not " + this.evaluationId + ".");
            Assert.AreEqual(this.answer, this.answers.Answer, 0, "Answer's Answer not " + this.answer + ".");
            Assert.AreEqual(this.questionId, this.answers.QuestionId, 0, "Answer's QuestionId not " + this.questionId + ".");
        }
    }
}