﻿using EvaluationSystem.Controller;
using EvaluationSystem.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace EvaluationSystem.View
{
    public partial class TypeOneEvaluation : Form
    {
        //private List<string> categories;
        //private List<string> questions;
        private List<Control> questionControlsList;
        private List<Categories> categoryList;
        private List<Questions> questionList;
        private EvaluationControl controller;
        private int evaluationIdInEvaluation;
        private Evaluation evaluation;

        public TypeOneEvaluation(int evaluationID)
        {
            InitializeComponent();

            this.controller = new EvaluationControl();
            //TODO: Replace 1 with the typeID passed in the TypeOneEvaluation constructor
            evaluationIdInEvaluation = evaluationID;
            try
            {
                this.evaluation = this.controller.GetSingleEvaluationByID(evaluationIdInEvaluation);
                if (this.evaluation == null)
                {
                    throw new Exception("Evaluation with id: " + evaluationID + " could not be found.");
                }
                txtType.Text = this.evaluation.TypeID.ToString();
                txtStage.Text = this.evaluation.StageID.ToString();
                Employees currentEmployee =
                    this.controller.getEmployeeData(this.controller.getEmployeeUserName(this.evaluation.ParticipantID));
                lblParticipant.Text = "Evaluation for: " + currentEmployee.EmployeeInfo;
                if (lblParticipant.Right > this.Width)
                    lblParticipant.Left = this.Width - lblParticipant.Width;
                categoryList = this.controller.getCategoriesByType(this.evaluation.TypeID);
                this.questionList = this.controller.getQuestionsByType(this.evaluation.TypeID);
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.GetType().ToString(), ex.Message.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString(), ex.Message.ToString());
            }
            CreateQuestions();
        }

        private void CreateQuestions()
        {
            questionControlsList = new List<Control>();
            flowLayoutPanel.Controls.Clear();
            flowLayoutPanel.SuspendLayout();

            foreach (Categories category in this.categoryList)
            {
                // Iterate through each category
                int catCounter = 1;
                Label lblCategory = new Label();
                lblCategory.Name = catCounter.ToString();
                lblCategory.Text = category.CategoryName;
                lblCategory.AutoSize = true;
                lblCategory.Font = new Font(lblCategory.Font, FontStyle.Bold);
                lblCategory.Margin = new System.Windows.Forms.Padding(10, 5, 0, 5);
                flowLayoutPanel.Controls.Add(lblCategory);
                int questionCounterID = 1;
                foreach (Questions question in this.questionList)
                {
                    if (question.CategoryId == category.CategoryNum)
                    {
                        // Iterate through each question
                        Label lblQuestion = new Label();
                        // TODO: Replace this with question ID

                        lblQuestion.Name = lblQuestion.ToString();
                        lblQuestion.Text = question.Question;
                        lblQuestion.Margin = new System.Windows.Forms.Padding(10, 1, 5, 1);
                        lblQuestion.AutoSize = true;
                        lblQuestion.MaximumSize = new Size(250, 0);
                        lblQuestion.MinimumSize = new Size(250, 0);

                        // Format question panel and add label for the question text
                        FlowLayoutPanel panel = new FlowLayoutPanel();
                        if (questionCounterID % 2 != 0)
                        {
                            panel.BackColor = Color.LightGray;
                        }
                        panel.Name = "pnl" + questionCounterID.ToString();
                        panel.Tag = question.QuestionId.ToString();
                        questionCounterID = questionCounterID + 1;
                        panel.AutoSize = true;
                        panel.FlowDirection = FlowDirection.LeftToRight;
                        questionControlsList.Add(panel);
                        int answerCounter = 1;
                        int numberOfQuestions = 1;
                        if (this.evaluation.TypeID == 1)
                        {
                            numberOfQuestions = 5;
                        }
                        else
                        {
                            numberOfQuestions = 10;
                        }
                        // Add question to question panel
                        panel.Controls.Add(lblQuestion);

                        // Add answers to question panel
                        while (answerCounter <= numberOfQuestions)
                        {
                            RadioButton rdoButton1 = new RadioButton();
                            rdoButton1.Name = "radio" + answerCounter.ToString() + " QUESTION ID GOES HERE";
                            rdoButton1.Text = answerCounter.ToString();
                            rdoButton1.Tag = answerCounter;
                            panel.Controls.Add(rdoButton1);
                            answerCounter++;
                        }

                        // Add question panel to the main panel
                        flowLayoutPanel.Controls.Add(panel);
                    }
                    else
                    {
                        continue;
                    }
                }
            }
            flowLayoutPanel.ResumeLayout();
        }

        private void submitButton_Click_1(object sender, EventArgs e)
        {
            List<Answers> answersList = new List<Answers>();

            List<String> answersListStringOnly = new List<String>();
            List<FlowLayoutPanel> noAnswerList = new List<FlowLayoutPanel>();
            try
            {
                foreach (FlowLayoutPanel questionPanel in this.questionControlsList)
                {
                    //Check if each radio button has an answer
                    var checkButton = questionPanel.Controls.OfType<RadioButton>().FirstOrDefault(r => r.Checked);
                    List<Label> questionLabel = questionPanel.Controls.OfType<Label>().ToList();
                    questionLabel[0].ForeColor = System.Drawing.Color.Black;
                    if (checkButton == null)
                    {
                        noAnswerList.Add(questionPanel);
                        continue;
                    }
                    // Collect answer in a list
                    //answersListStringOnly.Add(checkButton.Tag.ToString());
                    Answers answer = new Answers();
                    answer.QuestionId = Int32.Parse(questionPanel.Tag.ToString());
                    answer.Answer = Int32.Parse(checkButton.Tag.ToString());
                    answer.EvaluationId = this.evaluationIdInEvaluation;
                    answersList.Add(answer);
                }

                // Highlight unaswered questions in red
                if (noAnswerList.Count > 0)
                {
                    foreach (FlowLayoutPanel current in noAnswerList)
                    {
                        List<Label> questionLabel = current.Controls.OfType<Label>().ToList();
                        questionLabel[0].ForeColor = System.Drawing.Color.Red;
                    }
                    MessageBox.Show("All questions must be answered.");
                    return;
                }

                this.evaluation.EvaluationID = evaluationIdInEvaluation;
                this.controller.SaveAnswersAndUpdateEvaluationStatus(answersList, this.evaluation);
                //string answerString = "";
                //foreach (String answer in answersListStringOnly)
                //{

                //    answerString = answerString + answer;
                //}
                //MessageBox.Show(answerString);
                MessageBox.Show("Sucess!", "Your evaluation was successfully saved");
                this.Close();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
