﻿using EvaluationSystem.Controller;
using EvaluationSystem.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EvaluationSystem.View
{
    public partial class DisplayEvaluationsByCohort : Form
    {
        private EvaluationControl controller;

        public DisplayEvaluationsByCohort()
        {
            InitializeComponent();
            this.controller = new EvaluationControl();
            this.cohortNameComboBox.SelectedIndexChanged += cohortNameComboBox_SelectedIndexChanged;
        }

        void cohortNameComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.FillForm();
        }

        private void DisplayEvaluationsByCohort_Load(object sender, EventArgs e)
        {
            try
            {
                this.cohortTableAdapter.Fill(this._CS6232_g3DataSet.cohort);
                this.FillForm();
                
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        private void FillForm()
        {
            try
            {
                
                int cohortID = (int) this.cohortNameComboBox.SelectedValue;
                BindingList<Evaluation> evalList = this.controller.GetEvaluationsByCohortID(cohortID);

                foreach (Evaluation eval in evalList)
                {
                    Employees participant = controller.GetEmployeeByID(eval.ParticipantID);
                    Employees evaluator = controller.GetEmployeeByID(eval.EvaluatorID);
                    eval.ParticipantFirstName = participant.FName;
                    eval.ParticipantLastName = participant.LName;
                    eval.EvaluatorFirstName = evaluator.FName;
                    eval.EvaluatorLastName = evaluator.LName;
                }
                this.evaluationDataGridView.DataSource = evalList;

                
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        private void DisplayEvaluationsByCohort_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.cohortNameComboBox.Dispose();
        }

 
    }
}
