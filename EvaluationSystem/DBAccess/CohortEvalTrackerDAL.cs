﻿using System;
using System.Data.SqlClient;
using System.Transactions;

namespace EvaluationSystem.DBAccess
{
    public class CohortEvalTrackerDAL
    {
        /// <summary>
        /// Create an entry for a given cohort, type, and stage in the cohot_eval_tracker table
        /// </summary>
        /// <param name="cohortID"></param>
        /// <param name="stageID"></param>
        /// <param name="typeID"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public static bool CreateCohortEval(int cohortID, int stageID, int typeID, DateTime start, DateTime end)
        {
            String insertStatement = "INSERT INTO cohort_eval_tracker " +
                                     "VALUES (@cohortID, @stageID, @typeID, @start, @end)";
            Boolean successfulInsert = true;
            try
            {
                using (SqlConnection connection = EngageDBConnection.GetConnection())
                {
                    connection.Open();
                    using (SqlCommand insertCommand = new SqlCommand(insertStatement, connection))
                    {
                        insertCommand.Parameters.AddWithValue("@cohortID", cohortID);
                        insertCommand.Parameters.AddWithValue("@stageID", stageID);
                        insertCommand.Parameters.AddWithValue("@typeID", typeID);
                        insertCommand.Parameters.AddWithValue("@start", start);
                        insertCommand.Parameters.AddWithValue("@end", end);

                        int rowsAffected = insertCommand.ExecuteNonQuery();

                        // Returns false if no rows are returned
                        if (rowsAffected <= 0)
                        {
                            successfulInsert = false;
                        }
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {

                throw;
            }
            return successfulInsert;
        }

        /// <summary>
        /// Adds an entry in the cohort_eval_tracker and self evaluation entries for each member of a given cohort, stage, and type
        /// </summary>
        /// <param name="cohortID"></param>
        /// <param name="typeID"></param>
        /// <param name="stageID"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public static bool CreateCohortEvalAndSelfEval(int cohortID, int typeID, int stageID, DateTime start,
            DateTime end)
        {
            Boolean successfulCreateAndInsert = false;
            using (TransactionScope transaction = new TransactionScope())
            {
                //Create an entry in the cohort_eval_tracker table for a given cohort
                CreateCohortEval(cohortID, stageID, typeID, start, end);

                //Create self evaluations for every member for a given cohort
                successfulCreateAndInsert = EvaluationsDAL.CreateSelfEvaluations(cohortID, typeID, stageID);

                transaction.Complete();
            }
            return successfulCreateAndInsert;
        }

        /// <summary>
        /// Creates Evaluation for Cohort if it doesn't already exists and if the previous stage is completed
        /// </summary>
        /// <param name="cohortID"></param>
        /// <param name="typeID"></param>
        /// <param name="stageID"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public static bool CreateEvalForCohort(int cohortID, int typeID, int stageID, DateTime startDate,
            DateTime endDate)
        {
            bool insertSuccessful = false;
            String insertStatement = "INSERT INTO cohort_eval_tracker" +
                         " VALUES (@cohortID, @typeID, @stageID, @startDate, @endDate)";

            if (!CohortEvalTrackerDAL.DoesEvalForCohortAlreadyExists(cohortID, typeID, stageID))
            {
                if (stageID == 1)
                {

                    insertSuccessful = InsertEvaluation(cohortID, typeID, stageID, startDate, endDate, insertStatement,
                        insertSuccessful);

                }
                else
                {
                    if (CohortEvalTrackerDAL.PreviousStageCompleted(cohortID, typeID, stageID, startDate))
                    {
                        insertSuccessful = InsertEvaluation(cohortID, typeID, stageID, startDate, endDate,
                            insertStatement, insertSuccessful);

                    }
                    else
                    {
                        throw new Exception("Previous stage has not yet completed");
                    }
                }
            }
            else
            {
                throw new Exception("Evaluation already exists");
            }
            return insertSuccessful;
        }

        /// <summary>
        /// Checks if evaluation already exists
        /// </summary>
        /// <param name="cohortID"></param>
        /// <param name="typeID"></param>
        /// <param name="stageID"></param>
        /// <returns></returns>
        public static bool DoesEvalForCohortAlreadyExists(int cohortID, int typeID, int stageID)
        {
            String selectStatement = "SELECT cohortID, typeID, stageID " +
                            " FROM cohort_eval_tracker " +
                            " WHERE cohortID = @cohortID AND" +
                            " typeID = @typeID AND" +
                            " stageID = @stageID";

            Boolean doesEvalExist = false;

            try
            {
                using (SqlConnection connection = EngageDBConnection.GetConnection())
                {
                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        selectCommand.Parameters.AddWithValue("@cohortID", cohortID);
                        selectCommand.Parameters.AddWithValue("@typeID", typeID);
                        selectCommand.Parameters.AddWithValue("@stageID", stageID);
                        connection.Open();
                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                doesEvalExist = true;
                            }
                        }
                    }
                }
            }
            catch (SqlException)
            {

                throw;
            }
            catch (Exception)
            {
                throw;
            }
            return doesEvalExist;
        }

        /// <summary>
        /// Checks whether the previous stage was completed
        /// </summary>
        /// <param name="cohortID"></param>
        /// <param name="typeID"></param>
        /// <param name="stageID"></param>
        /// <param name="startDate"></param>
        /// <returns></returns>
        public static bool PreviousStageCompleted(int cohortID, int typeID, int stageID, DateTime startDate)
        {
            bool previousStageCompleted = true;
            String selectStatement = "SELECT TOP 1 endDate" +
                                     " FROM cohort_eval_tracker" +
                                     " WHERE typeID = @typeID AND stageID < @stageID AND cohortID = @cohortID AND @startDate >" +
                                     "      (SELECT TOP 1 endDate" +
                                     "      FROM cohort_eval_tracker" +
                                     "      WHERE typeID = @typeID AND" +
                                     "      cohortID = @cohortID AND" +
                                     "      stageID = @stageID - 1" +
                                     "      ORDER BY stageID DESC)";

            try
            {
                using (SqlConnection connection = EngageDBConnection.GetConnection())
                {
                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        selectCommand.Parameters.AddWithValue("@cohortID", cohortID);
                        selectCommand.Parameters.AddWithValue("@typeID", typeID);
                        selectCommand.Parameters.AddWithValue("@stageID", stageID);
                        selectCommand.Parameters.AddWithValue("@startDate", startDate);
                        connection.Open();
                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            if (!reader.HasRows)
                            {
                                previousStageCompleted = false;
                            }
                        }
                    }
                }
            }
            catch (SqlException)
            {

                throw;
            }
            catch (Exception)
            {
                throw;
            }
            return previousStageCompleted;
        }

        /// <summary>
        /// Insert evaluation into cohort_eval_tracker table
        /// </summary>
        /// <param name="cohortID"></param>
        /// <param name="typeID"></param>
        /// <param name="stageID"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="insertStatement"></param>
        /// <param name="insertSuccessful"></param>
        /// <returns></returns>
        private static bool InsertEvaluation(int cohortID, int typeID, int stageID, DateTime startDate, DateTime endDate,
            string insertStatement, bool insertSuccessful)
        {
            using (SqlConnection connection = EngageDBConnection.GetConnection())
            {
                using (SqlCommand insertCommand = new SqlCommand(insertStatement, connection))
                {
                    SqlTransaction transaction = null;

                    try
                    {
                        connection.Open();

                        // Start Transaction
                        transaction = connection.BeginTransaction();
                        insertCommand.Transaction = transaction;

                        insertCommand.Parameters.AddWithValue("@cohortID", cohortID);
                        insertCommand.Parameters.AddWithValue("@typeID", typeID);
                        insertCommand.Parameters.AddWithValue("@stageID", stageID);
                        insertCommand.Parameters.AddWithValue("startDate", startDate);
                        insertCommand.Parameters.AddWithValue("@endDate", endDate);


                        int affectedRows = insertCommand.ExecuteNonQuery();
                        if (affectedRows <= 0)
                        {
                            insertSuccessful = false;
                        }
                        else
                        {
                            insertSuccessful = true;
                        }
                        transaction.Commit();
                    }
                    catch (SqlException)
                    {
                        if (transaction != null)
                        {
                            transaction.Rollback();
                        }
                        throw;
                    }
                    catch (Exception)
                    {
                        if (transaction != null)
                        {
                            transaction.Rollback();
                        }
                        throw;
                    }
                }
            }
            return insertSuccessful;
        }


    }
}
