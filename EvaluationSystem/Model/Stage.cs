﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluationSystem.Model
{
    public class Stage
    {
        public int StageID { get; set; }
        public string StageName { get; set; }
    }
}
