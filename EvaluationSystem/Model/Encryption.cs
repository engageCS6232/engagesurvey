﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace EvaluationSystem.Model
{
    public class Encryption
    {
        /// <summary>
        /// Gets the hash of the input.
        /// </summary>
        /// <param name="input"></param>
        /// <returns>A string representation of the hash.</returns>
        public String GetSAW1Hash(String input)
        {
            SHA1CryptoServiceProvider x = new SHA1CryptoServiceProvider();
            byte[] bs = Encoding.UTF8.GetBytes(input);
            bs = x.ComputeHash(bs);
            StringBuilder s = new StringBuilder();
            foreach (byte b in bs)
            {
                s.Append(b.ToString("x2").ToLower());
            }
            String password = s.ToString();
            return "0x" + password;
        }
    }
}
