﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EvaluationSystem.Model;

namespace EvaluationSystemTest
{
    [TestClass]
    public class EmployeesTest
    {
        private Employees employee = new Employees();
        private int employeeID = 1;
        private String fName = "TestFirstName";
        private String lName = "TestLastName";
        private String email = "testemail@test.com";
        private String username = "test1";
        private Boolean administrator = true;
        private int? cohortID = 1;
        private String employeeInfo;

        /// <summary>
        /// Test for newly created Employees, all gets show return null
        /// </summary>
        [TestMethod]
        public void TestNullEmployees()
        {
            //Create Employees
            this.employeeInfo = ", (0)";
            
            //Return Employees Data
            Assert.AreEqual(0, this.employee.EmployeeID, 0, "Employees's EmployeeID not 0.");
            Assert.IsNull(this.employee.FName, "Employees's FName not null.");
            Assert.IsNull(this.employee.LName, "Employees's LName not null.");
            Assert.IsNull(this.employee.Email, "Employees's Email not null.");
            Assert.IsNull(this.employee.Username, "Employees's Username not null.");
            Assert.IsFalse(this.employee.Administrator, "Employees's Administrator not false.");
            Assert.IsNull(this.employee.CohortID, "Employees's CohortID not null.");
            Assert.AreEqual(this.employeeInfo, this.employee.EmployeeInfo, "Employees's EmployeeInfo not correct.");
        }

        /// <summary>
        /// Test for set and get of an Employees [Employees class]
        /// </summary>
        [TestMethod]
        public void TestActiveEmployees()
        {
            //Create Employee
            this.employee.EmployeeID = this.employeeID;
            this.employee.FName = this.fName;
            this.employee.LName = this.lName;
            this.employee.Email = this.email;
            this.employee.Username = this.username;
            this.employee.Administrator = this.administrator;
            this.employee.CohortID = this.cohortID;
            this.employeeInfo = this.lName + ", " + this.fName + "(" + this.employeeID + ")";
            
            //Return Employee Data
            Assert.AreEqual(this.employeeID, this.employee.EmployeeID, 0, "Employees's EmployeeID not " + this.employeeID + ".");
            Assert.AreEqual(this.fName, this.employee.FName, "Employees's FName not " + this.fName + ".");
            Assert.AreEqual(this.lName, this.employee.LName, "Employees's LName not " + this.lName + ".");
            Assert.AreEqual(this.email, this.employee.Email, "Employees's Email not " + this.email + ".");
            Assert.AreEqual(this.username, this.employee.Username, "Employees's Username not " + this.username + ".");
            Assert.AreEqual(this.administrator, this.employee.Administrator, "Employees's Administrator not " + this.administrator + ".");
            Assert.AreEqual(this.cohortID, this.employee.CohortID, "Employees's CohortID not " + this.cohortID + ".");
            Assert.AreEqual(this.employeeInfo, this.employee.EmployeeInfo, "Employee's EmployeeInfo not " + this.employeeInfo + ".");
        }
    }
}