﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EvaluationSystem.Model;

namespace EvaluationSystemTest.ModelTest
{
    [TestClass]
    public class EvaluationTest
    {
        //Test Evaluation
        private Evaluation evaluation = new Evaluation();
        private int evaluationID = 1;
        private int stageID = 1;
        private int roleID = 1;
        private DateTime? completionDate = DateTime.Now.Date;
        private DateTime? dueDate = DateTime.Now.AddDays(2).Date;
        private int typeID = 1;
        private int participantID = 1;
        private int evaluatorID = 1;
        private string stageName = "Test Stage";
        private string roleName = "Test Role";
        private string typeName = "Test Type";
        private string evaluator = "Test Evaluator";
        private string participantFirstName = "TestFirstName";
        private string participantLastName = "TestLastName";
        private string evaluatorFirstName = "TestFirstName";
        private string evaluatorLastName = "TestLastName";
        private string participantInfo;
        private string evaluatorInfo;

        /// <summary>
        /// Test for newly created Evaluation, all gets show return null
        /// </summary>
        [TestMethod]
        public void TestNullEvaluation()
        {
            //Create Evaluation
            this.participantInfo = ", (0)";
            this.evaluatorInfo = ", (0)";

            //Return Evaluation Data
            Assert.AreEqual(0, this.evaluation.EvaluationID, 0, "Evaluation's EvaluationID not 0.");
            Assert.AreEqual(0, this.evaluation.StageID, 0, "Evaluation's StageID not 0.");
            Assert.AreEqual(0, this.evaluation.RoleID, 0, "Evaluation's RoleID not 0.");
            Assert.IsNull(this.evaluation.CompletionDate, "Evaluation's CompletionDate not null.");
            Assert.IsNull(this.evaluation.DueDate, "Evaluation's DueDate not null.");
            Assert.AreEqual(0, this.evaluation.TypeID, 0, "Evaluation's TypeID not 0.");
            Assert.AreEqual(0, this.evaluation.ParticipantID, 0, "Evaluation's ParticipantID not 0.");
            Assert.AreEqual(0, this.evaluation.EvaluatorID, 0, "Evaluation's EvaluatorID not 0.");
            Assert.IsNull(this.evaluation.StageName, "Evaluation's StageName not null.");
            Assert.IsNull(this.evaluation.RoleName, "Evaluation's RoleName not null.");
            Assert.IsNull(this.evaluation.TypeName, "Evaluation's TypeName not null.");
            Assert.IsNull(this.evaluation.Evaluator, "Evaluation's Evaluator not null.");
            Assert.IsNull(this.evaluation.ParticipantFirstName, "Evaluation's ParticipantFirstName not null.");
            Assert.IsNull(this.evaluation.ParticipantLastName, "Evaluation's ParticipantLastName not null.");
            Assert.IsNull(this.evaluation.EvaluatorFirstName, "Evaluation's EvaluatorFirstName not null.");
            Assert.IsNull(this.evaluation.EvaluatorLastName, "Evaluation's EvaluatorLastName not null.");
            Assert.AreEqual(this.participantInfo, this.evaluation.ParticipantInfo, "Evaluation's ParticipantInfo not " + this.participantInfo + ".");
            Assert.AreEqual(this.evaluatorInfo, this.evaluation.EvaluatorInfo, "Evaluation's EvaluatorInfo not " + this.evaluatorInfo + ".");
        }

        /// <summary>
        /// Test for set and get of an evaluation [Evaluations class]
        /// </summary>
        [TestMethod]
        public void TestActiveEvaluation()
        {
            //Create Evaluation
            this.evaluation.EvaluationID = this.evaluationID;
            this.evaluation.StageID = this.stageID;
            this.evaluation.RoleID = this.roleID;
            this.evaluation.CompletionDate = this.completionDate;
            this.evaluation.DueDate = this.dueDate;
            this.evaluation.TypeID = this.typeID;
            this.evaluation.ParticipantID = this.participantID;
            this.evaluation.EvaluatorID = this.evaluatorID;
            this.evaluation.StageName = this.stageName;
            this.evaluation.RoleName = this.roleName;
            this.evaluation.TypeName = this.typeName;
            this.evaluation.Evaluator = this.evaluator;
            this.evaluation.ParticipantFirstName = this.participantFirstName;
            this.evaluation.ParticipantLastName = this.participantLastName;
            this.evaluation.EvaluatorFirstName = this.evaluatorFirstName;
            this.evaluation.EvaluatorLastName = this.evaluatorLastName;
            this.participantInfo = this.participantFirstName + ", " + this.participantLastName + "(" + this.participantID + ")";
            this.evaluatorInfo = this.evaluatorFirstName + ", " + this.evaluatorLastName + "(" + this.evaluatorID + ")";

            //Return Evaluation Data
            Assert.AreEqual(this.evaluationID, this.evaluation.EvaluationID, 0, "Evaluation's EvaluationID not " + this.evaluationID + ".");
            Assert.AreEqual(this.stageID, this.evaluation.StageID, 0, "Evaluation's StageID not " + this.stageID + ".");
            Assert.AreEqual(this.roleID, this.evaluation.RoleID, 0, "Evaluation's RoleID not " + this.roleID + ".");
            Assert.AreEqual(this.completionDate, this.evaluation.CompletionDate, "Evaluation's CompletionDate not " + this.completionDate + ".");
            Assert.AreEqual(this.dueDate, this.evaluation.DueDate, "Evaluation's DueDate not " + this.dueDate + ".");
            Assert.AreEqual(this.typeID, this.evaluation.TypeID, 0, "Evaluation's TypeID not " + this.typeID + ".");
            Assert.AreEqual(this.participantID, this.evaluation.ParticipantID, 0, "Evaluation's ParticipantID not " + this.participantID + ".");
            Assert.AreEqual(this.evaluatorID, this.evaluation.EvaluatorID, 0, "Evaluation's EvaluatorID not " + this.evaluatorID + ".");
            Assert.AreEqual(this.stageName, this.evaluation.StageName, "Evaluation's StageName not not " + this.stageName + ".");
            Assert.AreEqual(this.roleName, this.evaluation.RoleName, "Evaluation's RoleName not not " + this.roleName + ".");
            Assert.AreEqual(this.typeName, this.evaluation.TypeName, "Evaluation's TypeName not not " + this.typeName + ".");
            Assert.AreEqual(this.evaluator, this.evaluation.Evaluator, "Evaluation's Evaluator not not " + this.evaluator + ".");
            Assert.AreEqual(this.participantFirstName, this.evaluation.ParticipantFirstName, "Evaluation's ParticipantFirstName not " + this.participantFirstName + ".");
            Assert.AreEqual(this.participantLastName, this.evaluation.ParticipantLastName, "Evaluation's ParticipantLastName not " + this.participantLastName + ".");
            Assert.AreEqual(this.evaluatorFirstName, this.evaluation.EvaluatorFirstName, "Evaluation's EvaluatorFirstName not " + this.evaluatorFirstName + ".");
            Assert.AreEqual(this.evaluatorLastName, this.evaluation.EvaluatorLastName, "Evaluation's EvaluatorLastName not " + this.evaluatorLastName + ".");
            Assert.AreEqual(this.participantInfo, this.evaluation.ParticipantInfo, "Evaluation's ParticipantInfo not " + this.participantInfo + ".");
            Assert.AreEqual(this.evaluatorInfo, this.evaluation.EvaluatorInfo, "Evaluation's EvaluatorInfo not " + this.evaluatorInfo + ".");
        }
    }
}