﻿using EvaluationSystem.Controller;
using EvaluationSystem.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EvaluationSystem.View
{
    public partial class EmployeeSelfEvaluationForm : Form
    {
        private EvaluationControl controller;
        private Form parentForm;
        private Employees theEmployee;

        public EmployeeSelfEvaluationForm(Employees employee, Form parent)
        {
            InitializeComponent();
            this.theEmployee = employee;
            this.parentForm = parent;
            this.controller = new EvaluationControl();
        }



        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void EmployeeSelfEvaluationForm_Load(object sender, EventArgs e)
        {
            try
            {
                List<Evaluation> evalList = this.controller.getSelfEvaluationsByEmployee(this.theEmployee.EmployeeID);
                foreach (Evaluation eval in evalList)
                {
                    eval.EvaluatorFirstName = this.theEmployee.FName;
                    eval.EvaluatorLastName = this.theEmployee.LName;
                    eval.EvaluatorID = this.theEmployee.EmployeeID;
                }

                this.evaluationDataGridView.DataSource = evalList;
                DataGridViewRowCollection rows = this.evaluationDataGridView.Rows;
                foreach (DataGridViewRow row in rows)
                {
                    DataGridViewCellCollection cells = row.Cells;
                    if (cells["roleName"].Value.ToString() == "Self")
                    {
                        cells["roleName"].Value = "Self Evaluation";
                    }
                    else if (cells["roleName"].Value.ToString() == "Supervisor")
                    {
                        cells["roleName"].Value = "Assigned to Supervisor";
                    }
                    else
                    {
                        cells["roleName"].Value = "Assigned to Co-Worker";
                    }                   
                }
               
                
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());

            }
        }

        void evaluationDataGridView_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
           DataGridViewRowCollection rows = this.evaluationDataGridView.Rows;
            foreach (DataGridViewRow row in rows)
            {
                bool HasSelectedEvaluators = true;
                DataGridViewCellCollection cells = row.Cells;
                if (cells["EvaluationID"].Value != null)
                {
                    HasSelectedEvaluators = this.controller.HasAllEvauatorsSelected((int)cells["EvaluationID"].Value);
                }
                Console.WriteLine(HasSelectedEvaluators);
                if (HasSelectedEvaluators)
                {
                    var BtnCell = (DataGridViewButtonCell)cells["Take"];
                    BtnCell.Value = "Take Evaluation";
                }
                else
                {
                    var BtnCell = (DataGridViewButtonCell)cells["Take"];
                    BtnCell.Value = "Select Evaluators";
                }

            }
        }

        private void evaluationDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 6)
            {
                try
                {
                    int rowID = e.RowIndex;
                    DataGridViewRow row = evaluationDataGridView.Rows[rowID];
                    DataGridViewCell cell = row.Cells["EvaluationID"];
                    int evaluationID = (int)cell.Value;
                    DataGridViewCell Takecell = row.Cells["Take"];
                    if (Takecell.Value.Equals("Take Evaluation"))
                    {
                        TypeOneEvaluation frmTypeOneEval = new TypeOneEvaluation(evaluationID);
                        frmTypeOneEval.MdiParent = parentForm;
                        frmTypeOneEval.Show();
                        frmTypeOneEval.FormClosed += frmTypeOneEval_FormClosed;
                    }
                    else
                    {
                        EvaluatorForm frmEvaluator = new EvaluatorForm();
                        frmEvaluator.MdiParent = parentForm;
                        frmEvaluator.LoadEmployee(this.theEmployee);
                        frmEvaluator.Show();
                        frmEvaluator.FormClosed += frmTypeOneEval_FormClosed;
                    }
                    

                    this.Hide();
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Message, ex.GetType().ToString());
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, ex.GetType().ToString());
                }
            }
        }

        void frmTypeOneEval_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Show();
            this.EmployeeSelfEvaluationForm_Load(sender, e);
        }
    }
}
