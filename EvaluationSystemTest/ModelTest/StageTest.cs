﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EvaluationSystem.Model;

namespace EvaluationSystemTest.ModelTest
{
    [TestClass]
    public class StageTest
    {
        private Stage stage = new Stage();
        private int stageID = 1;
        private string stageName = "Test Stage";

        /// <summary>
        /// Test for newly created Stage, all gets show return null
        /// </summary>
        [TestMethod]
        public void TestNullStage()
        {
            //Return stage Data
            Assert.AreEqual(0, this.stage.StageID, 0, "Stage's StageID not 0.");
            Assert.IsNull(this.stage.StageName, "Stage's StageName not null.");
        }

        /// <summary>
        /// Test for set and get of an Stage [Stage class]
        /// </summary>
        [TestMethod]
        public void TestActivestage()
        {
            //Create stage
            this.stage.StageID = this.stageID;
            this.stage.StageName = this.stageName;

            //Return stage Data
            Assert.AreEqual(this.stageID, this.stage.StageID, 0, "Stage's StageID not " + this.stageID + ".");
            Assert.AreEqual(this.stageName, this.stage.StageName, "Stage's StageName not " + this.stageName + ".");
        }
    }
}