﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EvaluationSystem.DBAccess;
using System.Collections.Generic;
using EvaluationSystem.Model;

namespace EvaluationSystemTest.DBAccessTest
{
    [TestClass]
    public class TypesDALTest
    {
        /// <summary>
        /// Test return a Types List
        /// </summary>
        [TestMethod]
        public void TestGetTypesList()
        {
            //Correct Results
            int typesCount = 2;
            int indexTypes = 0;
            int typeID = 1;
            string typeName = "Type1";

            //Get Types List
            List<Types> typesList = TypesDAL.GetTypesList();
            Types types = typesList[indexTypes];

            //Check Types Results
            Assert.AreEqual(typesCount, typesList.Count, 0, "Types count not " + typesCount + ".");
            Assert.AreEqual(typeID, types.TypeID, 0, "Types's TypeID not " + typeID + ".");
            Assert.AreEqual(typeName, types.TypeName, "Types's TypeName not " + typeName + ".");
        }
    }
}