﻿using EvaluationSystem.Controller;
using EvaluationSystem.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;

namespace EvaluationSystem.View
{
    
    public partial class CohortsCreateForm : Form
    {
        private EvaluationControl evalControl;
        private BindingList<Employees> cohortMemberList;
        private List<Employees> employeeList;
        private int cohortIdInserted;

        public CohortsCreateForm()
        {
            InitializeComponent();

            this.evalControl = new EvaluationControl();
            this.cohortNameBox.TextChanged += cohortNameBox_TextChanged;
            
            this.employeeButton.Enabled = false;
            
        }

        /// <summary>
        /// Closes the create cohort form without performing any other action. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CohortsCreateForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the '_CS6232_g3DataSet.selected' table. You can move, or remove it, as needed.
            employeeList = new List<Employees>();
            try
            {
                employeeList = this.evalControl.getEmployeesWithoutCohorts();

                employeeListBox.DataSource = employeeList;

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }

            cohortMemberList = new BindingList<Employees>();
            this.cohortMemberList.ListChanged += cohortMemberList_ListChanged;
        }

        /// <summary>
        /// Adds employee to the cohort box and removes it from the employee box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cohortButton_Click(object sender, EventArgs e)
        {
            if (employeeList.Count() > 0)
            {
                cohortMemberList.Add(employeeList[employeeListBox.SelectedIndex]);
                employeeList.Remove(employeeList[employeeListBox.SelectedIndex]);
                this.updateEmployeeListBoxBinding(sender);
            }
            else
            {
                this.cohortButton.Enabled = false;
            }
        }


        /// <summary>
        /// Adds an employee to the employee box and removes it from the cohort box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void employeeButton_Click(object sender, EventArgs e)
        {
            if (cohortMemberList.Count() > 0)
            {
                employeeList.Add(cohortMemberList[cohortBox.SelectedIndex]);
                cohortMemberList.Remove(cohortMemberList[cohortBox.SelectedIndex]);
                this.updateEmployeeListBoxBinding(sender);
            }
        }

        /// <summary>
        /// Updates the binding on the EmployeeListBox so display will update when
        /// items are removed or added to the list. 
        /// </summary>
        private void updateEmployeeListBoxBinding(Object sender)
        {
            employeeListBox.DataSource = null;
            employeeListBox.Items.Clear();
            employeeListBox.DataSource = employeeList;
            employeeListBox.DisplayMember = "employeeInfo";
            cohortBox.DataSource = cohortMemberList;
            Button clickSender = (Button)sender;
            
            if (clickSender.Name == "cohortButton" && employeeListBox.Items.Count != 0)
            {
                clickSender.Enabled = true;
                employeeButton.Enabled = true;
                employeeListBox.SelectedIndex = 0;
            }
            else if (clickSender.Name == "employeeButton" && cohortBox.Items.Count != 0)
            {
                clickSender.Enabled = true;
                cohortButton.Enabled = true;
                cohortBox.SelectedIndex = 0;
            }
            else
            {
                clickSender.Enabled = false;
            }
        }

        /// <summary>
        /// Checks if there if the cohortNameBox is empty. If it has text then it will create add the cohort
        /// to the cohort list and return the id of that cohort.
        /// It will also assign that id to each of the Employees in the cohortMemberList then add them to the Selected
        /// table
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void submitButton_Click(object sender, EventArgs e)
        {
            if (Validator.IsPresent(cohortNameBox))
            {
                String cohortName = cohortNameBox.Text.ToString();
                try
                {
                    cohortIdInserted = evalControl.CreateCohort(cohortName, cohortMemberList);
                    
                    if (cohortIdInserted == 0)
                    {
                        throw new System.Exception("An employee of " + cohortName + " has recently been modified since this form was loaded");
                    }

                    foreach (Employees current in cohortMemberList)
                    {
                        current.CohortID = cohortIdInserted;
                    }
                    
                    bool isSuccesful = evalControl.UpdateCohort(cohortIdInserted, cohortMemberList);
                    if (isSuccesful)
                    {
                        this.Close();
                        MessageBox.Show("Cohort Successfully Created", "Cohort Created");
                    }
                    else
                    {
                        MessageBox.Show("Unable To Create Cohort", "Create Error");
                    }
                    
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Message.ToString());
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message.ToString());
                    this.Close();
                }
            }
        }

        // *************** Private listener methods *************** //

        /// <summary>
        /// Enables the submit button only if there is a cohort name and 
        /// at least one employee in the cohort list. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cohortNameBox_TextChanged(object sender, EventArgs e)
        {
            if (this.cohortNameBox.Text == "" || this.cohortMemberList.Count() == 0)
            {
                this.submitButton.Enabled = false;
            }
            else
            {
                this.submitButton.Enabled = true;
            }
        }

        /// <summary>
        /// enables the submit button when the Cohort List box is not empty, and disables 
        /// it otherwise. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cohortMemberList_ListChanged(object sender, ListChangedEventArgs e)
        {
            if (this.cohortMemberList.Count < 1 || this.cohortNameBox.Text == "")
            {
                this.submitButton.Enabled = false;
            }
            else
            {
                this.submitButton.Enabled = true;
            }
        }
    }
}
