﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EvaluationSystem.Model;

namespace EvaluationSystemTest.ModelTest
{
    [TestClass]
    public class QuestionsTest
    {
        private Questions questions = new Questions();
        private int questionId = 1;
        private string question = "Test Question";
        private string description = "Test Description";
        private int typeId = 1;
        private int categoryId = 1;

        /// <summary>
        /// Test for newly created Questions, all gets show return null
        /// </summary>
        [TestMethod]
        public void TestNullQuestions()
        {
            //Return Questions Data
            Assert.AreEqual(0, this.questions.QuestionId, 0, "Questions's QuestionId not 0.");
            Assert.IsNull(this.questions.Question, "Questions's Question not null.");
            Assert.IsNull(this.questions.Description, "Questions's Description not null.");
            Assert.AreEqual(0, this.questions.TypeId, 0, "Questions's TypeId not 0.");
            Assert.AreEqual(0, this.questions.CategoryId, 0, "Questions's CategoryId not 0.");
        }

        /// <summary>
        /// Test for set and get of an Questions [Questions class]
        /// </summary>
        [TestMethod]
        public void TestActiveQuestions()
        {
            //Create Questions
            this.questions.QuestionId = this.questionId;
            this.questions.Question = this.question;
            this.questions.Description = this.description;
            this.questions.TypeId = this.typeId;
            this.questions.CategoryId = this.categoryId;

            //Return Questions Data
            Assert.AreEqual(this.questionId, this.questions.QuestionId, 0, "Questions's QuestionId not " + this.questionId + ".");
            Assert.AreEqual(this.question, this.questions.Question, "Questions's Question not " + this.question + ".");
            Assert.AreEqual(this.description, this.questions.Description, "Questions's Description not " + this.description + ".");
            Assert.AreEqual(this.typeId, this.questions.TypeId, 0, "Questions's TypeId not " + this.typeId + ".");
            Assert.AreEqual(this.categoryId, this.questions.CategoryId, 0, "Questions's CategoryId not " + this.categoryId + ".");
        }
    }
}