﻿using EvaluationSystem.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluationSystem.DBAccess
{
    public static class TypesDAL
    {
        public static List<Types> GetTypesList()
        {
            List<Types> typesList = new List<Types>();

            string selectStatement = "SELECT typeID, typeName FROM types";
            try 
            {
                using (SqlConnection connection = EngageDBConnection.GetConnection())
                {
                    connection.Open();
                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Types type = new Types();
                                type.TypeID = (int) reader["typeID"];
                                type.TypeName = reader["typeName"].ToString();
                                typesList.Add(type);
                            }
                        }
                    }
                }
            } 
            catch (SqlException) 
            {
                throw;
            }
            catch (Exception) 
            {
                throw;
            }

            return typesList;    
                
        }
    }
}
