﻿namespace EvaluationSystem.View
{
    partial class GraphReportSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.typeLabel = new System.Windows.Forms.Label();
            this.typeBox = new System.Windows.Forms.ComboBox();
            this.typesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.reports = new EvaluationSystem.Reports();
            this.cohortLabel = new System.Windows.Forms.Label();
            this.cohortBox = new System.Windows.Forms.ComboBox();
            this.cohortBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cancelButton = new System.Windows.Forms.Button();
            this.submitButton = new System.Windows.Forms.Button();
            this.cohortTableAdapter = new EvaluationSystem.ReportsTableAdapters.cohortTableAdapter();
            this.typesTableAdapter = new EvaluationSystem.ReportsTableAdapters.typesTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.typesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reports)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cohortBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // typeLabel
            // 
            this.typeLabel.AutoSize = true;
            this.typeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.typeLabel.Location = new System.Drawing.Point(51, 106);
            this.typeLabel.Name = "typeLabel";
            this.typeLabel.Size = new System.Drawing.Size(57, 25);
            this.typeLabel.TabIndex = 40;
            this.typeLabel.Text = "Type";
            // 
            // typeBox
            // 
            this.typeBox.DataSource = this.typesBindingSource;
            this.typeBox.DisplayMember = "typeName";
            this.typeBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.typeBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.typeBox.FormattingEnabled = true;
            this.typeBox.Location = new System.Drawing.Point(163, 102);
            this.typeBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.typeBox.Name = "typeBox";
            this.typeBox.Size = new System.Drawing.Size(257, 33);
            this.typeBox.TabIndex = 39;
            this.typeBox.ValueMember = "typeID";
            // 
            // typesBindingSource
            // 
            this.typesBindingSource.DataMember = "types";
            this.typesBindingSource.DataSource = this.reports;
            // 
            // reports
            // 
            this.reports.DataSetName = "Reports";
            this.reports.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cohortLabel
            // 
            this.cohortLabel.AutoSize = true;
            this.cohortLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cohortLabel.Location = new System.Drawing.Point(50, 44);
            this.cohortLabel.Name = "cohortLabel";
            this.cohortLabel.Size = new System.Drawing.Size(71, 25);
            this.cohortLabel.TabIndex = 38;
            this.cohortLabel.Text = "Cohort";
            // 
            // cohortBox
            // 
            this.cohortBox.DataSource = this.cohortBindingSource;
            this.cohortBox.DisplayMember = "cohortName";
            this.cohortBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cohortBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cohortBox.FormattingEnabled = true;
            this.cohortBox.Location = new System.Drawing.Point(162, 40);
            this.cohortBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cohortBox.Name = "cohortBox";
            this.cohortBox.Size = new System.Drawing.Size(257, 33);
            this.cohortBox.TabIndex = 37;
            this.cohortBox.ValueMember = "cohortID";
            // 
            // cohortBindingSource
            // 
            this.cohortBindingSource.DataMember = "cohort";
            this.cohortBindingSource.DataSource = this.reports;
            // 
            // cancelButton
            // 
            this.cancelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelButton.Location = new System.Drawing.Point(312, 169);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(107, 38);
            this.cancelButton.TabIndex = 36;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // submitButton
            // 
            this.submitButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.submitButton.Location = new System.Drawing.Point(163, 169);
            this.submitButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.submitButton.Name = "submitButton";
            this.submitButton.Size = new System.Drawing.Size(107, 38);
            this.submitButton.TabIndex = 35;
            this.submitButton.Text = "Submit";
            this.submitButton.UseVisualStyleBackColor = true;
            this.submitButton.Click += new System.EventHandler(this.submitButton_Click);
            // 
            // cohortTableAdapter
            // 
            this.cohortTableAdapter.ClearBeforeFill = true;
            // 
            // typesTableAdapter
            // 
            this.typesTableAdapter.ClearBeforeFill = true;
            // 
            // GraphReportSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(477, 252);
            this.Controls.Add(this.typeLabel);
            this.Controls.Add(this.typeBox);
            this.Controls.Add(this.cohortLabel);
            this.Controls.Add(this.cohortBox);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.submitButton);
            this.Name = "GraphReportSettings";
            this.Text = "Cohort Performance Report";
            this.Load += new System.EventHandler(this.GraphReportSettings_Load);
            ((System.ComponentModel.ISupportInitialize)(this.typesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reports)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cohortBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label typeLabel;
        private System.Windows.Forms.ComboBox typeBox;
        private System.Windows.Forms.Label cohortLabel;
        private System.Windows.Forms.ComboBox cohortBox;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button submitButton;
        private Reports reports;
        private System.Windows.Forms.BindingSource cohortBindingSource;
        private ReportsTableAdapters.cohortTableAdapter cohortTableAdapter;
        private System.Windows.Forms.BindingSource typesBindingSource;
        private ReportsTableAdapters.typesTableAdapter typesTableAdapter;
    }
}