﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EvaluationSystem.DBAccess;
using System.Collections.Generic;
using EvaluationSystem.Model;

namespace EvaluationSystemTest.DBAccessTest
{
    [TestClass]
    public class StageDALTest
    {
        /// <summary>
        /// Test return a Stage List
        /// </summary>
        [TestMethod]
        public void TestGetStageList()
        {
            //Correct Results
            int stageCount = 5;
            int indexStage = 0;
            int stageID = 1;
            string stageName = "Stage1";

            //Get Stage List
            List<Stage> stageList = StageDAL.GetStageList();
            Stage stage = stageList[indexStage];

            //Check Stage Results
            Assert.AreEqual(stageCount, stageList.Count, 0, "Stage count not " + stageCount + ".");
            Assert.AreEqual(stageID, stage.StageID, 0, "Stage's StageID not " + stageID + ".");
            Assert.AreEqual(stageName, stage.StageName, "Stage's StageName not " + stageName + ".");
        }
    }
}