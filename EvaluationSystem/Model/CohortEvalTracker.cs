﻿using System;

namespace EvaluationSystem.Model
{
    public class CohortEvalTracker
    {

        public CohortEvalTracker()
        {

        }

        public int cohortID { get; set; }
        public int typeID { get; set; }
        public int stageID { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
    }
}
