﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EvaluationSystem.View
{
    public partial class GraphReportSettings : Form
    {
        public GraphReportSettings()
        {
            InitializeComponent();
        }

        private void GraphReportSettings_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'reports.types' table. You can move, or remove it, as needed.
            this.typesTableAdapter.Fill(this.reports.types);
            // TODO: This line of code loads data into the 'reports.cohort' table. You can move, or remove it, as needed.
            this.cohortTableAdapter.Fill(this.reports.cohort);

        }

        /// <summary>
        /// Closes the form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Submits the form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void submitButton_Click(object sender, EventArgs e)
        {
            int cohortID, typeID;
            cohortID = (int)cohortBox.SelectedValue;
            typeID = (int)typeBox.SelectedValue;
            
            GraphReport thisEvalReport = new GraphReport();
            thisEvalReport.load(cohortID, typeID);
            thisEvalReport.MdiParent = MdiParent;
            thisEvalReport.Show();
            this.Close();
        }

        
    }
}
