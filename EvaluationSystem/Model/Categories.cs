﻿using System;

namespace EvaluationSystem.Model
{
   public class Categories
    {
        public Categories()
        {

        }

        public int CategoryId { get; set; }
        public String CategoryName { get; set; }
        public String Description { get; set; }
        public int CategoryNum { get; set; }
    }
}
