﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EvaluationSystem.Model;

namespace EvaluationSystemTest.ModelTest
{
    [TestClass]
    public class TypesTest
    {
        private Types types = new Types();
        private int typeID = 1;
        private string typeName = "Test Types";

        /// <summary>
        /// Test for newly created Types, all gets show return null
        /// </summary>
        [TestMethod]
        public void TestNullTypes()
        {
            //Return Types Data
            Assert.AreEqual(0, this.types.TypeID, 0, "Types's TypeID not 0.");
            Assert.IsNull(this.types.TypeName, "Types's TypeName not null.");
        }

        /// <summary>
        /// Test for set and get of an Types [Types class]
        /// </summary>
        [TestMethod]
        public void TestActiveTypes()
        {
            //Create Types
            this.types.TypeID = this.typeID;
            this.types.TypeName = this.typeName;

            //Return Types Data
            Assert.AreEqual(this.typeID, this.types.TypeID, 0, "Types's TypeID not " + this.typeID + ".");
            Assert.AreEqual(this.typeName, this.types.TypeName, "Types's TypeName not " + this.typeName + ".");
        }
    }
}