﻿using EvaluationSystem.Controller;
using EvaluationSystem.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EvaluationSystem.View
{
    public partial class CohortsDeleteForm : Form
    {
        private BindingList<Employees> employeeList;
        private BindingList<Employees> cohortEmployees;
        private EvaluationControl controller;

        public CohortsDeleteForm()
        {
            InitializeComponent();
            this.controller = new EvaluationControl();
            this.cbxCohort.SelectedValueChanged += cbxCohort_SelectedValueChanged;
        }


        /// <summary>
        /// Loads the data into the form when the Cohort ID is changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void cbxCohort_SelectedValueChanged(object sender, EventArgs e)
        {
            this.FillFormData();    
        }

        /// <summary>
        /// Loads the data into the form when the form is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CohortsDeleteForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the '_CS6232_g3DataSet.cohort' table. You can move, or remove it, as needed.
            this.cohortTableAdapter.Fill(this._CS6232_g3DataSet.cohort);

            this.FillFormData();
        }

        /// <summary>
        /// Fills the form with updated data
        /// </summary>
        private void FillFormData()
        {
            try
            {
                List<Employees> employees =  controller.getEmployeesWithoutCohorts();
                this.employeeList = new BindingList<Employees>(employees);
                this.employeeListBox.DataSource = employeeList;
                List<Employees> cohortEmployeeList = controller.GetMembersOfCohort(Convert.ToInt32(this.cbxCohort.SelectedValue));
                this.cohortEmployees = new BindingList<Employees>(cohortEmployeeList);
                this.editCohortBox.DataSource = cohortEmployees;
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }


        /// <summary>
        /// Closes the form with no action taken.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cohortButton_Click(object sender, EventArgs e)
        {
            if (cohortEmployees.Count() > 0)
            {
                this.employeeList.Add(cohortEmployees[editCohortBox.SelectedIndex]);
                cohortEmployees.Remove(cohortEmployees[editCohortBox.SelectedIndex]);
                
            }
        }

        /// <summary>
        /// Moves employee from the employeeList to the cohort ListBox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void employeeButton_Click(object sender, EventArgs e)
        {
            if (employeeList.Count() > 0)
            {
                this.cohortEmployees.Add(employeeList[employeeListBox.SelectedIndex]);
                employeeList.Remove(employeeList[employeeListBox.SelectedIndex]);
               
            }
        }

        /// <summary>
        /// Deletes the selected cohort
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void deleteButton_Click(object sender, EventArgs e)
        {
            bool isDeleted = false;

            DialogResult result = MessageBox.Show("Are you sure you want to delete this cohort", "Confirmation", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                try
                {
                    this.controller.DidCohortAnswer(Convert.ToInt32(this.cbxCohort.SelectedValue));
                    isDeleted = controller.DeleteCohort(Convert.ToInt32(this.cbxCohort.SelectedValue));
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Message, ex.GetType().ToString());
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, ex.GetType().ToString());
                }
                if (isDeleted)
                {
                    MessageBox.Show("Cohort Successfully Deleted");
                    this.CohortsDeleteForm_Load(sender, e);
                }
            }
            else
            {
                return;
            }
            
        }

        /// <summary>
        /// Updates the selected cohort
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void modifyButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.controller.DidCohortAnswer(Convert.ToInt32(this.cbxCohort.SelectedValue));

                foreach (Employees employee in this.cohortEmployees)
                {
                    employee.CohortID = Convert.ToInt32(this.cbxCohort.SelectedValue);
                }
                bool isUpdated = controller.UpdateCohort(Convert.ToInt32(this.cbxCohort.SelectedValue), this.cohortEmployees);

                if (isUpdated)
                {
                    MessageBox.Show("Cohort successfully updated. \n\rYou will need to reassign all evaluations.");
                    this.CohortsDeleteForm_Load(sender, e);
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }
    }
}
