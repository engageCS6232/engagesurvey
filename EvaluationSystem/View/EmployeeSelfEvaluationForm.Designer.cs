﻿namespace EvaluationSystem.View
{
    partial class EmployeeSelfEvaluationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.cancelButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.evaluationDataGridView = new System.Windows.Forms.DataGridView();
            this.evaluationBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.EvaluationID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RoleName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Take = new System.Windows.Forms.DataGridViewButtonColumn();
            ((System.ComponentModel.ISupportInitialize)(this.evaluationDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.evaluationBindingSource)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cancelButton
            // 
            this.cancelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelButton.Location = new System.Drawing.Point(847, 318);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(107, 38);
            this.cancelButton.TabIndex = 7;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(17, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(237, 25);
            this.label1.TabIndex = 6;
            this.label1.Text = "Available Self Evaluations";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // evaluationDataGridView
            // 
            this.evaluationDataGridView.AutoGenerateColumns = false;
            this.evaluationDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.evaluationDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EvaluationID,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn12,
            this.RoleName,
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn18,
            this.Take});
            this.evaluationDataGridView.DataSource = this.evaluationBindingSource;
            this.evaluationDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.evaluationDataGridView.Location = new System.Drawing.Point(0, 0);
            this.evaluationDataGridView.Margin = new System.Windows.Forms.Padding(2);
            this.evaluationDataGridView.Name = "evaluationDataGridView";
            this.evaluationDataGridView.RowTemplate.Height = 33;
            this.evaluationDataGridView.Size = new System.Drawing.Size(946, 232);
            this.evaluationDataGridView.TabIndex = 7;
            this.evaluationDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.evaluationDataGridView_CellContentClick);
            this.evaluationDataGridView.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.evaluationDataGridView_DataBindingComplete);
            // 
            // evaluationBindingSource
            // 
            this.evaluationBindingSource.DataSource = typeof(EvaluationSystem.Model.Evaluation);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.evaluationDataGridView);
            this.panel1.Location = new System.Drawing.Point(22, 56);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(946, 232);
            this.panel1.TabIndex = 8;
            // 
            // EvaluationID
            // 
            this.EvaluationID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.EvaluationID.DataPropertyName = "EvaluationID";
            this.EvaluationID.HeaderText = "EvaluationID";
            this.EvaluationID.Name = "EvaluationID";
            this.EvaluationID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.EvaluationID.Width = 116;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewTextBoxColumn10.DataPropertyName = "StageID";
            this.dataGridViewTextBoxColumn10.HeaderText = "StageID";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.Width = 87;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewTextBoxColumn12.DataPropertyName = "TypeID";
            this.dataGridViewTextBoxColumn12.HeaderText = "TypeID";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.Width = 82;
            // 
            // RoleName
            // 
            this.RoleName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.RoleName.DataPropertyName = "RoleName";
            this.RoleName.HeaderText = "RoleName";
            this.RoleName.Name = "RoleName";
            this.RoleName.Width = 103;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewTextBoxColumn17.DataPropertyName = "ParticipantInfo";
            this.dataGridViewTextBoxColumn17.HeaderText = "ParticipantName";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            this.dataGridViewTextBoxColumn17.Width = 141;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewTextBoxColumn18.DataPropertyName = "EvaluatorInfo";
            this.dataGridViewTextBoxColumn18.HeaderText = "EvaluatorName";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            this.dataGridViewTextBoxColumn18.Width = 134;
            // 
            // Take
            // 
            this.Take.HeaderText = "Next Steps";
            this.Take.Name = "Take";
            this.Take.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Take.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Take.Width = 150;
            // 
            // EmployeeSelfEvaluationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(980, 377);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "EmployeeSelfEvaluationForm";
            this.Text = "EmployeeSelfEvaluationForm";
            this.Load += new System.EventHandler(this.EmployeeSelfEvaluationForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.evaluationDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.evaluationBindingSource)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.BindingSource evaluationBindingSource;
        private System.Windows.Forms.DataGridView evaluationDataGridView;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridViewTextBoxColumn EvaluationID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn RoleName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewButtonColumn Take;

    }
}