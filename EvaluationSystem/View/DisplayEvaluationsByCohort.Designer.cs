﻿namespace EvaluationSystem.View
{
    partial class DisplayEvaluationsByCohort
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label cohortNameLabel;
            this.evaluationBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.evaluationDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RoleName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._CS6232_g3DataSet = new EvaluationSystem._CS6232_g3DataSet();
            this.cohortBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cohortTableAdapter = new EvaluationSystem._CS6232_g3DataSetTableAdapters.cohortTableAdapter();
            this.tableAdapterManager = new EvaluationSystem._CS6232_g3DataSetTableAdapters.TableAdapterManager();
            this.cohortNameComboBox = new System.Windows.Forms.ComboBox();
            cohortNameLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.evaluationBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.evaluationDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._CS6232_g3DataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cohortBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // cohortNameLabel
            // 
            cohortNameLabel.AutoSize = true;
            cohortNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            cohortNameLabel.Location = new System.Drawing.Point(72, 59);
            cohortNameLabel.Name = "cohortNameLabel";
            cohortNameLabel.Size = new System.Drawing.Size(123, 37);
            cohortNameLabel.TabIndex = 2;
            cohortNameLabel.Text = "Cohort:";
            // 
            // evaluationBindingSource
            // 
            this.evaluationBindingSource.DataSource = typeof(EvaluationSystem.Model.Evaluation);
            // 
            // evaluationDataGridView
            // 
            this.evaluationDataGridView.AllowUserToAddRows = false;
            this.evaluationDataGridView.AllowUserToDeleteRows = false;
            this.evaluationDataGridView.AutoGenerateColumns = false;
            this.evaluationDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.evaluationDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.RoleName,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn18});
            this.evaluationDataGridView.DataSource = this.evaluationBindingSource;
            this.evaluationDataGridView.Location = new System.Drawing.Point(16, 172);
            this.evaluationDataGridView.Name = "evaluationDataGridView";
            this.evaluationDataGridView.ReadOnly = true;
            this.evaluationDataGridView.RowTemplate.Height = 33;
            this.evaluationDataGridView.Size = new System.Drawing.Size(1896, 583);
            this.evaluationDataGridView.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "EvaluationID";
            this.dataGridViewTextBoxColumn1.HeaderText = "EvaluationID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // RoleName
            // 
            this.RoleName.DataPropertyName = "RoleName";
            this.RoleName.HeaderText = "RoleName";
            this.RoleName.Name = "RoleName";
            this.RoleName.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "CompletionDate";
            this.dataGridViewTextBoxColumn4.HeaderText = "CompletionDate";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "DueDate";
            this.dataGridViewTextBoxColumn9.HeaderText = "DueDate";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "StageID";
            this.dataGridViewTextBoxColumn10.HeaderText = "StageID";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "TypeID";
            this.dataGridViewTextBoxColumn12.HeaderText = "TypeID";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.DataPropertyName = "ParticipantInfo";
            this.dataGridViewTextBoxColumn17.HeaderText = "ParticipantInfo";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.DataPropertyName = "EvaluatorInfo";
            this.dataGridViewTextBoxColumn18.HeaderText = "EvaluatorInfo";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            // 
            // _CS6232_g3DataSet
            // 
            this._CS6232_g3DataSet.DataSetName = "_CS6232_g3DataSet";
            this._CS6232_g3DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cohortBindingSource
            // 
            this.cohortBindingSource.DataMember = "cohort";
            this.cohortBindingSource.DataSource = this._CS6232_g3DataSet;
            // 
            // cohortTableAdapter
            // 
            this.cohortTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.cohortTableAdapter = this.cohortTableAdapter;
            this.tableAdapterManager.employeesTableAdapter = null;
            this.tableAdapterManager.selectedTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = EvaluationSystem._CS6232_g3DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // cohortNameComboBox
            // 
            this.cohortNameComboBox.DataSource = this.cohortBindingSource;
            this.cohortNameComboBox.DisplayMember = "cohortName";
            this.cohortNameComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cohortNameComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cohortNameComboBox.FormattingEnabled = true;
            this.cohortNameComboBox.Location = new System.Drawing.Point(228, 56);
            this.cohortNameComboBox.Name = "cohortNameComboBox";
            this.cohortNameComboBox.Size = new System.Drawing.Size(276, 45);
            this.cohortNameComboBox.TabIndex = 3;
            this.cohortNameComboBox.ValueMember = "cohortID";
            // 
            // DisplayEvaluationsByCohort
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1942, 847);
            this.Controls.Add(cohortNameLabel);
            this.Controls.Add(this.cohortNameComboBox);
            this.Controls.Add(this.evaluationDataGridView);
            this.Name = "DisplayEvaluationsByCohort";
            this.Text = "DisplayEvaluationsByCohort";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DisplayEvaluationsByCohort_FormClosing);
            this.Load += new System.EventHandler(this.DisplayEvaluationsByCohort_Load);
            ((System.ComponentModel.ISupportInitialize)(this.evaluationBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.evaluationDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._CS6232_g3DataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cohortBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource evaluationBindingSource;
        private System.Windows.Forms.DataGridView evaluationDataGridView;
        private _CS6232_g3DataSet _CS6232_g3DataSet;
        private System.Windows.Forms.BindingSource cohortBindingSource;
        private _CS6232_g3DataSetTableAdapters.cohortTableAdapter cohortTableAdapter;
        private _CS6232_g3DataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.ComboBox cohortNameComboBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn RoleName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
    }
}