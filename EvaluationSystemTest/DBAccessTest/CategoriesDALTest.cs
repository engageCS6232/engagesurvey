﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using EvaluationSystem.Model;
using EvaluationSystem.DBAccess;

namespace EvaluationSystemTest.DBAccessTest
{
    [TestClass]
    public class CategoriesDALTest
    {
        /// <summary>
        /// Test return a Categories List of given Type
        /// </summary>
        [TestMethod]
        public void TestGetCategoriesByType()
        {
            //Correct Results
            int categoriesCount = 5;
            int indexCategories = 0;
            int categoryId = 1;
            string categoryName = "Category 1 Type 1";
            string description = "Earth///////////////////////////////";
            int typeId = 1;
            int categoryNum = 1;

            //Get Categories List
            List<Categories> categoriesListType1 = CategoriesDAL.getCategoriesByType(typeId);
            Categories categories = categoriesListType1[indexCategories];

            //Check Categories Results
            Assert.AreEqual(categoriesCount, categoriesListType1.Count, 0, "Categories count not " + categoriesCount + ".");
            Assert.AreEqual(categoryId, categories.CategoryId, 0, "Categories's CategoryId not " + categoryId + ".");
            Assert.AreEqual(categoryName, categories.CategoryName, "Categories's CategoryName not " + categoryName + ".");
            Assert.AreEqual(description, categories.Description, "Categories's Description not " + description + ".");
            Assert.AreEqual(categoryNum, categories.CategoryNum, 0, "Categories's CategoryNum not " + categoryNum + ".");
        }
    }
}