﻿using EvaluationSystem.Controller;
using EvaluationSystem.Model;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace EvaluationSystem.View
{
    public partial class EvaluationCreateForm : Form
    {

        private EvaluationControl controller;

        /// <summary>
        /// Constructor for the EvaluationCreateForm. 
        /// </summary>
        public EvaluationCreateForm()
        {
            InitializeComponent();
            controller = new EvaluationControl();
            this.cbxCohort.SelectedIndexChanged += cbxCohort_SelectedIndexChanged;


        }

        /// <summary>
        /// Listener for when the index of the cohort combobox changes. 
        /// reloads the form to display information for the respective cohort. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void cbxCohort_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.fillFormData();
        }


        /// <summary>
        /// Loads the initial information needed for the create evaluation form. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CreateCohortForm_Load(object sender, EventArgs e)
        {

            try
            {
                // populate cohort combobox
                List<Cohorts> cohortList = controller.GetCohortList();
                this.cbxCohort.DataSource = cohortList;
                this.cbxCohort.DisplayMember = "cohortName";
                this.cbxCohort.ValueMember = "cohortID";

                //populate types combobox
                List<Types> typesList = controller.GetTypesList();
                this.cbxType.DataSource = typesList;
                this.cbxType.DisplayMember = "typeName";
                this.cbxType.ValueMember = "typeID";

                //populate stage combobox
                List<Stage> stageList = controller.GetStageList();
                this.cbxStage.DataSource = stageList;
                this.cbxStage.DisplayMember = "stageName";
                this.cbxStage.ValueMember = "stageID";

                this.startDatePicker.MinDate = System.DateTime.Now;
                this.endDatePicker.MinDate = System.DateTime.Now;
                this.endDatePicker.Value = System.DateTime.Now.AddDays(30);

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());

            }


        }

        /// <summary>
        /// Closes the form whe the cancel button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Queries the database and fills the form with the appropriate data.
        /// </summary>
        private void fillFormData()
        {

        }

        /// <summary>
        /// Checks to ensure no invalid inputs are in the form and then inserts the new 
        /// evaluation into the cohort_eval_tracker table
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void submitButton_Click(object sender, EventArgs e)
        {

            if (endDatePicker.Value < startDatePicker.Value)
            {
                MessageBox.Show("End date cannot come before start date", "Input error");
            }
            else
            {
                try
                {
                    CohortEvalTracker cohortEvalToCreate = new CohortEvalTracker();
                    cohortEvalToCreate.cohortID = Convert.ToInt32(cbxCohort.SelectedValue);
                    cohortEvalToCreate.stageID = Convert.ToInt32(cbxStage.SelectedValue);
                    cohortEvalToCreate.typeID = Convert.ToInt32(this.cbxType.SelectedValue);
                    cohortEvalToCreate.startDate = this.startDatePicker.Value;
                    cohortEvalToCreate.endDate = this.endDatePicker.Value;

                    this.controller.CreateEvaluationsForCohortAndMembers(cohortEvalToCreate);

                    MessageBox.Show("Evaluation Successfully Created");
                    this.cohort_eval_trackerTableAdapter.Fill(this.evalTrackerTypeAndStageDataSet1.cohort_eval_tracker);
                    this.cohortTableAdapter.FillByAvailableCohorts(this._CS6232_g3DataSet.cohort);
                    this.fillFormData();
                }
                catch (DuplicateKeyException ex)
                {
                    MessageBox.Show("The database has been updated by another user and the form needs to be reloaded", ex.GetType().ToString());
                }

                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Message.ToString(), ex.GetType().ToString());
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message.ToString(), ex.GetType().ToString());
                }
            }
        }

        /// <summary>
        /// Updates the end date so that the default is 30 from the start date . 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void updateEndDate(object sender, EventArgs e)
        {
            if (endDatePicker.Enabled)
            {
                this.endDatePicker.Value = this.startDatePicker.Value.AddDays(30);
            }

        }
    }
}
